defmodule TrixtaLibrary.TrafficAlerts.TrafficExpert do
  def match_alert(
        traffic_alert,
        alert_criteria
      ) do
    default_alert_criteria = %{
      "make" => nil,
      "numberplate" => nil
    }

    updated_traffic_alert_format = create_matching_traffic_alert_format(traffic_alert)

    defaulted_alert_criteria =
      Map.merge(
        default_alert_criteria,
        alert_criteria
        |> Map.delete("alert_id")
        |> Map.delete("notify")
        |> Map.delete("message")
      )

    result =
      Enum.all?(
        defaulted_alert_criteria,
        fn {key, criteria_value} ->
          do_alert_values_match(key, Map.get(updated_traffic_alert_format, key), criteria_value)
        end
      )

    case result do
      true -> {"criteria_met", %{"result" => result}}
      false -> {"criteria_not_met", %{"result" => result}}
    end
  end

  @doc """
    Returns a new Map in the structure required for matching purposes. By
    checking each key in the traffic_alert if a match is found return
    the format that is needed to match on
  """
  defp create_matching_traffic_alert_format(traffic_alert) do
    Enum.into(
      traffic_alert,
      %{},
      fn {key, value} ->
        case key do
          "numberplate" -> {key, Map.get(value, "text")}
          _ -> {key, value}
        end
      end
    )
  end

  defp do_alert_values_match(_key, alert_value, criteria_value)
       when is_binary(alert_value) and is_binary(criteria_value) do
    String.equivalent?(String.downcase(alert_value), String.downcase(criteria_value))
  end

  # If a value isn't passed in with the criteria_value, assume it passes.
  defp do_alert_values_match(_key, _alert_value, nil) do
    true
  end

  defp do_alert_values_match(_key, alert_value, criteria_value) do
    alert_value === criteria_value
  end
end
