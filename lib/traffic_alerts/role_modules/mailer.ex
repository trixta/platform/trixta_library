defmodule TrixtaLibrary.TrafficAlerts.Mailer do
  import Bamboo.Email
  require Logger

  def traffic_alert_triggered_plain(%{} = context) do
    "email/templates/traffic_alert_triggered_email.plain.handlebars"
    |> TrixtaLibrary.Mail.EmailTemplates.render_body(context)
  end

  def build_traffic_alert_triggered_email(recipient_email_address, content) do
    new_email(
      to: recipient_email_address,
      from: "no-reply@trixta.com",
      subject: "Trixta Traffic Alert Triggered",
      text_body: traffic_alert_triggered_plain(content)
    )
    |> TrixtaLibrary.Mail.Email.add_required_headers()
  end

  def send_traffic_alert_triggered_email(recipient_email_address, content)
      when is_binary(recipient_email_address) do
    # Check if the recipient_email_address is valid:
    case Regex.run(~r/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/, recipient_email_address) do
      [_email] ->
        build_traffic_alert_triggered_email(recipient_email_address, content)
        |> TrixtaLibrary.Mail.Mailer.deliver_now()

      _ ->
        Logger.error(
          "Could not send traffic alert e-mail. Invalid recipient e-mail address: #{
            inspect(recipient_email_address)
          }"
        )

        :error
    end
  end

  def send_traffic_alert_triggered_email(recipient_email_address, content) do
    Logger.error(
      "Could not send traffic alert e-mail. Invalid e-mail address or content. E-mail: #{
        inspect(recipient_email_address)
      } . Content: #{inspect(content)}"
    )

    :error
  end
end
