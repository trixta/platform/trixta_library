defmodule TrixtaLibrary.TrafficAlerts.Notifier do
  import Bamboo.Email
  alias TrixtaLibrary.TrafficAlerts.Mailer
  require Logger

  def send_traffic_alert_email(
        traffic_alert_data,
        traffic_alert_criteria
      ) do
    email_content = %{
      model: Map.get(traffic_alert_data, "make", "model unknown"),
      number_plate: get_in(traffic_alert_data, ["numberplate", "text"]) || "numberplate unknown",
      message: Map.get(traffic_alert_criteria, "message", "")
    }

    recipient_email_address = Map.get(traffic_alert_criteria, "notify", "")

    case Mailer.send_traffic_alert_triggered_email(recipient_email_address, email_content) do
      %Bamboo.Email{} ->
        {"success", %{}}

      err ->
        Logger.error("Could not send traffic alert email. Mail helper returned: #{inspect(err)}")

        {
          "error",
          %{
            "details" =>
              "Could not send traffic alert email. Mail helper returned: #{inspect(err)}"
          }
        }
    end
  end

  def send_traffic_alert_email(traffic_alert_data, _processing_result, traffic_alert_criteria) do
    Logger.error(
      "Cannot send traffic alert e-mail. Invalid traffic alert criteria or traffic alert data: Traffic Alert Criteria: #{
        inspect(traffic_alert_criteria)
      } , Traffic Alert data: #{inspect(traffic_alert_data)}"
    )

    {"error", %{"details" => "Invalid traffic alert data or traffic alert criteria"}}
  end
end
