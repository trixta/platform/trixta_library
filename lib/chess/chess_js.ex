defmodule TrixtaLibrary.Chess.ChessJS do
  defp get_context() do
    :code.priv_dir(:trixta_space)
    |> Path.join("js/chessjs_wrapper.js")
    |> Path.expand()
    |> File.read!()
    |> Execjs.compile()
  end

  @moduledoc ~S"""
  Play a move. A pgn looks like "1. b4 g5 2. Nc3 Nf6 3. Ba3 Bh6 4. Qb1 Rg8". A move looks like "e2e4".
  """
  def validate_move(nil, move) do
    validate_move("", move)
  end

  def validate_move(pgn, move) do
    prev = System.monotonic_time()

    result =
      get_context()
      |> Execjs.call("validate_move", [pgn, move])
      |> map_valid_move_result()

    next = System.monotonic_time()
    diff = (next - prev) / 1_000_000
    IO.puts("validate_move took: #{diff}")

    result
  end

  def map_valid_move_result(%{"valid" => false, "pgn" => pgn}) do
    {"invalid_move", %{"pgn" => pgn}}
  end

  def map_valid_move_result(%{"valid" => true, "pgn" => pgn}) do
    {"valid_move", %{"pgn" => pgn}}
  end

  #  @moduledoc ~s"""
  #  Simulate a chess game
  #  """
  #  def simulate_game() do
  #    play_game(Game.new)
  #  end
  #  def simulate_game(%Game{status: status, current_position: current_position} = game) do
  #  end
end
