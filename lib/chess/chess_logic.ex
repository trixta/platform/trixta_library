defmodule TrixtaLibrary.Chess.ChessLogic do
  alias ChessLogic.Game
  alias ChessLogic.Position

  @doc ~S"""
  Play a move. A pgn looks like "1. b4 g5 2. Nc3 Nf6 3. Ba3 Bh6 4. Qb1 Rg8". A move looks like "e2e4".
  """
  def validate_move(pgn, move) do
    case from_pgn(pgn) do
      {:ok, g} ->
        Game.play(g, move) |> map_valid_move_result()

      _ ->
        {:error, "Could process PGN: #{pgn}"}
    end
  end

  def map_valid_move_result({:error, _}) do
    {"invalid_move", nil}
  end

  def map_valid_move_result({:ok, %Game{winner: winner} = g}) do
    case g do
      %Game{status: :over, result: "1/2-1/2"} -> {"stalemate", nil}
      %Game{status: :over, result: "1-0"} -> {{"checkmate", Atom.to_string(winner)}}
      %Game{status: :over, result: "0-1"} -> {{"checkmate", Atom.to_string(winner)}}
      _ -> {"valid_move", nil}
    end
  end

  @doc ~s"""
  Simulate a chess game
  """
  def simulate_game() do
    simulate_game(Game.new())
  end

  def simulate_game(%Game{status: status, current_position: current_position} = game) do
    case status do
      n when n in [:started, :playing] ->
        possible_moves = Position.all_possible_moves(current_position)
        selected_move = Enum.random(possible_moves)
        IO.puts("possible_moves:")
        IO.inspect(possible_moves)
        IO.puts("selected_move: #{selected_move}")
        IO.puts("pgn:")
        IO.inspect(Game.to_pgn(game))

        try do
          case Game.play(game, selected_move) do
            {:ok, g} ->
              IO.puts("post move postion:")
              IO.inspect(g.current_position)
              simulate_game(g)

            _ ->
              IO.puts("Error on move: #{selected_move}, retrying")
              simulate_game(game)
          end
        rescue
          _e in CaseClauseError ->
            IO.puts("Error on move: #{selected_move}, retrying")
            simulate_game(game)
        end

      _ ->
        IO.inspect(game)
        {:ok, game.status}
    end
  end

  @doc ~S"""
  Restore a game from a pgn. A pgn looks like "1. b4 g5 2. Nc3 Nf6 3. Ba3 Bh6 4. Qb1 Rg8".
  """
  def from_pgn(pgn) do
    pgn = String.trim(pgn)

    case pgn do
      "" ->
        {:ok, Game.new()}

      _ ->
        g =
          pgn
          # Remove move numbers
          |> String.replace(~r/\d+\.(\.\.)?/, "", [:global])
          # Remove black move indicator
          |> String.replace(~r/\.\.\./, "", [:global])
          |> String.trim()
          # Split into list of moves
          |> String.split(~r/\s+/)
          |> Enum.reduce(
            Game.new(),
            fn san, g ->
              case Position.san_to_move(g.current_position, to_string(san)) do
                {:ok, m} ->
                  {:ok, g} = Game.play(g, m)
                  g

                {:error, reason} ->
                  IO.puts("Could not process game tokens #{reason} for #{inspect(g)}")
                  g
              end
            end
          )

        {:ok, g}
    end
  end
end
