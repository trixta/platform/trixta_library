defmodule TrixtaLibrary.ChessFlow.ColorAlternator do
  @white "white"
  @black "black"
  @alternated "alternated"
  @color "color"

  def alternate_color() do
    {"defaulted", %{"color" => @white}}
  end

  def alternate_color(@white) do
    {@alternated, %{"color" => @black}}
  end

  def alternate_color(@black) do
    {@alternated, %{"color" => @white}}
  end

  def alternate_color(name) do
    raise("color: '#{inspect(name)}' is not valid for this color alternator")
  end
end
