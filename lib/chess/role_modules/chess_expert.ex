defmodule TrixtaLibrary.ChessFlow.ChessExpert do
  def validate_move(move, pgn) do
    TrixtaLibrary.Chess.ChessJS.validate_move(pgn, move)
  end
end
