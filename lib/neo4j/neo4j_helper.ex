defmodule TrixtaLibrary.Neo4j.Neo4jHelper do
  @moduledoc """
  Facilitates the configuration of services and running service calls.
  """

  require Logger

  # Accepts a map of relationships from gmail include "to", "from", "cc" and "bcc"
  # Sets up the correct create statements for neo4j and
  # saves them to a neo4j instance
  def create_statement_list_from_payload(payload) do
    header_map = headers_to_map(payload["headers"])

    from = header_map["from"]
    to_queries = create_to_queries(from, header_map["to"])
    cc_queries = create_cc_queries(from, header_map["cc"])
    bcc_queries = create_bcc_queries(from, header_map["bcc"])
    # Remove nil values
    to_queries ++ cc_queries ++ bcc_queries
  end

  defp headers_to_map(valuelist) do
    Enum.map(valuelist, fn instance ->
      %{String.downcase(instance["name"]) => [instance["value"]]}
    end)
    |> Enum.reduce(%{}, fn instance, acc ->
      Map.merge(acc, instance)
    end)
  end

  defp create_to_queries(from, nil) do
    []
  end

  defp create_to_queries(from, to) do
    trim_email_list(to)
    |> Enum.map(fn to_email ->
      "MERGE (a {name: '#{from}'}) MERGE (b {name: '#{to_email}'}) MERGE (a)-[r:HAS_EMAILED]->(b)"
    end)
  end

  defp create_cc_queries(from, nil) do
    []
  end

  defp create_cc_queries(from, cc) do
    trim_email_list(cc)
    |> Enum.map(fn to_cc ->
      "MERGE (a {name: '#{from}'}) MERGE (b {name: '#{to_cc}'}) MERGE (a)-[r:HAS_CCD]->(b)"
    end)
  end

  defp create_bcc_queries(from, nil) do
    []
  end

  defp create_bcc_queries(from, bcc) do
    trim_email_list(bcc)
    |> Enum.map(fn to_bcc ->
      "MERGE (a {name: '#{from}'}) MERGE (b {name: '#{to_bcc}'}) MERGE (a)-[r:HAS_BCCD]->(b)"
    end)
  end

  # Utility function to break up a list into a string, cast from charlist to string and trim the values.
  # eg "collin@trixta.com, barry@trixta.com" -> ["collin@trixta.com", "barry@trixta.com"]
  defp trim_email_list(email_list) do
    email_list
    |> to_string()
    |> String.split(",")
    |> Enum.map(&String.trim/1)
  end

  def save_to_neo_db(bolt_url, auth, statements) do
    try do
      neo4j_config = [
        url: bolt_url,
        basic_auth: [username: auth["username"], password: auth["password"]],
        ssl: true
      ]

      {:ok, _neo} = Bolt.Sips.start_link(neo4j_config)
      conn = Bolt.Sips.conn()

      Enum.each(statements, fn statement ->
        Bolt.Sips.query!(conn, statement)
      end)
    rescue
      e ->
        Logger.error("Failed to commit statement with reason: #{e.message}")
        {:error, %{"details" => "Failed to commit statement with reason: #{e.message}"}}
    end
  end

  '''
  def neo4jtest() do
    neo4j_config = [url: "bolt://<your bolt url>:7687",basic_auth: [username: "neo4j", password: "12345"],ssl: true]
    {:ok, _neo} = Bolt.Sips.start_link(neo4j_config)
    conn = Bolt.Sips.conn()
    Bolt.Sips.query!(conn, "MATCH (n) RETURN n")
  end
  '''
end
