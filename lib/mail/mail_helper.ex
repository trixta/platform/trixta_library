defmodule TrixtaLibrary.Mail.Helper do
  require Logger

  @doc """
  This is called when an existing agent invites a new user from the front-end.
  """
  def send_invitation_email(%{
        "email_address" => new_agent_email_address,
        "trixta_web_ui_endpoint" => trixta_web_ui_endpoint,
        "space" => space_domain,
        "role_id" => role_id,
        "verify_token" => verify_token
      }) do
    # here we check if it is a list then create relevant template string
    TrixtaLibrary.Mail.Email.invitation_email(
      new_agent_email_address,
      trixta_web_ui_endpoint,
      space_domain,
      verify_token,
      (is_list(role_id) && Enum.join(role_id, ", ")) || role_id
    )
    |> TrixtaLibrary.Mail.Mailer.deliver_now()

    {"success", %{}}
  end

  def send_invitation_custom_email(
        %{
          "email_address" => new_agent_email_address,
          "trixta_web_ui_endpoint" => trixta_web_ui_endpoint,
          "space" => space_domain,
          "role_id" => role_id,
          "verify_token" => verify_token
        },
        subject,
        from,
        text_body_base64
      ) do
    TrixtaLibrary.Mail.Email.invitation_custom_email(
      %{
        "new_agent_email_address" => new_agent_email_address,
        "trixta_web_ui_endpoint" => trixta_web_ui_endpoint,
        "space_domain" => space_domain,
        "verify_token" => verify_token,
        "role_id" => role_id
      },
      subject,
      from,
      text_body_base64
    )
    |> TrixtaLibrary.Mail.Mailer.deliver_now()

    {"success", %{}}
  end

  def send_custom_email_with_attachment(to, subject, from, filename, data, text_body) do
    context = %{}

    TrixtaLibrary.Mail.Email.custom_email_with_attachment(
      to,
      subject,
      from,
      filename,
      data,
      context,
      text_body
    )
    |> TrixtaLibrary.Mail.Mailer.deliver_now()

    {"success", %{}}
  end

  def send_password_reset_request_email(email_address, reset_token, space, trixta_web_ui_endpoint) do
    TrixtaLibrary.Mail.Email.password_reset_request_email(
      email_address,
      reset_token,
      space,
      trixta_web_ui_endpoint
    )
    |> TrixtaLibrary.Mail.Mailer.deliver_now()
  end

  def send_account_verify_request_email(%{
        "email_address" => email_address,
        "profile" => %{"firstname" => name},
        "verify_token" => verify_token,
        "trixta_web_ui_endpoint" => trixta_web_ui_endpoint,
        "space" => space_domain
      }) do
    send_account_verify_request_email(
      email_address,
      trixta_web_ui_endpoint,
      space_domain,
      verify_token,
      name
    )

    {"success", %{}}
  end

  def send_account_verify_request_email(
        email_address,
        trixta_web_ui_endpoint,
        space_domain,
        verify_token,
        name
      ) do
    TrixtaLibrary.Mail.Email.account_verification_request_email(
      email_address,
      trixta_web_ui_endpoint,
      space_domain,
      verify_token,
      name
    )
    |> TrixtaLibrary.Mail.Mailer.deliver_now()
  end

  def send_welcome_email(email_address, space_url \\ nil, trixta_web_ui_endpoint \\ nil) do
    TrixtaLibrary.Mail.Email.build_welcome_email(email_address, space_url, trixta_web_ui_endpoint)
    |> TrixtaLibrary.Mail.Mailer.deliver_now()
  end

  def send_feedback_email(content) do
    TrixtaLibrary.Mail.Email.build_feedback_email(content)
    |> TrixtaLibrary.Mail.Mailer.deliver_now()
  end

  def send_alert_triggered_email(agent_email_address, %{:timestamp => timestamp} = content)
      when is_binary(agent_email_address) and is_binary(timestamp) do
    content = Map.put(content, :timestamp, format_timestamp(timestamp))
    # Check if the agent e-mail address is valid:
    case Regex.run(~r/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/, agent_email_address) do
      [_email] ->
        TrixtaLibrary.Mail.Email.build_alert_triggered_email(agent_email_address, content)
        |> TrixtaLibrary.Mail.Mailer.deliver_now()

      _ ->
        Logger.error(
          "Could not send alert e-mail. Invalid agent e-mail address: #{
            inspect(agent_email_address)
          }"
        )

        :error
    end
  end

  def send_alert_triggered_email(agent_email_address, content) do
    Logger.error(
      "Could not send alert e-mail. Invalid e-mail address or content. E-mail: #{
        inspect(agent_email_address)
      } . Content: #{inspect(content)}"
    )

    :error
  end

  defp format_timestamp(timestamp) when is_binary(timestamp) do
    case DateTime.from_iso8601(timestamp) do
      {:ok, parsed_timestamp, _} ->
        format_timestamp(parsed_timestamp)

      _ ->
        Logger.warn(
          "Could not parse timestamp for alert: #{inspect(timestamp)} . Cannot format the timestamp."
        )

        timestamp
    end
  end

  defp format_timestamp(%DateTime{} = datetime) do
    case datetime |> Calendar.Strftime.strftime("%Y-%m-%d %H:%M:%S GMT") do
      {:ok, formatted_timestamp} ->
        formatted_timestamp

      _ ->
        Logger.warn(
          "Could not format this timestamp while sending alert e-mail: #{inspect(datetime)}"
        )

        datetime
    end
  end

  defp format_timestamp(timestamp) do
    Logger.warn(
      "Could not format this timestamp while sending alert e-mail: #{inspect(timestamp)}"
    )

    timestamp
  end

  # send custom email WIHOUT data
  def send_custom_email(to, subject, from, body, context, nil, nil, shouldEncodeBody) do
    # check if the body is decoded or not

    TrixtaLibrary.Mail.Email.send_custom_email_no_attachment(
      to,
      subject,
      from,
      body,
      context,
      nil,
      nil,
      shouldEncodeBody || false
    )
    |> TrixtaLibrary.Mail.Mailer.deliver_now()

    {"success", %{}}
  end

  # send custom email WITH data
  def send_custom_email(to, subject, from, body, context, filename, data, shouldEncodeBody) do
    TrixtaLibrary.Mail.Email.send_custom_email_attachment(
      to,
      subject,
      from,
      body,
      context,
      filename,
      data,
      shouldEncodeBody || false
    )
    |> TrixtaLibrary.Mail.Mailer.deliver_now()

    {"success", %{}}
  end
end
