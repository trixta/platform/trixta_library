defmodule TrixtaLibrary.Mail.EmailTemplates do
  require Logger

  def invitation_email_plain(%{} = context) do
    "email/templates/invitation_email.plain.handlebars"
    |> render_body(context)
  end

  def password_reset_html(%{} = context) do
    "email/templates/reset_password_email.html.handlebars"
    |> render_body(context)
  end

  def password_reset_plain(%{} = context) do
    "email/templates/reset_password_email.plain.handlebars"
    |> render_body(context)
  end

  def account_verify_plain(%{} = context) do
    "email/templates/account_verify_mail.plain.handlebars"
    |> render_body(context)
  end

  def welcome_email_html(%{} = context) do
    "email/templates/welcome_mail.html.handlebars"
    |> render_body(context)
  end

  def welcome_email_plain(%{} = context) do
    "email/templates/welcome_mail.plain.handlebars"
    |> render_body(context)
  end

  def alert_triggered_plain(%{} = context) do
    "email/templates/alert_triggered_email.plain.handlebars"
    |> render_body(context)
  end

  @doc """
  Renders the body of the email based on handlebars email template
  """
  def render_body(template_path, %{} = context) when is_binary(template_path) do
    :code.priv_dir(:trixta_space)
    |> Path.join(template_path)
    |> Path.expand()
    |> File.read!()
    |> Mustache.render(context)
  end

  @doc """
   Log an error if unable to render the body of the email using handlebars template
  """
  def render_body(template_path, context) do
    Logger.error(
      "Could not render e-mail body: Invalid template path or context. Template: #{
        inspect(template_path)
      } , context: #{inspect(context)}"
    )

    ""
  end
end
