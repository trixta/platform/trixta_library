defmodule TrixtaLibrary.Mail.Email do
  import Bamboo.Email

  @doc """
  Sent when an existing agent invites a new user from the front-end.
  """
  def invitation_email(
        new_agent_email_address,
        trixta_web_ui_endpoint,
        space_domain,
        verify_token,
        role_id
      ) do
    subject = "You have been invited to Trixta"

    new_email(
      to: new_agent_email_address,
      from: "no-reply@trixta.com",
      subject: subject,
      text_body:
        TrixtaLibrary.Mail.EmailTemplates.invitation_email_plain(%{
          new_agent_email_address: URI.encode_www_form(new_agent_email_address),
          verify_token: URI.encode_www_form(verify_token),
          trixta_web_ui_endpoint: trixta_web_ui_endpoint,
          space_domain_encoded: URI.encode_www_form(space_domain),
          space_domain: space_domain,
          role_id: role_id,
          subject: subject
        })
    )
    |> add_required_headers()
  end

  def invitation_custom_email(
        %{
          "new_agent_email_address" => new_agent_email_address,
          "trixta_web_ui_endpoint" => trixta_web_ui_endpoint,
          "space_domain" => space_domain,
          "verify_token" => verify_token,
          "role_id" => role_id
        },
        subject \\ "You have been invited to Trixta",
        from \\ "no-reply@trixta.com",
        text_body_base64 \\ ""
      ) do
    new_email(
      to: new_agent_email_address,
      from: from,
      subject: subject,
      text_body:
        Mustache.render(
          Base.decode64!(text_body_base64),
          %{
            new_agent_email_address: URI.encode_www_form(new_agent_email_address),
            verify_token: URI.encode_www_form(verify_token),
            trixta_web_ui_endpoint: trixta_web_ui_endpoint,
            space_domain_encoded: URI.encode_www_form(space_domain),
            space_domain: space_domain,
            role_id: role_id,
            subject: subject
          }
        )
    )
    |> add_required_headers()
  end

  def custom_email_with_attachment(
        to,
        subject,
        from,
        filename,
        data,
        context,
        text_body \\ ""
      ) do
    new_email(
      to: to,
      from: from,
      subject: subject,
      text_body:
        Mustache.render(
          text_body,
          context
        )
    )
    |> add_required_headers()
    |> put_attachment(%Bamboo.Attachment{
      filename: filename,
      data: data
    })
  end

  def account_verification_request_email(
        email_address,
        trixta_web_ui_endpoint,
        space_domain,
        verify_token,
        name
      ) do
    config = Application.get_env(:trixta_library, TrixtaLibrary.Mail.Mailer)

    subject = "Verify your email address"

    new_email(
      to: email_address,
      from: "no-reply@trixta.com",
      subject: subject,

      #            # Disabling html emails as they are currently not rendering correctly in gmail
      #            html_body: TrixtaLibrary.Mail.EmailTemplates.password_reset_html(
      #              %{
      #                email_address: URI.encode(email_address),
      #                trixta_web_ui_endpoint: trixta_web_ui_endpoint,
      #                token: URI.encode(reset_token),
      #                space: URI.encode(space_domain),
      #                subject: subject
      #              }
      #            ),

      text_body:
        TrixtaLibrary.Mail.EmailTemplates.account_verify_plain(%{
          email_address: URI.encode(email_address),
          trixta_web_ui_endpoint: trixta_web_ui_endpoint,
          token: URI.encode_www_form(verify_token),
          space: URI.encode_www_form(space_domain),
          name: name,
          subject: subject
        })
    )
    |> add_required_headers()
  end

  def password_reset_request_email(email_address, reset_token, space, trixta_web_ui_endpoint) do
    config = Application.get_env(:trixta_library, TrixtaLibrary.Mail.Mailer)

    trixta_domain_url = trixta_web_ui_endpoint

    trixta_space_url = space

    subject = "Reset your trixta password"

    new_email(
      to: email_address,
      from: "no-reply@trixta.com",
      subject: subject,

      #            # Disabling html emails as they are currently not rendering correctly in gmail
      #            html_body: TrixtaLibrary.Mail.EmailTemplates.password_reset_html(
      #              %{
      #                email_address: URI.encode(email_address),
      #                trixta_domain: trixta_domain_url,
      #                token: URI.encode(reset_token),
      #                space: URI.encode(trixta_space_url),
      #                subject: subject
      #              }
      #            ),

      text_body:
        TrixtaLibrary.Mail.EmailTemplates.password_reset_plain(%{
          email_address: URI.encode(email_address),
          trixta_domain: trixta_domain_url,
          token: URI.encode_www_form(reset_token),
          space: URI.encode_www_form(trixta_space_url),
          subject: subject
        })
    )
    |> add_required_headers()
  end

  def build_welcome_email(email_address, space_url \\ nil, trixta_web_ui_endpoint \\ nil) do
    config = Application.get_env(:trixta_library, TrixtaLibrary.Mail.Mailer)

    trixta_domain_url =
      trixta_web_ui_endpoint || System.get_env("TRIXTA_DOMAIN_URL") ||
        Keyword.get(config, :trixta_domain_url)

    trixta_space_url =
      space_url || System.get_env("TRIXTA_SPACE_URL") || Keyword.get(config, :trixta_space_url)

    subject = "Welcome to trixta"

    new_email(
      to: email_address,
      from: "no-reply@trixta.com",
      subject: subject,

      #      # Disabling html emails as they are currently not rendering correctly in gmail
      #      html_body: TrixtaLibrary.Mail.EmailTemplates.welcome_email_html(
      #        %{
      #          email_address: URI.encode(email_address),
      #          trixta_domain: trixta_domain_url,
      #          token: URI.encode(reset_token),
      #          space: URI.encode(trixta_space_url),
      #          subject: subject
      #        }
      #      ),

      text_body:
        TrixtaLibrary.Mail.EmailTemplates.welcome_email_plain(%{
          email_address_unencoded: email_address,
          email_address: URI.encode_www_form(email_address),
          trixta_domain: trixta_domain_url,
          space: trixta_space_url,
          subject: subject
        })
    )
    |> add_required_headers()
  end

  def build_feedback_email(content) do
    config = Application.get_env(:trixta_library, TrixtaLibrary.Mail.Mailer)

    trixta_feedback_email =
      System.get_env("TRIXTA_FEEDBACK_EMAIL") || Keyword.get(config, :trixta_feedback_email)

    subject = "Trixta Feedback"

    new_email(
      to: trixta_feedback_email,
      from: "no-reply@trixta.com",
      subject: subject,
      text_body: content
    )
    |> add_required_headers()
  end

  def build_alert_triggered_email(agent_email_address, content) do
    new_email(
      to: agent_email_address,
      from: "no-reply@trixta.com",
      subject: "Trixta Alert Triggered",
      text_body: TrixtaLibrary.Mail.EmailTemplates.alert_triggered_plain(content)
    )
    |> add_required_headers()
  end

  defp add_required_headers(email) do
    config = Application.get_env(:trixta_library, TrixtaLibrary.Mail.Mailer)

    trixta_bounce_email =
      System.get_env("TRIXTA_BOUNCE_EMAIL") || Keyword.get(config, :trixta_bounce_email)

    trixta_complaints_email =
      System.get_env("TRIXTA_COMPLAINTS_EMAIL") || Keyword.get(config, :trixta_complaints_email)

    email
    |> put_header("Bounces-to", trixta_bounce_email)
    |> put_header("Return-Path", trixta_complaints_email)
  end

  # send custom email data
  def send_custom_email_no_attachment(
        to,
        subject,
        from,
        message,
        context \\ %{},
        filename,
        data,
        shouldEncodeBody
      ) do
    message = (shouldEncodeBody && Base.encode64(message)) || message

    case Base.decode64(message) do
      :error ->
        new_email(
          to: to,
          from: from,
          subject: subject,
          text_body: message
        )
        |> add_required_headers()

      {:ok, decoded_mesage} ->
        new_email(
          to: to,
          from: from,
          subject: subject,
          text_body:
            Mustache.render(
              decoded_mesage,
              context
            )
        )
        |> add_required_headers()
    end
  end

  # the reason i dont use the default send with attach above is becase we need to decode the data in the body
  def send_custom_email_attachment(
        to,
        subject,
        from,
        message \\ "",
        context \\ %{},
        filename,
        data,
        shouldEncodeBody
      ) do
    message = (shouldEncodeBody && Base.encode64(message)) || message

    case Base.decode64(message) do
      :error ->
        new_email(
          to: to,
          from: from,
          subject: subject,
          text_body: message
        )
        |> add_required_headers()
        |> put_attachment(%Bamboo.Attachment{
          filename: filename,
          data: data
        })

      {:ok, decoded_mesage} ->
        new_email(
          to: to,
          from: from,
          subject: subject,
          text_body:
            Mustache.render(
              decoded_mesage,
              context
            )
        )
        |> add_required_headers()
        |> put_attachment(%Bamboo.Attachment{
          filename: filename,
          data: data
        })
    end
  end
end
