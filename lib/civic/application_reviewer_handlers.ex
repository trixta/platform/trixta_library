defmodule TrixtaLibrary.Civic.ApplicationReviewerHandlers do
  @moduledoc """
  Helper functions for the Civic Application Reviewer (aka 'Save validator') role.
  This role is currently filled by Civic's React app that admins use to approve validators.
  Two actions:
      - save_validator_on_blockchain : Initiates saving a validator to the blockchain.
                                       This action is NOT defined here...it is a flow handler configured on the role,
                                       linked to the 'Save validator on blockchain' flow.

      - submit_signed_transaction    : Saving a validator requires a transaction to be signed by the client (React app).
                                       When the client submits the signed transaction, the mid-flow room trigger has to be fired.
                                       The handler is implemented in this module, which routes the transaction to the appropriate flow.
      

  Ideally, we'd do away with the Room Trigger and use an updated step type / flow device, but we'll stick with the current flow
  until the new version of the flow engine is ready. That way we don't have to make too many flow changes for now.
  """
  require Logger

  @doc """
  Called when the Civic React client submits a signed transaction back to the space.
  """
  def submit_signed_transaction(%{"signed_transactions" => signed_txes, "flow_id" => flow_id}) do
    # This is a room trigger because the 'Save Validator on Blockchain' flow currently uses a RebelOS 'room trigger' to receive the signed transactions
    # from the React app.
    # We copy the flow_id into the trixta_tx_id on the trigger payload, because the flow currently has filter logic on the trigger itself and we
    # prefer not to change the flows too much at this point.
    trigger_payload = %{"signed_transactions" => signed_txes, "trixta_tx_id" => flow_id}

    inject_room_trigger(
      "application_reviewer",
      "submit_signed_transaction",
      trigger_payload,
      flow_id
    )
  end

  defp inject_room_trigger(role_name, action_name, payload, flow_id) do
    Logger.debug(
      "trixta_space injecting a room trigger into the flow engine. Role name: #{role_name} - Action name: #{
        inspect(action_name)
      } . Flow id: #{inspect(flow_id)}"
    )

    # Send the trigger to the RebelOS flow engine for processing.
    # The 'ExecutionManager' is a globally name-registered GenServer in the rebelos_engine_trixta project (flow engine).
    GenServer.call(
      ExecutionManager,
      {:inject_room_trigger, role_name, action_name, payload, flow_id}
    )
  end
end
