defmodule TrixtaLibrary.Civic.DatastoreQueryHelpers do
  @moduledoc """
  Helper functions to access flow execution logs from the rebel engine.
  """
  require Logger

  def list_execution_logs_over_timespan(%{
        "start_date_time" => start_date_time,
        "end_date_time" => end_date_time
      }) do
    Mongo.aggregate(
      :mongo,
      "flow_execution_records",
      [
        %{
          "$match": %{
            flow_name: %{"$exists": true},
            terminated_at: %{
              "$gte": DateTime.from_iso8601(start_date_time) |> elem(1),
              "$lte": DateTime.from_iso8601(end_date_time) |> elem(1)
            }
          }
        },
        %{"$group": %{_id: %{name: "$flow_name", flow_id: "$flow_id"}, count: %{"$sum": 1}}}
      ],
      pool: DBConnection.Poolboy
    )
    |> Enum.to_list()
  end

  def query_details_execution_logs_over_timespan_by_id(%{
        "id" => id,
        "start_date_time" => start_date_time,
        "end_date_time" => end_date_time
      }) do
    Mongo.aggregate(
      :mongo,
      "flow_execution_records",
      [
        %{
          "$match": %{
            flow_id: id,
            terminated_at: %{
              "$gte": DateTime.from_iso8601(start_date_time) |> elem(1),
              "$lte": DateTime.from_iso8601(end_date_time) |> elem(1)
            }
          }
        },
        %{
          "$group": %{
            _id: %{
              id: "$_id",
              flow_instance_id: "$flow_instance_id",
              build_end: "$build_end",
              flow_name: "$flow_name",
              step_execution_logs: "$step_execution_logs",
              build_started_at: "$build_started_at",
              exec_started_at: "$exec_started_at",
              terminated_at: "$terminated_at"
            }
          }
        }
      ],
      pool: DBConnection.Poolboy
    )
    |> Enum.to_list()
  end
end
