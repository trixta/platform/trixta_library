defmodule TrixtaLibrary.Civic.BlockchainProcessorHandlers do
  @moduledoc """
  Helper functions for the Civic Blockchain Processor (aka 'Node server') to call in order to
  ask for transaction signing, notify about mining results, etc.
  """
  require Logger

  # These id's are defined in the Civic service on gitlab, as well as the escrow flows.
  # They map to a particular type of trigger. In future, we could replace these with simpler 'role trigger' constructs.
  @sign_request_trigger_id "7e4e6b57-2141-4bc0-be9b-70ac8f9edc85"
  @mining_result_trigger_id "b5fd02e0-f477-4bd8-8b5b-7fd7207d0aba"

  @doc """
  Called when the blockchain processor needs us to have a transaction signed.
  """
  def request_transaction_signing(%{
        "address" => eth_address,
        "unsigned_transactions" => unsigned_transactions,
        "flow_id" => flow_id
      }) do
    # The flows still expect 'trixta_tx_id' in the trigger filter logic.
    payload = %{
      "address" => eth_address,
      "unsigned_transactions" => unsigned_transactions,
      "trixta_tx_id" => flow_id
    }

    inject_service_trigger(@sign_request_trigger_id, payload, flow_id)
  end

  @doc """
  Called by the blockchain processor when something goes wrong before a transaction gets to the signing stage.
  """
  def pre_sign_error(%{"flow_id" => flow_id, "error_details" => error_details}) do
    # 1. The flows still expect 'trixta_tx_id' in the trigger filter logic.
    # 2. Even though the trigger id is the same as the normal 'sign requested' case, the presence of the 'error_details' property is what signifies to the flow that this is an error.
    Logger.info(
      "pre-sign error came in from BlockchainProcessor. Flow id: #{inspect(flow_id)} . Error details: #{
        inspect(error_details)
      }"
    )

    payload = %{"error_details" => error_details, "trixta_tx_id" => flow_id}
    inject_service_trigger(@sign_request_trigger_id, payload, flow_id)
  end

  @doc """
  Called by the blockchain processor when civic-marketplace-tx reports that mining succeeded.
  """
  def mining_succeeded(%{"flow_id" => flow_id}) do
    # The flows still expect 'trixta_tx_id' in the trigger filter logic.
    Logger.info(
      "mining success notification came in from BlockchainProcessor. Flow id: #{inspect(flow_id)}"
    )

    inject_service_trigger(@mining_result_trigger_id, %{"trixta_tx_id" => flow_id}, flow_id)
  end

  @doc """
  Called by the blockchain processor when civic-marketplace-tx reports that mining failed for some reason (timeout, insufficient ether, etc)
  """
  def mining_failed(%{"flow_id" => flow_id, "error_details" => error_details}) do
    # 1. The flows still expect 'trixta_tx_id' in the trigger filter logic.
    # 2. Even though the trigger id is the same as the success case, the presence of the 'error_details' property is what signifies to the flow that this is an error.
    Logger.info(
      "mining failure error came in from BlockchainProcessor. Flow id: #{inspect(flow_id)} . Error details: error_details"
    )

    payload = %{"error_details" => error_details, "trixta_tx_id" => flow_id}
    inject_service_trigger(@mining_result_trigger_id, payload, flow_id)
  end

  defp inject_service_trigger(service_trigger_id, payload, flow_id) do
    Logger.info(
      "trixta_space injecting a service trigger into the flow engine. Service trigger id: #{
        inspect(service_trigger_id)
      } - Flow instance id: #{inspect(flow_id)} . Payload: #{inspect(payload)}"
    )

    # Send the trigger to the RebelOS flow engine for processing.
    # The 'ExecutionManager' is a globally name-registered GenServer in the rebelos_engine_trixta project (flow engine).
    GenServer.call(
      ExecutionManager,
      {:inject_service_trigger, %{"guid" => service_trigger_id}, payload, flow_id}
    )
  end
end
