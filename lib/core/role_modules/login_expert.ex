defmodule TrixtaLibrary.Core.LoginExpert do
  # overload the login_agent function to convert return tags to binary, for step branching comparison logic.
  def overload_login_agent(identity, password) do
    case TrixtaSpace.AgentsSupervisor.login_agent(identity, password) do
      {:ok, data} -> {"ok", data}
      {:error, data} -> {"error", data}
      {_, _} -> {"unknown", %{}}
    end
  end
end
