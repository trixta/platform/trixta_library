defmodule TrixtaLibrary.Core.AwsExpert do
  @moduledoc """
  Helper funtions for Aws
  """
  require Logger
  require ExAws.S3
  require ExAws.EC2
  import SweetXml

  # External Functions
  @doc """
  Backs up a spaces data (DETS) to a given AWS s3 bucket.
  Payload example:
  {
    "identity": "agent",
    "password": "12345",
    "topic": "space:trixta_app_builder",
    "event": "space_data_backup",
    "args": {
      "aws_access_key_id":"12345",
      "aws_access_key":"12345",
      "aws_region": "us-west-2",
      "aws_bucket": "s3_bucket_name",
      "file_name":"testing_from_localhost"
    }
  }
  """
  def space_data_backup(_source_data, input_data) do
    Logger.info("Backing up data storage data from /var/tmp to s3 bucket")
    # Generate dets archive
    {:ok, dets_zip_filename} = generate_dets_archive(input_data)

    Logger.info("Archive creation successful: #{dets_zip_filename}")

    {:ok, file_binary} = File.read(dets_zip_filename)

    Logger.info("Uploading data archive to s3 bucket...")

    case ExAws.S3.put_object(
           input_data["aws_bucket"] || "databackup.uat.trixta.io",
           dets_zip_filename,
           file_binary
         )
         |> request_with_config(input_data) do
      {:ok, _} ->
        Logger.info("Successfully uploaded data archive! ")

      {:error, error_string} ->
        Logger.error("Failed to upload archive. Reason: #{error_string}")
        raise error_string
    end

    File.rm(dets_zip_filename)

    {"success", %{}}
  end

  def space_data_restore(_source_data, input_data) do
    # dostuff to backup data
    Logger.error("Restored from s3 not yet implemented")
    raise("Not implemented")
  end

  @doc """
  Does everything needed to expose a space on a cluster's ALB.
  This assumes the space has already been created on the cluster through helm,
  and can be accessed using its internet-facing classic load balancer.
  These steps integrate the space's ingress into the cluster's main ALB through a target group.

  If an ALB rule already exists for this hostname, then deregister and re-register the targets
  under the new port (the k8s port changes with every helm upgrade)

  ## Parameters
    - vpc_id : Identifies the cluster within AWS.
    - host_name : The host pattern to use for the ALB listener rule, e.g. foobar.test.trixta.io
    - port : The port on which the target group listens. This would have been set up when the k8s ingress controller was created.
    - credentials : A map containing the aws_region, aws_access_key_id and aws_access_key
  """
  def set_up_new_space_routing(vpc_id, host_name, port, credentials) do
    # We want only the hostname without the protocol.
    sanitized_host_name =
      String.replace(host_name, "http://", "")
      |> String.replace("https://", "")

    target_group_name = host_to_target_group_name(sanitized_host_name)

    # First check if an ALB rule already exists for this hostname.
    # If it does, deregister the targets and re-create them with an update target group port.
    {:ok, listener_arn} = get_alb_listener_by_vpc(vpc_id, credentials)
    {:ok, rules_xml} = describe_rules(listener_arn, credentials)

    if alb_rule_exists?(rules_xml, sanitized_host_name) do
      :ok = update_space_port(vpc_id, port, target_group_name, credentials)
      Logger.info("AWS space NodePort has been updated to #{port} .")
      {"success", %{}}
    else
      # This is for a new space. Set up everything from scratch:
      {:ok, next_free_priority} = get_next_free_rule_priority(rules_xml, credentials)

      {:ok, target_group_arn} =
        create_target_group_and_register(target_group_name, vpc_id, port, credentials)

      :ok = add_target_group_healthcheck(target_group_arn, credentials)

      :ok =
        create_alb_rule(
          listener_arn,
          rules_xml,
          sanitized_host_name,
          target_group_arn,
          credentials
        )

      Logger.info(
        "AWS space routing set up for #{sanitized_host_name} . It should now be accessible there through the cluster's ALB."
      )

      {"success", %{}}
    end
  end

  @doc """
  Deletes all resources related to the space at the particular host_name.

  - vpc_id : Identifies the cluster within AWS.
  - host_name : The space's public domain as set up in the ALB rules.
  - credentials : A map containing the aws_region, aws_access_key_id and aws_access_key
  """
  def delete_space_routing(vpc_id, host_name, credentials) do
    sanitized_host_name =
      String.replace(host_name, "http://", "")
      |> String.replace("https://", "")

    # Get the ALB's listener and attached rule:
    {:ok, alb_rule_arn} = get_alb_rule_arn(vpc_id, sanitized_host_name, credentials)
    :ok = delete_alb_rule(alb_rule_arn, credentials)

    # Delete the target group:
    {:ok, target_group_arn} = get_target_group_arn(vpc_id, sanitized_host_name, credentials)
    :ok = delete_target_group(target_group_arn, credentials)

    {"success", %{}}
  end

  def update_space_port(vpc_id, new_port, target_group_name, credentials) do
    {:ok, [_first | _rest] = target_groups} = describe_target_groups(vpc_id, credentials)

    existing_target_group =
      target_groups
      |> Enum.filter(&(&1.target_group_name == target_group_name))
      |> Enum.at(0)

    if not is_map(existing_target_group) do
      raise "An ALB rule already existed, but no corresponding target group exists. Cannot proceed with update."
    end

    # De-register all this cluster's EC2 workers from the target group:
    deregister_worker_targets_from_group(
      existing_target_group.target_group_arn,
      vpc_id,
      credentials
    )

    # Now re-register the worker targets under the new port:
    {:ok, _} =
      register_worker_targets_with_group(
        existing_target_group.target_group_arn,
        vpc_id,
        new_port,
        credentials
      )

    :ok = add_target_group_healthcheck(existing_target_group.target_group_arn, credentials)

    :ok
  end

  def get_alb_listener_by_vpc(vpc_id, credentials) do
    {:ok, alb_arn} = get_alb_by_vpc(vpc_id, credentials)

    case ExAws.ElasticLoadBalancingV2.describe_listeners(load_balancer_arn: alb_arn)
         |> request_with_config(credentials) do
      {:ok, %{:status_code => 200, :body => xml_body}} ->
        # We only care about the first listener. In our case, there's always just one listener on port 443 of the ALB.
        {:ok, xpath(xml_body, ~x"//ListenerArn/text()") |> to_string}

      err ->
        Logger.error(
          "Could not describe listeners for load balancer ARN #{inspect(alb_arn)} . Details: #{
            inspect(err)
          }"
        )

        {:error, err}
    end
  end

  @doc """
  Returns the ARN of the ALB for a particular cluster vpc.
  This ARN is needed when creating listener rules, for example.
  """
  def get_alb_by_vpc(vpc_id, credentials) do
    case describe_albs(credentials) |> Enum.find(&(&1.vpc_id == vpc_id)) do
      %{:load_balancer_arn => arn} ->
        {:ok, arn}

      _ ->
        Logger.error("Could not find ALB for VPC #{inspect(vpc_id)}")
        {:error, :not_found}
    end
  end

  def describe_albs(credentials) do
    # Note: The 'V2' version of this module returns only ALB's (which we want) and NLB's .
    # Classic LB's are returned by the V1 version, which we don't care about here.
    # See the first paragraph here: https://hexdocs.pm/ex_aws_elastic_load_balancing/ExAws.ElasticLoadBalancingV2.html
    case ExAws.ElasticLoadBalancingV2.describe_load_balancers()
         |> request_with_config(credentials) do
      {:ok, %{:body => %{:load_balancers => albs}}} when is_list(albs) ->
        albs

      err ->
        Logger.error("Could not fetch list of ALB's from AWS. Details: #{inspect(err)}")
        []
    end
  end

  def describe_target_groups(vpc_id, credentials) do
    case ExAws.ElasticLoadBalancingV2.describe_target_groups()
         |> request_with_config(credentials) do
      {:ok, %{:body => %{:target_groups => groups}}} when is_list(groups) ->
        {:ok,
         groups
         |> Enum.filter(&(&1.vpc_id == vpc_id))}

      err ->
        Logger.error(
          "Could not fetch AWS target groups for VPC id #{inspect(vpc_id)} . Details: #{
            inspect(err)
          }"
        )

        {:error, err}
    end
  end

  @doc """
  Creates a new target group
  """
  def create_target_group_and_register(new_target_group_name, existing_vpc_id, port, credentials) do
    {:ok, target_group_arn} =
      create_target_group(new_target_group_name, existing_vpc_id, port, credentials)

    case register_worker_targets_with_group(target_group_arn, existing_vpc_id, port, credentials) do
      {:ok, %{:status_code => 200}} ->
        {:ok, target_group_arn}

      err ->
        Logger.error(
          "Could not register EC2 workers with newly created target group #{
            inspect(new_target_group_name)
          } . You may have to delete the target group manually to clean up. Details: #{
            inspect(err)
          }"
        )

        {:error, err}
    end
  end

  @doc """
  Creates a target group on the given VPC and port.
  Returns the ARN of the newly created target group.
  """
  def create_target_group(name, vpc_id, port, credentials) do
    case ExAws.ElasticLoadBalancingV2.create_target_group(name, vpc_id,
           port: port,
           protocol: "HTTP"
         )
         |> request_with_config(credentials) do
      {:ok, %{:body => xml_body}} ->
        # Return the new target group's ARN from the response body:
        {:ok, xpath(xml_body, ~x"//TargetGroupArn/text()") |> to_string()}

      err ->
        Logger.error("Could not create target group on AWS. Details: #{inspect(err)}")
        {:error, err}
    end
  end

  def add_target_group_healthcheck(target_group_arn, credentials) do
    case ExAws.ElasticLoadBalancingV2.modify_target_group(target_group_arn,
           heath_check_port: 80,
           health_check_protocol: "HTTP",
           health_check_path: "/healthz"
         )
         |> request_with_config(credentials) do
      {:ok, %{:status_code => 200}} ->
        :ok

      err ->
        Logger.error(
          "Could not add healthcheck for target group arn #{inspect(target_group_arn)} . Details: #{
            inspect(err)
          }"
        )

        {:error, err}
    end
  end

  @doc """
  Looks up the worker node EC2 instances for the given VPC,
  and then registers those under the given target group.
  The target group must exist already, and all instances must be in the 'running' state.
  """
  def register_worker_targets_with_group(target_group_arn, vpc_id, port, credentials) do
    {:ok, ec2_instance_ids} = get_worker_instances_by_vpc(vpc_id, credentials)

    ExAws.ElasticLoadBalancingV2.register_targets(
      target_group_arn,
      ec2_instance_ids |> Enum.map(fn id -> %{id: id, port: port} end)
    )
    |> request_with_config(credentials)
  end

  @doc """
  Looks up the worker node EC2 instances for the given VPC,
  and then de-registers them from the given target group.
  This is used when we need to take down a space or upgrade it (i.e. update its port)
  """
  def deregister_worker_targets_from_group(target_group_arn, vpc_id, credentials) do
    {:ok, ec2_instance_ids} = get_worker_instances_by_vpc(vpc_id, credentials)

    {:ok, %{:status_code => 200}} =
      ExAws.ElasticLoadBalancingV2.deregister_targets(
        target_group_arn,
        ec2_instance_ids |> Enum.map(fn id -> %{id: id} end)
      )
      |> request_with_config(credentials)
  end

  def change_target_group_default_port(target_group_arn, new_port, credentials) do
    case ExAws.ElasticLoadBalancingV2.modify_target_group(target_group_arn,
           port: new_port
         )
         |> request_with_config(credentials) do
      {:ok, %{:status_code => 200}} ->
        :ok

      err ->
        raise "Could not change target group default port on target group arn #{target_group_arn} . Details: #{
                inspect(err)
              }"
    end
  end

  @doc """
  Returns EC2 instance ID's of the worker nodes in the cluster by vpc id.
  This assumes the convention where worker instances are named like 'nodes-*' .
  """
  def get_worker_instances_by_vpc(vpc_id, credentials) do
    case ExAws.EC2.describe_instances(filters: ["vpc-id": [vpc_id], "tag:Name": ["nodes-*"]])
         |> request_with_config(credentials) do
      {:ok, %{body: xml_body}} ->
        # The trailing 'l' makes it return a list instead of only taking the first instance ID.
        {:ok, xpath(xml_body, ~x"//instanceId/text()"l) |> Enum.map(&Kernel.to_string/1)}

      err ->
        Logger.error(
          "Could not fetch list of EC2 instances to determine worker node instance id's. Details: #{
            inspect(err)
          }"
        )

        {:error, err}
    end
  end

  @doc """
  Creates a rule on the given VPC's ALB so that it forwards requests with the given host_pattern to the given target_group.
  """
  def create_alb_rule(listener_arn, rules_xml, host_pattern, target_group_arn, credentials) do
    {:ok, next_free_priority} = get_next_free_rule_priority(rules_xml, credentials)

    actions = [%{"TargetGroupArn" => target_group_arn, "Type" => "forward", "Order" => 1}]

    query =
      ExAws.ElasticLoadBalancingV2.create_rule(listener_arn, [], next_free_priority, actions)

    # For some reason I couldn't get ExAWS formatting the conditions correctly.
    # It kept leaving out the second '.member' after the 'Values'.
    # If this is not there, AWS returns 'Unexpected list element termination' or similar.
    # So we just build the conditions manually:
    updated_params =
      query.params
      |> Map.put("Conditions.member.1.Field", "host-header")
      |> Map.put("Conditions.member.1.Values.member.1", host_pattern)

    case Map.put(query, :params, updated_params)
         |> request_with_config(credentials) do
      {:ok, %{:status_code => 200}} ->
        :ok

      err ->
        Logger.error(
          "Could not create new ALB forwarding rule for host pattern #{inspect(host_pattern)}. Details: #{
            inspect(err)
          }"
        )

        {:error, err}
    end
  end

  def choose_random_node_port() do
    #  This is the acceptable range for NodePorts on AWS.
    {"success", %{"port" => Enum.random(30100..32767)}}
    # TODO: Check if this port already exists.
  end

  @doc """
  We can't create a new rule with a priority that already exists,
  so we have to ask AWS which rule is free.
  """
  def get_next_free_rule_priority(rules_xml, credentials) do
    # TODO: We may want to change this so it returns a random priority between say 1 and 1000
    # to prevent race conditions when multiple spaces are created at the same time.
    # This function could then block until a valid one is found.
    # It may also not be foolproof though. Needs further discussion.
    case xpath(rules_xml, ~x"//Priority/text()"l)
         |> Enum.map(&to_string/1)
         |> Enum.reject(&(&1 == "default")) do
      # No rules exist yet. This first one can have highest priority.
      [] ->
        {:ok, 1}

      rules when is_list(rules) ->
        {:ok,
         rules
         |> Enum.map(&(&1 |> Integer.parse() |> elem(0)))
         |> Enum.max()
         |> Kernel.+(1)}
    end
  end

  def describe_rules(listener_arn, credentials) do
    case ExAws.ElasticLoadBalancingV2.describe_rules(listener_arn: listener_arn)
         |> request_with_config(credentials) do
      {:ok, %{:body => xml_body, :status_code => 200}} ->
        {:ok, xml_body}

      err ->
        raise "Could not fetch list of existing ALB rules from AWS. Details: #{inspect(err)}"
    end
  end

  # Private functions

  defp alb_rule_exists?(rules_xml, host_name) do
    xpath(rules_xml, ~x"//HostHeaderConfig/Values/member/text()"l)
    |> Enum.any?(fn item -> item |> to_string() == host_name end)
  end

  defp get_alb_rule_arn(vpc_id, host_name, credentials) do
    {:ok, listener_arn} = get_alb_listener_by_vpc(vpc_id, credentials)
    {:ok, rules_xml} = describe_rules(listener_arn, credentials)

    # This xpath query finds the ARN of the rule associated with the given host_name.
    case xpath(
           rules_xml,
           ~x"//DescribeRulesResponse/DescribeRulesResult/Rules/member/Conditions/member/Values/member[text()='#{
             host_name
           }']/../../../../RuleArn/text()"
         )
         |> to_string() do
      "" ->
        Logger.warn(
          "Trying to delete ALB rule that doesn't exist: Couldn't find rule for host_name #{
            inspect(host_name)
          }"
        )

        {:ok, nil}

      arn ->
        {:ok, arn}
    end
  end

  def get_target_group_arn(vpc_id, host_name, credentials) do
    target_group_name = host_to_target_group_name(host_name)

    {:ok, target_groups} = describe_target_groups(vpc_id, credentials)

    case target_groups |> Enum.find(fn item -> item.target_group_name == target_group_name end) do
      %{:target_group_arn => arn} ->
        {:ok, arn}

      _ ->
        Logger.warn(
          "Could not find target group with name #{target_group_name} in VPC #{vpc_id}."
        )

        {:ok, nil}
    end
  end

  defp delete_target_group(target_group_arn, credentials) do
    case ExAws.ElasticLoadBalancingV2.delete_target_group(target_group_arn)
         |> request_with_config(credentials) do
      {:ok, _} ->
        :ok

      err ->
        Logger.warn(
          "Could not delete target group with arn #{target_group_arn} . Details: #{inspect(err)}"
        )

        {:error, err}
    end
  end

  defp delete_alb_rule(rule_arn, credentials) when is_binary(rule_arn) and rule_arn != "" do
    case ExAws.ElasticLoadBalancingV2.delete_rule(rule_arn)
         |> request_with_config(credentials) do
      {:ok, _} ->
        Logger.debug("Deleted ALB rule with ARN #{rule_arn}")
        :ok

      err ->
        Logger.error(
          "Could not delete ALB rule with arn #{inspect(rule_arn)} . Details: #{inspect(err)}"
        )

        :error
    end
  end

  defp delete_alb_rule(rule_arn, _credentials) do
    Logger.warn("Not deleting ALB rule because rule ARN is invalid: #{inspect(rule_arn)}")
    # Rule ARN is not a valid string. Don't do anything.
    :ok
  end

  defp request_with_config(payload, config) do
    # todo: add input validation
    ExAws.request(payload,
      region: config["aws_region"],
      access_key_id: config["aws_access_key_id"],
      secret_access_key: config["aws_access_key"]
    )
  end

  defp host_to_target_group_name(host_name) do
    # This is a convention we've chosen.
    String.replace(host_name, ".", "-")
    # AWS has a limit on the target group name length.
    |> String.slice(0, 31)
    # AWS doesn't allow target group names that start or end with hyphens.
    |> String.replace_leading("-", "")
    |> String.replace_trailing("-", "")
  end

  defp generate_dets_archive(input_data) do
    # todo: Error handling + file_path overrides.
    file_path = "/var/tmp/"

    Logger.info("Creating data archive...")
    # Get all files in /var/tmp
    var_files = File.ls!(file_path)

    # Filter out only dets files (starts with dev_ or mix_)
    dets_files =
      Enum.filter(var_files, fn x -> x =~ "#{Mix.env()}_" end) |> Enum.map(&String.to_charlist/1)

    # Generate filename based on input_data["s3_filename"] > kubernetes pod namespace > unknown
    backup_file_name =
      input_data["s3_filename"] ||
        case File.read("/var/run/secrets/kubernetes.io/serviceaccount/namespace") do
          {:ok, result} -> result
          {:error, _} -> "unknown-data-backup"
        end

    # Generate timestamp for s3 file. eg: "2019-26-06_10-48"
    backup_file_timestamp = Timex.now() |> Timex.format!("%Y-%d-%m_%H-%M", :strftime)

    # Zip all dets files
    :zip.create(
      "#{backup_file_name}-#{backup_file_timestamp}.zip",
      dets_files,
      cwd: '/var/tmp'
    )
  end
end

defmodule TrixtaLibrary.Core.AwsExpert.Beta do
  # TODO support other regions, this is currently locked to us-west-2
  def request_comprehend_medical(%{
        "text" => request_body,
        "credentials" => %{"aws_access_id" => aws_access_id, "aws_access_key" => aws_access_key}
      }) do
    config =
      ExAws.Config.new(:comprehend, [
        {:access_key_id, aws_access_id},
        {:secret_access_key, aws_access_key},
        {:region, "us-west-2"}
      ])

    config = Map.put(config, :host, "comprehendmedical.us-west-2.amazonaws.com")

    case ExAws.Request.request(
           :post,
           "https://comprehendmedical.us-west-2.amazonaws.com/",
           "{\"Text\":\"#{request_body}\", \"LanguageCode\":\"en\"}",
           [
             {"X-Amz-Target", "ComprehendMedical_20181030.DetectEntitiesV2"},
             {"Content-Type", "application/x-amz-json-1.1"}
           ],
           config,
           :comprehendmedical
         ) do
      {:ok, payload} -> {:ok, Map.get(payload, :body)}
      _ -> {:error, "Request Failed: AWS API error."}
    end
  end

  def parse_textract_data(response) do
    blocks = response["Blocks"]

    result_map =
      Enum.reduce(
        blocks,
        %{
          "block_map" => %{},
          "key_map" => %{},
          "value_map" => %{}
        },
        fn block, acc ->
          block_id = block["Id"]

          acc = Map.put(acc, "block_map", Map.put(acc["block_map"], block_id, block))

          block_type = block["BlockType"]

          if block_type == "KEY_VALUE_SET" do
            acc =
              if Enum.member?(block["EntityTypes"], "KEY") do
                Map.put(acc, "key_map", Map.put(acc["key_map"], block_id, block))
              else
                acc
              end

            acc =
              if Enum.member?(block["EntityTypes"], "VALUE") do
                Map.put(acc, "value_map", Map.put(acc["value_map"], block_id, block))
              else
                acc
              end
          else
            acc
          end
        end
      )

    {:ok, result_map}
  end

  def get_kv_relationship(%{
        "block_map" => block_map,
        "key_map" => key_map,
        "value_map" => value_map
      }) do
    Enum.map(key_map, fn key_block ->
      case find_value_block(key_block, value_map) do
        [value_block] ->
          key = get_text(key_block, block_map)
          value = get_text(value_block, block_map)
          %{"key" => key, "value" => value}

        _ ->
          %{"key" => "unknown", "value" => "unknown"}
      end
    end)
  end

  def find_value_block(
        {block_id,
         %{
           "Relationships" => relationships
         }},
        value_map
      ) do
    case relationships do
      [%{"Ids" => ids, "Type" => "VALUE"}, _children] ->
        Enum.filter(value_map, fn {value_block_id,
                                   %{
                                     "Id" => value_id
                                   }} ->
          value_id in ids
        end)

      other ->
        []
    end
  end

  def get_text(
        {block_id,
         %{
           "Relationships" => relationships
         }},
        blocks_map
      ) do
    case relationships do
      [_block, %{"Ids" => ids, "Type" => "CHILD"}] ->
        Enum.reduce(ids, [], fn id, acc ->
          word_blocks =
            Enum.filter(blocks_map, fn {block_id, _block_result} ->
              block_id == id
            end)

          result_text =
            Enum.map(word_blocks, fn {block_id, block_result} ->
              case block_result["BlockType"] do
                "WORD" ->
                  block_result["Text"]

                "SELECTION_ELEMENT" ->
                  if block_result["SelectionStatus"] == "SELECTED" do
                    "X"
                  else
                    ""
                  end
              end
            end)

          acc ++ [result_text]
        end)
        |> Enum.join(" ")

      [%{"Ids" => ids, "Type" => "CHILD"}] ->
        Enum.reduce(ids, [], fn id, acc ->
          word_blocks =
            Enum.filter(blocks_map, fn {block_id, _block_result} ->
              block_id == id
            end)

          result_text =
            Enum.map(word_blocks, fn {block_id, block_result} ->
              case block_result["BlockType"] do
                "WORD" ->
                  block_result["Text"]

                "SELECTION_ELEMENT" ->
                  if block_result["SelectionStatus"] == "SELECTED" do
                    "X"
                  else
                    ""
                  end
              end
            end)

          acc ++ [result_text]
        end)
        |> Enum.join(" ")

      other ->
        ""
    end
  end

  def get_text(other, blocks_map) do
    IO.inspect({"NO REL", other})
  end
end
