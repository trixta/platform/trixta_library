defmodule TrixtaLibrary.Core.PresetExpert do
  def report_differences(input) do
    all_presets =
      Enum.reduce(input, [], fn item, acc ->
        presets = get_in(item, ["config_info", "result"])

        if presets do
          acc ++ Enum.map(presets, fn item -> Map.get(item, "name") end)
        else
          acc
        end
      end)

    envs =
      Enum.map(input, fn item ->
        Map.get(item, "env_label")
      end)

    Enum.reduce(Enum.uniq(all_presets), %{}, fn preset, acc ->
      Map.merge(acc, %{
        preset => %{
          "exists" =>
            Enum.reduce(envs, %{}, fn env, acc ->
              {"ok", config_info} = TrixtaLibrary.Core.EnumExpert.find_in(input, "env_label", env)

              result = get_in(config_info, ["config_info", "result"])

              Map.merge(acc, %{
                String.pad_trailing(env, 20) =>
                  try do
                    case TrixtaLibrary.Core.EnumExpert.find_in(result, "name", preset) do
                      {"ok", _} -> true
                      _ -> false
                    end
                  rescue
                    err ->
                      false
                  end
              })
            end),
          "keys" =>
            Enum.reduce(envs, %{}, fn env, acc ->
              {"ok", config_info} = TrixtaLibrary.Core.EnumExpert.find_in(input, "env_label", env)

              result = get_in(config_info, ["config_info", "result"])

              Map.merge(acc, %{
                String.pad_trailing(env, 20) =>
                  try do
                    case TrixtaLibrary.Core.EnumExpert.find_in(result, "name", preset) do
                      {"ok", info} ->
                        info["key_hash"]

                      _ ->
                        nil
                    end
                  rescue
                    err ->
                      nil
                  end
              })
            end),
          "keys_values" =>
            Enum.reduce(envs, %{}, fn env, acc ->
              {"ok", config_info} = TrixtaLibrary.Core.EnumExpert.find_in(input, "env_label", env)

              result = get_in(config_info, ["config_info", "result"])

              Map.merge(acc, %{
                String.pad_trailing(env, 20) =>
                  try do
                    case TrixtaLibrary.Core.EnumExpert.find_in(result, "name", preset) do
                      {"ok", info} ->
                        info["key_value_hash"]

                      _ ->
                        nil
                    end
                  rescue
                    err ->
                      nil
                  end
              })
            end)
        }
      })
    end)
  end
end
