defmodule TrixtaLibrary.Core.EnumExpert do
  def member(enumerable, element) do
    case enumerable do
      enumerable when is_list(enumerable) ->
        case Enum.member?(enumerable, element) do
          true ->
            {"found", %{}}

          false ->
            {"not_found", %{}}
        end

      _ ->
        {"enumerable_not_a_list", %{}}
    end
  end

  def accumulate(acc, result) do
    {:ok,
     %{
       "result" =>
         (acc || []) ++
           [
             result
           ]
     }}
  end

  def group_by(enumerable, key_path_list, value_path_list)
      when is_list(key_path_list) and is_list(value_path_list) do
    {:ok,
     %{
       "result" =>
         Enum.group_by(enumerable, fn item -> get_in(item, key_path_list) end, fn item ->
           get_in(item, value_path_list)
         end)
     }}
  end

  def group_by(enumerable, key, value) do
    group_by(enumerable, [key], [value])
  end

  def uniq_by(enumerable, key_path_list)
      when is_list(key_path_list) do
    {:ok,
     %{
       "result" =>
         Enum.uniq_by(enumerable, fn item -> 
          get_in(item, key_path_list)
        end)
     }}
  end

  def uniq_by(enumerable, key) do
    uniq_by(enumerable, [key])
  end

  def find_in(enumerable, field_to_evaluate, value_to_match) do
    case Enum.find(enumerable, fn item ->
           case item do
             item when is_map(item) ->
               item[field_to_evaluate] == value_to_match
           end
         end) do
      nil ->
        {"not_found", nil}

      item when is_map(item) ->
        {"ok", item}

      item ->
        {"ok", %{"result" => item}}
    end
  end
end
