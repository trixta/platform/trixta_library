defmodule TrixtaLibrary.Core.ReactionExpert do

  @action_payload "action_payload"

  def resend_reactions(resend_list, role_name, flow_source_data_request_id) do
    # E.g item from list:

    # [
    # 0   "0.3367.0",
    # 1   1_612_285_572,
    # 2   "flow name",
    # 3   "######-b525-4f53-87e1-b3a6339bdceb",
    # 4   "many",
    # 5   "role[#######-05e8-4c60-aa92-24484ccd320a]",
    # 6   "reaction_name",
    # 7   "~",
    # 8   payload
    # ]

    Enum.each(resend_list, fn item ->
      append_topic_and_target_id =
        TrixtaSpaceWeb.Endpoint.broadcast!(
          Enum.join(["space:", role_name, ":", flow_source_data_request_id]),
          Enum.at(item, 6),
          Enum.at(item, 8)
        )
    end)

    {"ok", %{}}
  end

  def check_for_missing_reaction(resend_list, reaction_to_find) do
    case Enum.map(resend_list, fn item -> Enum.at(item, 6) end)
         |> Enum.member?(reaction_to_find) do
      true ->
        {"found", %{}}

      false ->
        {"missing", %{}}
    end
  end

  def convert_to_reactions_map(input) when is_list(input) do
    Enum.map(input, fn item -> item["data"] |> JSON.decode!() end)
    |> Enum.group_by(fn item -> Enum.at(item, 6) end)
  end

  def unique_timestamps(input) when is_map(input) do
    Enum.flat_map(input, fn {_k, v} -> v end)
    |> Enum.map(fn list -> Enum.at(list, 1) end)
    |> Enum.uniq()
    |> Enum.sort()
  end

  def get_reaction(
    reaction_details,
    prevent_composite \\ false
  )

  def get_reaction(
    %{
      "identity" => nil,
      "password" => nil,
      "token" => nil,
      "role" => role,
      "reaction" => reaction,
      "params" => nil
    } = reaction_details,
    prevent_composite
  ) do
    is_authed = false

    {role_without_group, group} =
      case role do
        [role, group] ->
          {role, group}

        [role] ->
          {role, nil}
      end

    action_payload = %{}

    get_reaction_internal(
      role_without_group,
      group,
      reaction,
      nil,
      is_authed,
      action_payload,
      0,
      reaction_details,
      prevent_composite
    )
  end

  def get_reaction(
        %{
          "identity" => identity,
          "password" => password,
          "token" => token,
          "role" => role,
          "reaction" => reaction,
          "params" => params
        } = reaction_details,
        prevent_composite
      ) do
    is_authed =
      if is_nil(token) do
        case TrixtaSpace.AgentsSupervisor.login_agent(identity, password) do
          {:ok, %{agent_id: agent_id, jwt: jwt}} ->
            true

          error_details ->
            false
        end
      else
        case TrixtaSpace.MigrationAgentsInterface.verify_jwt(token) do
          {:ok, claims} ->
            true

          {:error, _error} ->
            false
        end
      end

    {role_without_group, group} =
      case role do
        [role, group] ->
          {role, group}

        [role] ->
          {role, nil}
      end

    action_payload =
      if params[@action_payload] do
        params[@action_payload] |> Jason.decode!()
      else
        if params do
          params
        else
          %{}
        end
      end

    get_reaction_internal(
      role_without_group,
      group,
      reaction,
      identity,
      is_authed,
      action_payload,
      0,
      reaction_details,
      prevent_composite
    )
  end

  defp get_reaction_internal(
         role,
         group,
         reaction,
         identity,
         is_authed,
         action_payload,
         recursive_protection,
         reaction_details,
         prevent_composite
       ) do
    if recursive_protection < 2 do
      case (if group do
              if can_join_role?("space:#{role}[#{group}]", identity, is_authed) do
                get_reaction_data(
                  "r_out/many/#{role}[#{group}]:#{reaction}/~",
                  reaction_details,
                  prevent_composite
                )
              else
                %{:status => :error, :error => :unauthorized}
              end
            else
              if can_join_role?("space:#{role}", identity, is_authed) do
                get_reaction_data(
                  "r_out/many/#{role}:#{reaction}/~",
                  reaction_details,
                  prevent_composite
                )
              else
                %{:status => :error, :error => :unauthorized}
              end
            end) do
        %{result: [], status: :ok} ->
          # this means that the reaction is completely empty, we need to init the reaction with a flow

          TrixtaSpaceWeb.SpaceChannel.run_flow_by_reaction(
            reaction,
            %{
              @action_payload => %{
                "role" => role,
                "group" => group,
                "reaction" => reaction,
                "identity" => identity
              }
            },
            role,
            nil,
            nil,
            "__trixta_init_reactions_#{role}"
          )

          # attempt to get reaction again (now after init)
          get_reaction_internal(
            role,
            group,
            reaction,
            identity,
            is_authed,
            action_payload,
            recursive_protection + 1,
            reaction_details,
            prevent_composite
          )

        %{
          result: [
            %{
              "initial_data" => [
                "__trixta_action",
                [
                  target_flow,
                  target_role,
                  target_reaction_input
                ]
              ]
            }
          ],
          status: :ok
        } ->
          {outcome_name, outcome_data} =
            TrixtaSpaceWeb.SpaceChannel.run_flow_by_reaction(
              reaction,
              %{
                @action_payload => action_payload,
                "return_reaction_override" => [target_role, target_reaction_input]
              },
              target_role,
              nil,
              nil,
              target_flow
            )

          %{
            :status => :ok,
            :result =>
              if is_nil(outcome_data) do
                %{}
              else
                outcome_data
              end
          }

        normal ->
          normal
      end
    else
      %{
        :status => :error,
        :error =>
          "flow '__trixta_init_reactions_#{role}' does not exist, or the flow does not correctly init reaction '#{reaction}'"
      }
    end
  end

  defp get_reaction_data(key, reaction_details, prevent_composite) do
    result =
      case TrixtaSpaceWeb.Reaction.Helpers.match_trixta_r_out_key(
             key,
             reaction_details,
             prevent_composite
           ) do
        {"none", _result} ->
          %{}

        {"ok", %{"result" => data}} ->
          data
      end

    %{:status => :ok, :result => result}
  end

  defp can_join_role?(_topic, "root-agent", is_authed) do
    is_authed
  end

  defp can_join_role?(topic, agent_id, is_authed) do
    role = TrixtaFlowCode.Roles.get_role_from_topic(topic)

    if role.unrestricted do
      true
    else
      TrixtaSpaceWeb.SpaceChannel.authorized_via_controller?(role, topic, agent_id)
    end
  end
end
