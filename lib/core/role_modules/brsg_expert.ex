defmodule TrixtaLibrary.Core.BrsgExpert do
  def transform_to_brsg_format(kv_pairs) do
    Enum.map(kv_pairs, fn %{"key" => key, "value" => value} ->
      case key do
        "Provider Form ID" ->
          %{"PROVIDER_FORM_ID" => value}

        "Provider Precert Request Date" ->
          %{"PROVIDER_FORM_REQUEST_DATE" => value}

        "Primary Diagnosis ICD-10 Code (text)" ->
          %{"DIAGNOSIS_ICD_CODE" => value}

        "Primary CPT Code" ->
          %{"PROCEDURE_CPT_CODE" => value}

        "Surgical Approach" ->
          %{"PLANNED_SURGICAL_PROCEDURE" => value}

        "Surgery is cervical corpectomy with removal of at least half of the vertebral body" ->
          %{"SURGERY_IS_CERVICAL_CORPECTOMY_REMOVING_HALF_OF_VERTEBRAL_BODY" => value}

        "Spinal level of requested surgery" ->
          %{"SURGICAL_PROCEDURE_SPINAL_LEVEL" => value}

        "There is a presence of symptoms such as neck or cervico-brachial pain with findings of weakness, myelopathy or sensory deficit" ->
          %{"SYMPTOMS_OF_NECK_OR_CERVICOBRACHIAL_PAIN" => value}

        "All other reasonable sources of pain have been ruled out" ->
          %{"ALL_OTHER_REASONABLE_SOURCES_OF_PAIN_RULED_OUT" => value}

        "Radiology report provided (MRI or CT) (not just surgeon summary) - accept attachment" ->
          %{"IMAGING_REPORT_PROVIDED" => value}

        "Radiology/Imaging Report indicated findings (drop down)" ->
          %{"IMAGING_REPORT_FINDINGS" => value}

        "Spinal level that MRI / CT show to be an issue" ->
          %{"IMAGING_IDENTIFIED_SPINAL_LEVEL" => value}

        "Documented physicial exam, including neurologic exam, performed or reviewed by surgeon with findings" ->
          %{"SURGEON_EXAM_IDENTIFIED_FINDINGS" => value}

        "Spinal level that document exam findings indicate to be an issue" ->
          %{"SURGEON_EXAM_IDENTIFIED_SPINAL_LEVEL" => value}

        "Conservative Therapy Waiver" ->
          %{"REASON_TO_WAIVE_CONSERVATIVE_THERAPY" => value}

        "Patient has failed conservative therapy, inclusive of the noted activities, for at least 6 weeks within the past year" ->
          %{"FAILED_CONSERVATIVE_THERAPY_LIST" => value}

        "Where appropriate: has associated anxiety or depression been identified and managed?" ->
          %{"ANXIETY_DEPRESSION_IDENTIFIED_AND_MANAGED" => value}

        "Patient's activities of daily living are limited by persistent neck or cervico-brachial pain" ->
          %{
            "ACTIVITIES_OF_DAILY_LIVING_LIMITED_BY_PERSISTENT_NECK_OR_CERVICOBRACHIAL_PAIN" =>
              value
          }

        "Spinal Instrumentation Procedure CPT Code" ->
          %{"IMPLANT_CPT_CODE" => value}

        "Spinal Implant Manufacturer / Brand Name" ->
          %{"IMPLANT_MANUFACTURER_NAME" => value}

        "Spinal Implant Type" ->
          %{"SPINAL_IMPLANT_TYPE" => value}

        "Graft Name" ->
          %{"GRAFT_COMMON_NAME" => value}

        "Type of Graft" ->
          %{"GRAFT_TYPE" => value}

        "Graft Details Selection" ->
          %{"GRAFT_DETAILS" => value}

        "Instrumentation Name" ->
          %{"IMPLANT_COMMON_NAME" => value}

        "Instrumentation requested is a synthetic cervical cage/ spacer" ->
          %{"INSTRUMENTATION_IS_SYNTHETIC_CERVICAL_CAGE_SPACER" => value}

        "Condition Requiring Synthetic Cervical Cage/Spacer" ->
          %{"REASON_FOR_INSTRUMENTATION" => value}

        "Cervical Corpectomy Requested to Treat Condition (only Required for Surgical Approach of Corpectomy)" ->
          %{"CORPECTOMY_REQUESTED" => value}

        "Add Additional Implant Y/N (up to 5)" ->
          %{"ADDITIONAL_IMPLANT_YN" => value}

        _ ->
          upper_key = String.upcase(key) |> String.replace(" ", "_")
          %{upper_key => value}
      end
    end)
  end
end
