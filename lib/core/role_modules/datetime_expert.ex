defmodule TrixtaLibrary.Core.DateTimeExpert do
  def epoch_in_nano_from_iso8601(iso8601_date) do
    epoch_from_iso8601(iso8601_date, :nanosecond)
  end

  def epoch_in_milli_from_iso8601(iso8601_date) do
    epoch_from_iso8601(iso8601_date, :millisecond)
  end

  def epoch_in_seconds_from_iso8601(iso8601_date) do
    epoch_from_iso8601(iso8601_date, :second)
  end

  defp epoch_from_iso8601(iso8601_date, time_unit) do
    {:ok, datetime_result, utc_offset} = DateTime.from_iso8601(iso8601_date)
    {"ok", %{"result" => DateTime.to_unix(datetime_result, time_unit)}}
  end
end
