defmodule TrixtaLibrary.Core.ActionReactionExpert do
  @moduledoc """
  Enables sharing of action / reaction details for anonymous access.
  """
  alias TrixtaLibrary.Core.SpaceExpert
  alias TrixtaSpace.RolesSupervisor
  require Logger

  @shared_action "shared_action:"
  @shared_reaction "shared_reaction:"

  @doc """
  Generates and saves an access token for the given action or reaction,
  so that its details can be accessed anonymously.
  """
  def share_action_reaction_details(params) do
    case key(params) do
      {:ok, key} ->
        token = new_access_token()
        SpaceExpert.save_space_kv_pair(key, Map.put(params, "access_token", token))
        {"success", %{"token" => token}}

      _ ->
        Logger.error(
          "Cannot share action / reaction details. Invalid role or action / reaction name."
        )

        {"error", %{"details" => "Invalid role or action / reaction name."}}
    end
  end

  def fetch_action_reaction_details(%{"role" => role_name, "token" => token} = params) do
    with [{^role_name, %{} = role_details}] <-
           RolesSupervisor.role_details(role_name),
         {:ok, key} <- key(params),
         {"success", %{"access_token" => ^token}} <- SpaceExpert.get_single_state_record(key),
         {"success", %{} = action_reaction_details} <-
           fetch_action_reaction_details(params, role_details) do
      {"success", action_reaction_details}
    else
      _err ->
        {"error",
         %{"details" => "Role / action / reaction not found, or access token was invalid."}}
    end
  end

  def fetch_action_reaction_details(_) do
    {"error", %{"details" => "Role / action / reaction not found, or access token was invalid."}}
  end

  defp fetch_action_reaction_details(%{"action" => action_name}, %{
         :contract_actions => actions
       }) do
    case actions do
      %{^action_name => action_details} ->
        {"success", action_details}

      _ ->
        :error
    end
  end

  defp fetch_action_reaction_details(%{"reaction" => reaction_name}, %{
         :contract_reactions => reactions
       }) do
    case reactions do
      %{^reaction_name => reaction_details} ->
        {"success", reaction_details}

      _ ->
        :error
    end
  end

  defp fetch_action_reaction_details(_, _) do
    {"error", %{"details" => "Role / action / reaction not found, or access token was invalid."}}
  end

  defp key(%{"role" => role_name, "action" => action_name}) do
    {:ok, @shared_action <> role_name <> ":" <> action_name}
  end

  defp key(%{"role" => role_name, "reaction" => reaction_name}) do
    {:ok, @shared_reaction <> role_name <> ":" <> reaction_name}
  end

  defp key(_params) do
    Logger.error(
      "Cannot generate key for shared action / reaction. Invalid role or action / reaction name."
    )

    :error
  end

  defp new_access_token() do
    UUID.uuid4()
  end
end
