defmodule TrixtaLibrary.Core.MnesiaExpert do
  def full_backup(path \\ Application.get_env(:mnesia, :dir)) do
    {:ok, filename} = TrixtaSpace.Cluster.Mnesia.backup(path)

    {"ok", to_string(filename)}
  end

  def restore_steps(path) do
    TrixtaSpace.Cluster.Mnesia.rename_backup(
      [node()],
      to_charlist(path),
      temp_rename_path()
    )

    tables_to_skip = :mnesia.system_info(:tables) |> List.delete(:schema) |> List.delete(TrixtaSpace.Stores.StepsStore.Types.Step)

    {:atomic, _} =
      :mnesia.restore(temp_rename_filepath(), [
        {:recreate_tables, [TrixtaSpace.Stores.StepsStore.Types.Step]},
        {:skip_tables, tables_to_skip}
      ])

    {"ok", %{}}
  end

  def full_restore(path) do
    TrixtaSpace.Cluster.Mnesia.rename_backup(
      [node()],
      to_charlist(path),
      temp_rename_path()
    )

    {:atomic, _} =
      :mnesia.restore(temp_rename_filepath(), [
        {:default_op, :recreate_tables}
      ])

    {"ok", %{}}
  end

  defp temp_rename_path() do
    Application.get_env(:mnesia, :dir)
  end

  defp temp_rename_filepath() do
    (to_string(Application.get_env(:mnesia, :dir)) <> ".BUPTMP") |> to_charlist()
  end
end
