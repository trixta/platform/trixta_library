defmodule TrixtaLibrary.Core.CsvExpert do
  def to_csv(map_list) when is_list(map_list) do
    CSV.Encoding.Encoder.encode(map_list, headers: true) |> Enum.to_list() |> Enum.join()
  end
end
