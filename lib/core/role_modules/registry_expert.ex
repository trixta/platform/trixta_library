defmodule TrixtaLibrary.Core.RegistryExpert do
  def lookup(registry, key) do
    result =
      case Registry.lookup(registry, key) do
        [] ->
          []

        entries ->
          Enum.map(entries, fn {pid, value} ->
            [
              :erlang.pid_to_list(pid)
              |> to_string()
              |> String.trim_leading("<")
              |> String.trim_trailing(">")
            ] ++ Tuple.to_list(value)
          end)
      end

    {"ok", result}
  end

  def match(registry, key, pattern) do
    result =
      case Registry.match(registry, key, pattern) do
        [] ->
          []

        entries ->
          Enum.map(entries, fn {pid, value} ->
            [
              :erlang.pid_to_list(pid)
              |> to_string()
              |> String.trim_leading("<")
              |> String.trim_trailing(">")
            ] ++ Tuple.to_list(value)
          end)
      end

    {"ok", result}
  end

  def match(registry, key, pattern, guards) do
    result =
      case Registry.match(registry, key, pattern, guards) do
        [] ->
          []

        entries ->
          Enum.map(entries, fn {pid, value} ->
            [
              :erlang.pid_to_list(pid)
              |> to_string()
              |> String.trim_leading("<")
              |> String.trim_trailing(">")
            ] ++ Tuple.to_list(value)
          end)
      end

    {"ok", result}
  end
end
