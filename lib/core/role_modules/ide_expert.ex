defmodule TrixtaLibrary.Core.IdeExpert do
  def prefix_flows_with_roles(flows) do
    role_actions =
      TrixtaSpaceWeb.Role.Helpers.role_action_details()
      |> elem(1)
      |> Map.get("roles")
      |> Enum.reduce([], fn x, acc ->
        [%{"name" => x[:name], "actions" => Enum.map(x["actions"], fn y -> y["name"] end)}] ++ acc
      end)

    flow_results =
      Enum.reduce(flows, [], fn %{id: flow_id, name: _flow_name, tags: tags, version: version},
                                acc ->
        flows_with_roles =
          Enum.reduce(role_actions, [], fn %{"name" => name, "actions" => actions}, acc ->
            if Enum.member?(actions, flow_id) do
              [
                %{
                  id: flow_id,
                  name: "[#{name}]:#{flow_id}",
                  role: name,
                  tags: tags,
                  version: version
                }
              ] ++ acc
            else
              acc
            end
          end)

        if length(flows_with_roles) == 0 do
          [%{id: flow_id, name: "[---]:#{flow_id}", role: "...", tags: tags, version: version}] ++
            acc
        else
          flows_with_roles ++ acc
        end
      end)

    roles = TrixtaSpaceWeb.Role.Helpers.role_names() |> elem(1) |> Map.keys()

    results = [
      %{
        "name" => "Space",
        "type" => "space",
        "context" => "space",
        "children" => [
          %{
            "name" => "Roles",
            "type" => "roles",
            "context" => "space",
            "children" =>
              Enum.map(roles, fn role ->
                role_details = TrixtaFlowCode.Roles.get_role_from_topic("space:#{role}")

                %{
                  "name" => role,
                  "type" => "role",
                  "context" => "space",
                  "children" => [
                    %{
                      "name" => "Actions",
                      "type" => "actions",
                      "context" => role,
                      "children" =>
                        Enum.map(role_details.contract_actions, fn {action_key, action_value} ->
                          %{
                            "name" => action_key,
                            "type" => "action",
                            "context" => role,
                            "children" =>
                              if action_value["handler"]["type"] == "flow" do
                                [
                                  %{
                                    "name" => action_value["handler"]["name"],
                                    "type" => "flow",
                                    "children" => []
                                  }
                                ]
                              else
                                []
                              end
                          }
                        end)
                    },
                    %{
                      "name" => "Reactions",
                      "type" => "reactions",
                      "context" => role,
                      "children" =>
                        Enum.map(role_details.contract_reactions, fn {reaction_key,
                                                                      _reaction_value} ->
                          %{
                            "name" => reaction_key,
                            "type" => "reaction",
                            "context" => role,
                            "children" =>
                              Enum.filter(flow_results, fn %{:id => flow_id} ->
                                flow_id == "#{role}_#{reaction_key}"
                              end)
                              |> Enum.map(fn %{:id => flow_id} ->
                                %{"name" => flow_id, "type" => "flow", "children" => []}
                              end)
                          }
                        end)
                    }
                  ]
                }
              end) ++
                [
                  %{
                    "name" => "???",
                    "type" => "role",
                    "context" => "space",
                    "children" =>
                      Enum.filter(flow_results, fn %{:role => role_name} -> role_name == "..." end)
                      |> Enum.sort_by(fn flow -> flow.id end)
                      |> Enum.map(fn flow ->
                        %{"name" => flow.id, "type" => "flow", "children" => []}
                      end)
                  }
                ]
          },
          %{
            "name" => "Agents",
            "type" => "agents",
            "context" => "space",
            "children" => []
          },
          %{
            "name" => "Presets",
            "type" => "presets",
            "context" => "space",
            "children" => []
          }
        ]
      }
    ]

    {"ok",
     %{
       "results" => results
     }}
  end

  def generate_space_overview_flow(flows, search_term \\ nil) do
    role_actions =
      TrixtaSpaceWeb.Role.Helpers.role_action_details()
      |> elem(1)
      |> Map.get("roles")
      |> Enum.reduce([], fn x, acc ->
        [%{"name" => x[:name], "actions" => Enum.map(x["actions"], fn y -> y["name"] end)}] ++ acc
      end)

    flow_results =
      Enum.reduce(flows, [], fn %{id: flow_id, name: _flow_name, tags: tags, version: version},
                                acc ->
        flows_with_roles =
          Enum.reduce(role_actions, [], fn %{"name" => name, "actions" => actions}, acc ->
            if Enum.member?(actions, flow_id) do
              [
                %{
                  id: flow_id,
                  name: "[#{name}]:#{flow_id}",
                  role: name,
                  tags: tags,
                  version: version
                }
              ] ++ acc
            else
              acc
            end
          end)

        if length(flows_with_roles) == 0 do
          [%{id: flow_id, name: "[---]:#{flow_id}", role: "...", tags: tags, version: version}] ++
            acc
        else
          flows_with_roles ++ acc
        end
      end)

    roles = TrixtaSpaceWeb.Role.Helpers.role_names() |> elem(1) |> Map.keys()

    role_children =
      Enum.reduce(roles, %{}, fn role, acc ->
        role_details = TrixtaFlowCode.Roles.get_role_from_topic("space:#{role}")

        role_actions =
          Enum.reduce(role_details.contract_actions, %{}, fn {action_key, action_value}, acc ->
            local_flow =
              if action_value["handler"]["type"] == "flow" do
                %{
                  "#{action_value["handler"]["name"]}__flow" => %{
                    "inner" => [],
                    "settings" => %{
                      "context" => %{
                        "input_type" => "value",
                        "value" => %{"string" => action_key}
                      },
                      "name" => %{
                        "input_type" => "value",
                        "value" => %{"string" => action_value["handler"]["name"]}
                      }
                    },
                    "use" => "Trixta.Steps.TrixtaFlow.V_0_0_1"
                  }
                }
              else
                %{}
              end

            Map.merge(
              %{
                "#{action_key}__action_#{role}" => %{
                  "inner" => Map.keys(local_flow),
                  "settings" => %{
                    "context" => %{
                      "input_type" => "value",
                      "value" => %{"string" => role}
                    },
                    "name" => %{
                      "input_type" => "value",
                      "value" => %{"string" => action_key}
                    }
                  },
                  "use" => "Trixta.Steps.TrixtaAction.V_0_0_1"
                }
              },
              local_flow
            )
            |> Map.merge(acc)
          end)

        role_reactions =
          Enum.reduce(role_details.contract_reactions, %{}, fn {reaction_key, _reaction_value},
                                                               acc ->
            local_flow =
              Enum.filter(flow_results, fn %{:id => flow_id} ->
                flow_id == "#{role}_#{reaction_key}"
              end)
              |> Enum.reduce(%{}, fn %{:id => flow_id}, acc ->
                Map.merge(
                  acc,
                  %{
                    "#{flow_id}__flow" => %{
                      "inner" => [],
                      "settings" => %{
                        "context" => %{
                          "input_type" => "value",
                          "value" => %{"string" => reaction_key}
                        },
                        "name" => %{"input_type" => "value", "value" => %{"string" => flow_id}}
                      },
                      "use" => "Trixta.Steps.TrixtaFlow.V_0_0_1"
                    }
                  }
                )
              end)

            Map.merge(
              %{
                "#{reaction_key}__reaction_#{role}" => %{
                  "inner" => Map.keys(local_flow),
                  "settings" => %{
                    "context" => %{
                      "input_type" => "value",
                      "value" => %{"string" => role}
                    },
                    "name" => %{
                      "input_type" => "value",
                      "value" => %{"string" => reaction_key}
                    }
                  },
                  "use" => "Trixta.Steps.TrixtaReaction.V_0_0_1"
                }
              },
              local_flow
            )
            |> Map.merge(acc)
          end)

        Map.merge(
          acc,
          %{
            role => %{
              "inner" => ["actions__#{role}", "reactions__#{role}"],
              "settings" => %{
                "context" => %{
                  "input_type" => "value",
                  "value" => %{"string" => "space"}
                },
                "name" => %{"input_type" => "value", "value" => %{"string" => role}}
              },
              "use" => "Trixta.Steps.TrixtaRole.V_0_0_1"
            },
            "actions__#{role}" => %{
              "inner" =>
                role_details.contract_actions
                |> Map.keys()
                |> Enum.map(fn name -> "#{name}__action_#{role}" end),
              "settings" => %{
                "context" => %{
                  "input_type" => "value",
                  "value" => %{"string" => role}
                },
                "name" => %{
                  "input_type" => "value",
                  "value" => %{"string" => "actions"}
                }
              },
              "use" => "Trixta.Steps.TrixtaActions.V_0_0_1"
            },
            "reactions__#{role}" => %{
              "inner" =>
                role_details.contract_reactions
                |> Map.keys()
                |> Enum.map(fn name -> "#{name}__reaction_#{role}" end),
              "settings" => %{
                "context" => %{
                  "input_type" => "value",
                  "value" => %{"string" => role}
                },
                "name" => %{
                  "input_type" => "value",
                  "value" => %{"string" => "reactions"}
                }
              },
              "use" => "Trixta.Steps.TrixtaReactions.V_0_0_1"
            }
          }
        )
        |> Map.merge(role_actions)
        |> Map.merge(role_reactions)
      end)

    role_groups =
      Enum.group_by(roles, fn role ->
        role_details = TrixtaFlowCode.Roles.get_role_from_topic("space:#{role}")
        Enum.find(role_details.tags, fn tag -> String.starts_with?(tag, "group:") end)
      end)
      |> Enum.reduce(%{}, fn {role_group, role_list}, acc ->
        role_group =
          if role_group do
            (role_group
             |> String.split(":")
             |> List.last()) <>
              "__role_group"
          else
            "ungrouped__role_group"
          end

        Map.merge(
          acc,
          %{
            role_group => %{
              "inner" => role_list,
              "result_function" => %{"input_type" => "sequence_summary"},
              "use" => "Trixta.Steps.Sequence.V_0_0_1"
            }
          }
        )
      end)

    unattached_flows =
      Enum.filter(flow_results, fn %{:role => role_name} -> role_name == "..." end)
      |> Enum.sort_by(fn flow -> flow.id end)
      |> Enum.reduce(%{}, fn flow, acc ->
        Map.merge(
          acc,
          %{
            "#{flow.id}__flow" => %{
              "inner" => [],
              "settings" => %{
                "context" => %{
                  "input_type" => "value",
                  "value" => %{"string" => "???"}
                },
                "name" => %{"input_type" => "value", "value" => %{"string" => flow.id}}
              },
              "use" => "Trixta.Steps.TrixtaFlow.V_0_0_1"
            }
          }
        )
      end)

    results =
      Map.merge(
        %{
          "space" => %{
            "inner" => ["roles", "presets", "agents", "???"],
            "result_function" => %{"input_type" => "sequence_summary"},
            "use" => "Trixta.Steps.Sequence.V_0_0_1"
          },
          "agents" => %{
            "inner" => [],
            "result_function" => %{"input_type" => "sequence_summary"},
            "use" => "Trixta.Steps.TrixtaAgents.V_0_0_1"
          },
          "presets" => %{
            "inner" => [],
            "result_function" => %{"input_type" => "sequence_summary"},
            "use" => "Trixta.Steps.TrixtaPresets.V_0_0_1"
          },
          "roles" => %{
            "inner" => Map.keys(role_groups),
            # "inner" => roles ++ ["???"],
            "settings" => %{
              "context" => %{
                "input_type" => "value",
                "value" => %{"string" => "space"}
              },
              "name" => %{"input_type" => "value", "value" => %{"string" => "roles"}}
            },
            "use" => "Trixta.Steps.TrixtaRoles.V_0_0_1"
          }
        },
        role_children
      )
      |> Map.merge(%{
        "???" => %{
          "inner" => Map.keys(unattached_flows),
          "settings" => %{
            "context" => %{
              "input_type" => "value",
              "value" => %{"string" => "space"}
            },
            "name" => %{"input_type" => "value", "value" => %{"string" => "???"}}
          },
          "use" => "Trixta.Steps.TrixtaRole.V_0_0_1"
        }
      })
      |> Map.merge(unattached_flows)
      |> Map.merge(role_groups)

    if search_term do
      underscored_search_term =
        String.downcase(search_term)
        |> String.replace(" ", "_")

      matching_results =
        Enum.filter(results, fn {k, v} ->
          String.contains?(k, underscored_search_term)
        end)
        |> Enum.into(%{})

      parents_result = include_parents(matching_results, results)

      {"ok",
       %{
         "results" => Map.merge(matching_results, parents_result)
       }}
    else
      {"ok",
       %{
         "results" => results
       }}
    end
  end

  def include_parents(children, all_steps) do
    direct_parents =
      Enum.reduce(children, %{}, fn item, acc ->
        case include_parent(item, all_steps) do
          {step_id, step_details} ->
            Map.put_new(acc, step_id, step_details)

          nil ->
            acc
        end
      end)

    if length(Map.keys(direct_parents)) == 0 do
      direct_parents
    else
      Map.merge(direct_parents, include_parents(direct_parents, all_steps))
    end
  end

  def include_parent({step_id, _step_details}, all_steps) do
    Enum.find(all_steps, fn {k, v} ->
      step_in_inner(step_id, v) ||
        step_in_branch(step_id, v)
    end)
  end

  def step_in_inner(step_id, %{"inner" => inner}) do
    Enum.member?(inner, step_id)
  end

  def step_in_inner(_step_id, _step) do
    false
  end

  def step_in_branch(step_id, %{"branch" => branch}) do
    Enum.any?(branch, fn item ->
      case item do
        [conditions, targets] ->
          Enum.member?(targets, step_id)

        [conditions] ->
          false
      end
    end)
  end

  def step_in_branch(_step_id, _step) do
    false
  end
end
