defmodule TrixtaLibrary.Core.TestExpert do
    
    def assert_reaction(records, [role_reaction_name, expected_count]) when is_list(records) and is_number(expected_count) and is_binary(role_reaction_name) do
        # E.g [
        #     %{
        #       "agent_id" => "viewer_first",
        #       "flow_name" => "viewer_set_session_status",
        #       "group" => "706ab76c-a192-4c8c-9676-7e4bd94e268e",
        #       "input" => %{"initial_data" => "active", "settings" => nil},
        #       "role" => "viewer",
        #       "ts" => 1617631433
        #     },
        #     %{
        #       "agent_id" => "viewer_first",
        #       "flow_name" => "viewer_set_host_entered_session",
        #       "group" => "706ab76c-a192-4c8c-9676-7e4bd94e268e",
        #       "input" => %{"initial_data" => nil, "settings" => nil},
        #       "role" => "viewer",
        #       "ts" => 1617631430
        #     
        #   ], ["rolexyz_reactionnamexyz", 1]}
        
        case Enum.count(records, fn %{"flow_name" => flow_name} = item ->  
                flow_name == role_reaction_name
            end) do
            ^expected_count -> 
                {"passed", %{
                    "message" => "'#{role_reaction_name}' test passed. Correct count: #{expected_count}"
                }}

            other_count ->
                {"failed", %{
                    "message" => "'#{role_reaction_name}' test failed. Count expected: #{expected_count}, instead found: #{other_count}"
                }}

        end
    end

    def get_counts(results) do

        {"ok", Enum.reduce(results, %{
            "passed" => 0,
            "failed" => 0,
            "other" => 0
        }, fn item, acc -> 
            Map.merge(acc, 
                case item do
                    %{"result" => ["passed", _]} ->
                        %{"passed" => acc["passed"] + 1}

                    %{"result" => ["failed", _]} ->
                        %{"failed" => acc["failed"] + 1}

                    _ ->
                        %{"other" => acc["other"] + 1}
                end)
        end)}
    end
end