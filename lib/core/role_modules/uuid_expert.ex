defmodule TrixtaLibrary.Core.UuidExpert do
  def uuid4() do
    {"ok", %{"uuid" => UUID.uuid4()}}
  end
end
