defmodule TrixtaLibrary.Core.LoggerExpert do
    require Logger

    def debug(message) do
        Logger.debug(message)
        {"ok", %{}}
    end

    def info(message) do
        Logger.info(message)
        {"ok", %{}}
    end

    def warn(message) do
        Logger.warn(message)
        {"ok", %{}}
    end

    def error(message) do
        Logger.error(message)
        {"ok", %{}}
    end
end