defmodule TrixtaLibrary.Core.ApplicationExpert do
  def get_loki_config() do
    loki_settings = Application.get_env(:logger, :loki_logger)

    {:ok,
     %{
       "result" => %{
         "level" => Keyword.get(loki_settings, :level),
         "max_buffer" => Keyword.get(loki_settings, :max_buffer),
         "loki_host" => Keyword.get(loki_settings, :loki_host),
         "application_label" => Keyword.get(loki_settings, :loki_labels) |> Map.get(:application)
       }
     }}
  end

  def put_loki_config(%{
        "level" => level,
        "max_buffer" => max_buffer,
        "loki_host" => loki_host,
        "application_label" => application_label
      }) do
    result =
      Application.put_env(:logger, :loki_logger,
        level: String.to_existing_atom(level),
        format: {TrixtaUtils.Logging.LokiFormatter, :format_log},
        metadata: [:module, :function, :override_timestamp],
        max_buffer: max_buffer,
        loki_labels: %{application: application_label},
        loki_host: loki_host,
        loki_scope_org_id: "trixta"
      )

    {result, %{}}
  end

  def get_logger_backends() do
    logger_backends = Application.get_env(:logger, :backends)

    converted_backends =
      [] ++
        case Enum.member?(logger_backends, LokiLogger) do
          true ->
            ["loki"]

          false ->
            []
        end ++
        case Enum.member?(logger_backends, :console) do
          true ->
            ["console"]

          false ->
            []
        end

    {:ok, %{"result" => converted_backends}}
  end

  def set_logger_backends(backends) do
    converted_backends =
      [] ++
        case Enum.member?(backends, "loki") do
          true ->
            [LokiLogger]

          false ->
            []
        end ++
        case Enum.member?(backends, "console") do
          true ->
            [:console]

          false ->
            []
        end

    result = Application.put_env(:logger, :backends, converted_backends)

    {result, %{}}
  end

  def restart_logger() do
    result =
      case Application.stop(:logger) do
        :ok ->
          Application.start(:logger)

        _ ->
          :error
      end

    {result, %{}}
  end
end
