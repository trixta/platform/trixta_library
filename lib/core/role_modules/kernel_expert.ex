defmodule TrixtaLibrary.Core.KernelExpert do
  def is_equal(left, right) do
    if left == right do
      {"is_equal", %{}}
    else
      {"not_equal", %{}}
    end
  end

  def rem(dividend, divisor) do
    case dividend do
      dividend when is_number(dividend) ->
        case Kernel.rem(dividend, divisor) do
          0 ->
            {"zero", %{"result" => 0}}

          result ->
            {"remainder", %{"result" => result}}
        end

      _ ->
        {"dividend_not_a_number", %{}}
    end
  end

  def greater_than(first, second) do
    case first > second do
      true ->
        {"greater_than", %{}}

      _ ->
        {"not", %{}}
    end
  end

  def less_than(first, second) do
    case first < second do
      true ->
        {"less_than", %{}}

      _ ->
        {"not", %{}}
    end
  end

  def is_empty(first) do
    case is_nil(first) do
      true ->
        {"is_empty", %{}}

      _ ->
        {"not", %{}}
    end
  end

  def are_any_empty(list) do
    are_any_empty = Enum.reduce(list, fn item, acc -> is_nil(item) || acc end)

    case are_any_empty do
      true ->
        {"is_empty", %{}}

      _ ->
        {"not", %{}}
    end
  end
end
