defmodule TrixtaLibrary.Core.ClosedIntervalExpert do
  import ClosedIntervals

  def previous_in_interval(interval_data, timestamp) do
    case length(interval_data) do
      0 ->
        {"none", %{"result" => nil}}

      1 ->
        if List.first(interval_data) |> Enum.at(1) <= timestamp do
          {"ok", %{"result" => List.first(interval_data)}}
        else
          {"none", %{"result" => nil}}
        end

      _ ->
        {previous, _next} = get_interval(get_closed_intervals(interval_data), [nil, timestamp])

        case previous do
          :"-inf" ->
            {"none", %{"result" => previous}}

          _ ->
            {"ok", %{"result" => previous}}
        end
    end
  end

  def next_in_interval(interval_data, timestamp) do
    case length(interval_data) do
      0 ->
        {"none", %{"result" => nil}}

      1 ->
        if List.first(interval_data) |> Enum.at(1) > timestamp do
          {"ok", %{"result" => List.first(interval_data)}}
        else
          {"none", %{"result" => nil}}
        end

      _ ->
        {_previous, next} = get_interval(get_closed_intervals(interval_data), [nil, timestamp])

        case next do
          :"+inf" ->
            {"none", %{"result" => next}}

          _ ->
            {"ok", %{"result" => next}}
        end
    end
  end

  defp get_closed_intervals(interval_data) do
    order = fn a, b -> Enum.at(a, 1) <= Enum.at(b, 1) end
    eq = fn a, b -> Enum.at(a, 1) == Enum.at(b, 1) end

    from(interval_data, order: order, eq: eq)
  end
end
