defmodule TrixtaLibrary.Core.ProcessExpert do
  def reply_to_reactions(state_details, reaction, reply_tag, reply_data) do
    deferred_reactions = Map.get(state_details, "deferred_reactions", [])

    {"ok", items} =
      TrixtaLibrary.Core.ListExpert.filter_all_by_map_key_value(
        deferred_reactions,
        "reaction",
        reaction
      )

    result =
      Enum.map(items, fn %{"from" => from, "input" => _input, "reaction" => _reaction_name} ->
        GenServer.reply(from, {reply_tag, %{"result" => reply_data}})
      end)

    {"ok", %{"result" => result}}
  end

  @doc """
    Collect and clear garabge of unused data in memory
  """
  def garbage_collect_all_processes() do
    Enum.each(:erlang.processes(), fn pid -> :erlang.garbage_collect(pid) end)
  end

end
