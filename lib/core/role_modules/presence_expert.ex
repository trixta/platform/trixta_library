defmodule TrixtaLibrary.Core.PresenceExpert do
  def count_by_topic(topic) do
    Phoenix.Tracker.list(TrixtaSpaceWeb.Tracker, topic) |> length()
  end

  @doc """
    Returns a list of present agents for specific roles
  """
  def get_all_by_topic(topic) do
    Phoenix.Tracker.list(TrixtaSpaceWeb.Tracker, topic)
  end

  @doc """
    Returns a list of all present without duplicates
  """
  def get_all_by_topic_uniq(topic) do
    # return count and list of present agents map, no duplicates
    {
      "ok",
      Enum.uniq_by(
        Phoenix.Tracker.list(TrixtaSpaceWeb.Tracker, topic),
        fn {key, _value} -> key end
      )
    }
  end

  @doc """
    Returns a list of all authed present without duplicates, and all anon users
  """
  def get_all_by_topic_with_uniq_auth(topic) do
    conn_users = Phoenix.Tracker.list(TrixtaSpaceWeb.Tracker, topic)
    auth_users = Enum.filter(conn_users, fn {key, _value} -> key != nil end)
    anon_users = Enum.filter(conn_users, fn {key, _value} -> key == nil end)
    auth_uniq = Enum.uniq(auth_users, fn {key, _value} -> key end)

    {
      "ok",
      auth_uniq ++ anon_users
    }
  end

  @doc """
    Updates the phoenix socket meta data
  """
  def update_presence_metadata(pid, topic, key, data) do
    Phoenix.Tracker.update(
      TrixtaSpaceWeb.Tracker,
      pid |> IEx.Helpers.pid(),
      topic,
      key,
      fn meta -> Map.merge(meta, data) end
    )
  end

  @doc """
    Takes Enum and filters data based off a filter key
  """
  def filter_list_by_key(presence_list, filter_key) do
    Enum.filter(presence_list, fn {id, value} -> Map.has_key?(value, filter_key) end)
  end

  @doc """
    Sort presence list by property and sort order
  """
  def sort_list_by_key(presence_list, sort_key, sort_order \\ :desc) do
    Enum.sort_by(presence_list, fn item -> item[sort_key] end, sort_order)
  end

  @doc """
    Get user in presence list by id
  """
  def get_by_tuple_key(presence_list, id) do
    case item = Enum.find(presence_list, fn {key, _value} -> key == id end) do
      nil ->
        {"not_found", %{}}

      _ ->
        {"ok",
         %{
           "result" => item
         }}
    end
  end

  @doc """
  Returns a list of present agents for all roles.
  """
  def get_all_ungrouped(exclude_root_agent \\ false) when is_boolean(exclude_root_agent) do
    # Phoenix Presence doesn't allow us to query all channels in one go,
    # so we fetch a list of all roles and combine their presence lists.
    results =
      TrixtaSpace.RolesSupervisor.list_role_ids()
      |> Enum.reduce(%{}, fn role_id, acc ->
        acc
        |> Map.put(
          role_id,
          (fn ->
             TrixtaSpaceWeb.Presence.list("space:#{role_id}")
             |> simplify_structure()
             |> reject_root_agent(exclude_root_agent)
           end).()
        )
      end)

    # We've decided not to include the non-role channels (root, and session) for now,
    # because their presence data is not as interesting for us at the moment.

    {"ok", results}
  end

  def get_by_role_ungrouped(role_id, exclude_root_agent \\ false)
      when is_boolean(exclude_root_agent) do
    results =
      TrixtaSpaceWeb.Presence.list("space:#{role_id}")
      |> simplify_structure()
      |> reject_root_agent(exclude_root_agent)

    {if length(results) > 0 do
       "ok"
     else
       "none"
     end, results}
  end

  def get_by_role_agent_ungrouped(role_id, agent_id, exclude_root_agent \\ false)
      when is_boolean(exclude_root_agent) do
    results =
      TrixtaSpaceWeb.Presence.get_by_key("space:#{role_id}", agent_id)
      |> simplify_get_by_key_structure()
      |> reject_root_agent(exclude_root_agent)

    {if length(results) > 0 do
       "ok"
     else
       "none"
     end, results}
  end

  def get_present(nil) do
    raise("PresenceExpert: requested number of players is nil")
  end

  def get_present(topic, count, opts \\ [at_random: true]) do
    if Keyword.get(opts, :at_random) do
      {:ok,
       %{
         "results" =>
           Enum.take_random(Phoenix.Presence.list(TrixtaSpaceWeb.Presence, topic), count)
           |> Enum.map(fn {k, details} -> %{k => details} end)
       }}
    else
      raise("PresenceExpert: currently only supports at_random")
    end
  end

  def temp_host_anon_exclude_rules() do
    {"ok",
     %{
       "exclude_rules" => [
         %{
           "exclude_from" => "list",
           "field_to_check" => "data",
           "value_to_match" => nil
         },
         %{
           "exclude_from" => "both",
           "field_to_check" => "host_check",
           "value_to_match" => true
         }
       ]
     }}
  end

  def presence_list(role, group, exclude_rules \\ nil, order_by \\ nil, sort_order \\ :desc) do
    role_with_group =
      if group do
        "#{role}[#{group}]"
      else
        role
      end

    {"ok", presence_list} =
      TrixtaLibrary.Core.PresenceExpert.get_all_by_topic_with_uniq_auth(
        "space:#{role_with_group}"
      )

    presence_list =
      Enum.map(presence_list, fn {key, value} ->
        Map.put(value, "id", key)
      end)

    # excludes
    presence_count =
      Enum.reduce(exclude_rules, presence_list, fn exclude_rule, acc ->
        if exclude_rule["exclude_from"] in ["count", "both"] do
          Enum.reject(acc, fn item ->
            Map.get(item, exclude_rule["field_to_check"]) == exclude_rule["value_to_match"]
          end)
        else
          acc
        end
      end)
      |> Enum.count()

    presence_list =
      Enum.reduce(exclude_rules, presence_list, fn exclude_rule, acc ->
        if exclude_rule["exclude_from"] in ["list", "both"] do
          Enum.reject(acc, fn item ->
            Map.get(item, exclude_rule["field_to_check"]) == exclude_rule["value_to_match"]
          end)
        else
          acc
        end
      end)

    presence_list =
      TrixtaLibrary.Core.PresenceExpert.sort_list_by_key(presence_list, order_by, sort_order)
      |> Enum.take(50)
      

    {"ok",
     %{
       "presence_count" => presence_count,
       "presence_list" => presence_list,
       "role" => role_with_group
     }}
  end

  defp simplify_structure(presences) do
    Map.values(presences)
    |> Enum.map(fn item ->
      item.metas
    end)
    |> List.flatten()
  end

  defp simplify_get_by_key_structure(presence) do
    result = presence.metas
  end

  defp reject_root_agent(presences, false) do
    presences
  end

  defp reject_root_agent(presences, true) do
    Enum.reject(presences, fn item -> item.agent_id == "root-agent" end)
  end
end
