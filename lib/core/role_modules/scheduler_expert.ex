defmodule TrixtaLibrary.Core.SchedulerExpert do
  @moduledoc """
  Helper funtions for Scheduler Expert
  """
  require Logger
  require Quantum

  import Crontab.CronExpression

  @doc """
  Creates a scheduled job defined by the pass in config.
  Example input_data:
  {
    "identity": "root-agent",
    "password": "12345",
    "topic": "space:trixta_app_builder",
    "event": "scheduler_add_job",
    "args": {
      "password": "12345",
      "identity": "root-agent",
      "topic": "...",
      "event": "list",
      "args": {
        "job_name":"testing_job",
        "job_schedule":"* * * * *"
      }
    }
  }
  """
  def add_job(_source_data, input_data) do
    Logger.debug("Creating scheduled job with Config: #{inspect(input_data)}")

    # Creating job entry
    try do
      job_name = input_data["args"]["job_name"]

      TrixtaLibrary.Scheduler.new_job()
      |> Quantum.Job.set_name(String.to_atom(job_name))
      |> Quantum.Job.set_schedule(
        Crontab.CronExpression.Parser.parse!(input_data["args"]["job_schedule"])
      )
      |> Quantum.Job.set_task(fn ->
        # Job function: Posting to TrixtaSpace via http
        HTTPotion.post(
          "http://localhost:#{System.get_env("TRIXTA_SPACE_PORT") || 4000}/send/action",
          body: input_data |> Poison.encode!(),
          headers: ["Content-Type": "application/json"]
        )
      end)
      |> TrixtaLibrary.Scheduler.add_job()

      Logger.info("Successfully created JOB #{job_name}")
      {"success", %{"job_name" => job_name}}
    rescue
      e ->
        Logger.error("Failed to create job. Logging error: #{inspect(e)}")
        {"error", %{"details" => e}}
    end
  end

  @doc """
  Deletes a job with the given name
  """
  def delete_job(_source_data, input_data) do
    Logger.debug("Deleting scheduled job with name: #{input_data["job_name"]}")

    input_data["job_name"]
    |> String.to_atom()
    |> TrixtaLibrary.Scheduler.delete_job()

    # Quantum responds with the same message regardless of whether the job existed or not.
    {"success", %{}}
  end

  @doc """
  Lists all jobs with their configs
  """
  def list_jobs(_source_data, input_data) do
    Logger.debug("Listing all scheduled jobs")

    jobs =
      TrixtaLibrary.Scheduler.jobs()
      |> Enum.into(%{})
      |> Map.keys()

    {"success", %{"jobs" => jobs}}
  end
end
