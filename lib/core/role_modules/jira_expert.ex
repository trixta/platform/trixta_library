defmodule TrixtaLibrary.Core.JiraExpert do
  def create_description_from_action(action) do
    {:ok,
     %{
       "result" =>
         ~s(#{action["description"]} \n\n *API Details* \n\n #{action["name"]} \n\n *Request Payload* \n\n {code:json} \n\n #{
           action["request_schema"]
         } \n\n {code} \n\n *Details* \n\n #{action["notes"]})
     }}
  end
end
