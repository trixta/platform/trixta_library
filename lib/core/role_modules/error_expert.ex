defmodule TrixtaLibrary.Core.ErrorExpert do
  require Logger

  def log_error(data) do
    Logger.error(inspect(data))
    data
  end
end
