defmodule TrixtaLibrary.Core.ListExpert do
  def reject_all_by_map_key_value(input, key, condition) when is_list(input) and is_binary(key) do
    result =
      Enum.reject(input, fn %{^key => key} ->
        key == condition
      end)

    {"ok", result}
  end

  def filter_all_by_map_key_value(input, key, condition) when is_list(input) and is_binary(key) do
    result =
      Enum.filter(input, fn %{^key => key} ->
        key == condition
      end)

    {"ok", result}
  end

  def list_to_tuple(list) when is_list(list) do
    {"ok", List.to_tuple(list)}
  end

  def empty_list() do
    {"ok", []}
  end

  def fast_prepend_to_list(list, element) do
    [element | list]
  end

  def slow_append_to_list(list, element) do
    list ++ [element]
  end

  def concat_nilable(list_one, list_two) do
    {"ok", Enum.concat(list_one || [], list_two || [])}
  end

  def find_index(lst, target) do
    {"ok",
      Enum.find_index(lst, fn x -> x == target end)
    }
  end

end
