defmodule TrixtaLibrary.Core.RunningFlowsExpert do
  def lookup_by_flow_id(flow_id) do
    {_, result} =
      TrixtaLibrary.Core.RegistryExpert.lookup(
        :running_flows,
        flow_id
      )

    {"ok", result}
  end

  def lookup_all() do
    {"ok",
     Enum.reduce(TrixtaSpace.FlowsSupervisor.list_flow_ids(), [], fn flow_id, acc ->
       (TrixtaLibrary.Core.RegistryExpert.lookup(
          :running_flows,
          flow_id
        )
        |> elem(1)) ++ acc
     end)}
  end

  def match_by_correlation_id(flow_id, correlation_id) do
    {"ok",
     TrixtaLibrary.Core.RegistryExpert.match(
       :running_flows,
       flow_id,
       {:_, :_, :_, correlation_id, :_, :_}
     )
     |> elem(1)}
  end

  def match_by_tag(flow_id, tag) do
    {"ok",
     TrixtaLibrary.Core.RegistryExpert.match(
       :running_flows,
       flow_id,
       {:_, :_, :_, :_, tag, :_}
     )
     |> elem(1)}
  end

  def match_by_args(flow_id, args) do
    {"ok",
     TrixtaLibrary.Core.RegistryExpert.match(
       :running_flows,
       flow_id,
       {:_, :_, :_, :_, :_, args}
     )
     |> elem(1)}
  end

  def match_before_time(flow_id, timestamp) do
    guards = [{:<, :"$1", timestamp}]

    {"ok",
     TrixtaLibrary.Core.RegistryExpert.match(
       :running_flows,
       flow_id,
       {:"$1", :_, :_, :_, :_, :_},
       guards
     )
     |> elem(1)}
  end

  def match_after_time(flow_id, timestamp) do
    guards = [{:>, :"$1", timestamp}]

    {"ok",
     TrixtaLibrary.Core.RegistryExpert.match(
       :running_flows,
       flow_id,
       {:"$1", :_, :_, :_, :_, :_},
       guards
     )
     |> elem(1)}
  end

  def match_custom(flow_id, [
        time_started,
        flow_id,
        flow_version_name,
        flow_instance_id,
        tag,
        args
      ]) do
    # use atom :_ to wildcard match
    {"ok",
     TrixtaLibrary.Core.RegistryExpert.match(
       :running_flows,
       flow_id,
       {time_started, flow_id, flow_version_name, flow_instance_id, tag, args}
     )
     |> elem(1)}
  end

  def flow_state_by_correlation_id(flow_id, correlation_id) do
    case TrixtaLibrary.Core.RunningFlowsExpert.match_by_correlation_id(flow_id, correlation_id) do
      {"ok",
       [
         [
           _pid,
           ts,
           name,
           version,
           correlation_id,
           _tag,
           args
         ]
       ]} ->
        {"ok",
         %{
           "correlation_id" => correlation_id,
           "ts" => ts,
           "name" => name,
           "version" => version,
           "state" =>
             args
             |> Map.get("store_agent_pid")
             |> IEx.Helpers.pid()
             |> Agent.get(fn state -> state end),
           "args" =>
             args
             |> Map.get("args"),
           "group" =>
             args
             |> Map.get("group")
         }}

      {"ok", []} ->
        {"none", %{}}
    end
  end

  def convert_to_run_flow_info(input) do
    {"ok",
     %{
       "input" => input["args"],
       "options" => [
         # effect only so that it is fire and forget
         effect_only: true,
         resume: input["resume"],
         init_state: input["state"]
       ],
       "name" => input["name"],
       "version" => input["version"]
     }}
  end

end
