defmodule TrixtaLibrary.Core.MapExpert do
  def map_to_serializable_list(map) do
    Enum.map(map, fn {key, value} ->
      if is_map(value) do
        Map.merge(value, %{"name" => key})
      else
        nil
      end
    end)
    |> Enum.filter(fn x -> !is_nil(x) end)
  end

  def struct_to_serializable_map(struct) do
    Map.from_struct(struct) |> Poison.encode!() |> Poison.decode!()
  end

  def flatten_map(map, separator \\ "_") when is_map(map) do
    result =
      Enum.reduce(map, %{}, fn {key, value}, acc ->
        Map.merge(
          acc,
          Map.new(
            case value do
              value when is_map(value) ->
                Enum.map(value, fn {inner_key, inner_value} ->
                  {"#{key}#{separator}#{inner_key}", inner_value}
                end)

              value ->
                [{key, value}]
            end
          )
        )
      end)

    {:ok,
     case Enum.any?(result, fn {key, value} -> is_map(value) end) do
       true ->
         flatten_map(result, separator)

       false ->
         result
     end}
  end

  def get_module_from_map_via_step_name(map, step_name) do
    {"ok", Map.get(map, Module.concat([Macro.camelize(step_name)]))}
 end
end
