defmodule TrixtaLibrary.Core.MathExpert do
  def multiply(input, value) do
    {"ok", input * value}
  end

  def divide(input, value) do
    {"ok", input / value}
  end

  def add(input, value) do
    {"ok", input + value}
  end

  def subtract(input, value) do
    {"ok", input - value}
  end
end
