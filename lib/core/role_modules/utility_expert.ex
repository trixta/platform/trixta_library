defmodule TrixtaLibrary.Core.UtilityExpert do
  def echo_input(input) when is_map(input) do
    {"echo", input}
  end

  def echo_input(input) when is_binary(input) do
    {"echo", %{"result" => input}}
  end

  def echo_input(input) do
    {"echo", %{"result" => input}}
  end

  def echo_input(input, field) when is_binary(field) do
    {"echo_with_field", %{field => input}}
  end

  def input_as_tag(input) when is_list(input) do
    {"list", %{"result" => input}}
  end

  def input_as_tag(input) when is_map(input) do
    {"map", input}
  end

  def input_as_tag(input) when is_binary(input) do
    {input, %{}}
  end

  def input_as_tag(input) when is_atom(input) do
    {Atom.to_string(input), %{}}
  end

  def input_as_tag(input) when is_integer(input) do
    {Integer.to_string(input), %{}}
  end

  def input_as_tag(input) when is_float(input) do
    {Float.to_string(input), %{}}
  end

  def input_as_tag(input) when is_boolean(input) do
    {Atom.to_string(input), %{}}
  end

  def input_as_tag(input) do
    {"type_unsupported", %{}}
  end

  def input_as_tag() do
    {"nil", %{}}
  end

  @doc """
    The following function is for us to temporarily switch out existing IO.inspect calls, 
    so we don't log in the shortterm, this function should not be chosen ordinarily
  """
  def temp_inspect(input) do
    {:ok, input}
  end

  @doc """
  Generate random string based on the given legth. It is also possible to generate certain type of randomise string using the options below:
  * :all - generate alphanumeric random string
  * :alpha - generate nom-numeric random string
  * :numeric - generate numeric random string
  * :upcase - generate upper case non-numeric random string
  * :downcase - generate lower case non-numeric random string
  ## Example
      iex> Iurban.String.randomizer(20) //"Je5QaLj982f0Meb0ZBSK"
  """
  def randomizer(length, type \\ :all) do
    alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    numbers = "0123456789"

    lists =
      cond do
        type == :alpha -> alphabets <> String.downcase(alphabets)
        type == :numeric -> numbers
        type == :upcase -> alphabets
        type == :downcase -> String.downcase(alphabets)
        true -> alphabets <> String.downcase(alphabets) <> numbers
      end
      |> String.split("", trim: true)

    do_randomizer(length, lists)
  end

  @doc false
  defp get_range(length) when length > 1, do: (1..length)
  defp get_range(length), do: [1]

  @doc false
  defp do_randomizer(length, lists) do
    get_range(length)
    |> Enum.reduce([], fn(_, acc) -> [Enum.random(lists) | acc] end)
    |> Enum.join("")
  end
end
