defmodule TrixtaLibrary.Core.LokiExpert do
  @regex_exp ~r{\w+=[[:graph:]]+[[:blank:]]|\w+=[[:graph:]]+}

  def query_loki(
        from_date,
        to_date,
        filter_list,
        %{
          "application_label" => application_label,
          "loki_host" => loki_host
        }
      )
      when is_list(filter_list) do
    url = ~s(#{loki_host}/loki/api/v1/query_range)

    case HTTPotion.get(url,
           query: %{
             "query" => ~s({application="#{application_label}"} #{Enum.join(filter_list, " ")}),
             "start" => from_date,
             "end" => to_date,
             # 5000 is the current limit for loki, anything greater causes an error
             "limit" => 5000
           }
         ) do
      %HTTPotion.ErrorResponse{message: message} ->
        {:error, message}

      %HTTPotion.Response{status_code: status_code, body: response_body} ->
        case status_code do
          200 ->
            %{
              "status" => "success",
              "data" => %{
                "resultType" => "streams",
                "result" => result
              }
            } = Poison.decode!(response_body)

            {:ok,
             case result do
               [%{"stream" => stream, "values" => values} | _] ->
                 values
                 |> Enum.map(fn [ts, log] ->
                   remainder =
                     Regex.split(@regex_exp, log, include_captures: false, trim: true)
                     |> List.to_string()

                   key_values =
                     Regex.scan(@regex_exp, log)
                     |> Enum.map(fn [item] ->
                       [label, value] = String.trim(item) |> String.split("=", parts: 2)
                       {label, value}
                     end)
                     |> Map.new()
                     |> Map.merge(%{"log__" => String.trim(remainder), "ts" => ts})
                 end)

               [] ->
                 nil
             end}

          404 ->
            {:not_found, %{"response" => response_body}}

          _any ->
            {:error, %{"response" => response_body}}
        end
    end
  end
end
