defmodule TrixtaLibrary.Core.SocketExpert do
  def set_additional(pid_as_string, key, value) when is_binary(pid_as_string) and is_binary(key) or is_list(key) do
    send(IEx.Helpers.pid(pid_as_string), {:set_additional, key, value})

    {
      "ok",
      %{}
    }
  end

  def get_additional(pid_as_string, key) when is_binary(pid_as_string) do
    result = GenServer.call(IEx.Helpers.pid(pid_as_string), {:get_additional, key}, 5000)

    {
      "ok",
      result
    }
  end
end
