defmodule TrixtaLibrary.Core.DetsExpert do
  #TODO this whole module name is no longer applicable in post refactor version.

  def match_trixta_r_out_object(key_prefix) do
    raise "OBSOLETE: use TrixtaSpaceWeb.Reaction.Helpers.match_trixta_r_out_object"
  end

  def match_trixta_r_out_object(table, key_prefix) do
    raise "OBSOLETE: use TrixtaSpaceWeb.Reaction.Helpers.match_trixta_r_out_object"
  end

  def match_trixta_r_out_object(
        _table,
        key_prefix,
        %{
          "identity" => _identity,
          "password" => _password,
          "token" => _token,
          "role" => _role,
          "reaction" => _reaction,
          "params" => _params
        } = reaction_details
      ) do
    raise "OBSOLETE: use TrixtaSpaceWeb.Reaction.Helpers.match_trixta_r_out_object"
  end

  # __trixta_composite

  def match_trixta_r_out_key(key, reaction_details, prevent_composite) do
    raise "OBSOLETE: use TrixtaSpaceWeb.Reaction.Helpers.match_trixta_r_out_key"
  end

  def match_trixta_r_out_key(_table, key, reaction_details, prevent_composite) do
    raise "OBSOLETE: use TrixtaSpaceWeb.Reaction.Helpers.match_trixta_r_out_key"
  end

  def space_table() do
    raise "OBSOLETE: remove 'space_table'from flow"
  end

  def sync_primary do
    raise "OBSOLETE: remove 'sync_primary' from flow"
  end

  def sync_table(table_name) do
    raise "OBSOLETE: remove 'sync_table' from flow"
  end
end
