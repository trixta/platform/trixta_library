defmodule TrixtaLibrary.Core.FlowHistoryExpert do
  @moduledoc """
  Flow History Expert responsible for parsing, handling and querying flow information from ElasticSearch

  ---------------
  get_flow_history:
    Requires:
      input_data["to_date"] # start date for query
      input_data["from_date"] # end date for query
      input_data["flow_instance_id"] # flow instance id for query
      input_data["space_name"] # namespace for the query, this is important as ES is shared per-cluster

    Returns:
      list of {"cat", "flow_instance_id", "@timestamp"}


  ---------------
  get_flow_history_detail:
    Requires:
      input_data["flow_instance_id"] # flow instance id for query
      input_data["space_name"] # namespace for the query, this is important as ES is shared per-cluster

    Returns:
      list of {"cat", "@timestamp", "args"}
  """
  require Logger

  # Public functions
  def get_flow_history(_source_data, input_data) do
    case generate_query(input_data) do
      {"error", details} ->
        Logger.error(details)
        {"error", details}

      query ->
        case TrixtaMetrics.raw_search(%{"data" => Poison.encode!(query)}) do
          %{"took" => _took_value} = results ->
            {"success", results}

          {:error, reason} ->
            Logger.info("Could not lookup flow history. Reason: #{reason}}")

          _ ->
            Logger.error("Invalid response from TrixtaMetrics while querying flow history data")

            {"error",
             %{
               "details" => "Invalid response from TrixtaMetrics while querying flow history data"
             }}
        end
    end
  end

  def get_flow_history_loki(
        from_date,
        to_date,
        flow_name,
        loki_config
      ) do
    TrixtaLibrary.Core.LokiExpert.query_loki(
      from_date,
      to_date,
      [~s(|="t_flow_name=#{flow_name}")],
      loki_config
    )
  end

  def get_flow_history_detail(_source_data, input_data) do
    case generate_detail_query(input_data) do
      {"error", details} ->
        Logger.error(details)
        {"error", details}

      query ->
        case TrixtaMetrics.raw_search(%{"data" => Poison.encode!(query)}) do
          %{"took" => _took_value} = results ->
            {"success", results}

          _ ->
            Logger.error("Invalid response from TrixtaMetrics while querying flow history data")

            {"error",
             %{
               "details" => "Invalid response from TrixtaMetrics while querying flow history data"
             }}
        end
    end
  end

  ### Private functions

  # Generating query with to_date and from_date
  defp generate_query(%{
         "to_date" => to_date,
         "from_date" => from_date,
         "flow_name" => flow_name,
         "space_name" => space_name
       }) do
    # Build up ElasticSearch query
    %{
      "size" => 20,
      "query" => %{
        "bool" => %{
          "must" => [
            %{
              "term" => %{
                "kubernetes.namespace_name.keyword" => space_name
              }
            },
            %{
              "term" => %{
                "flow_name.keyword" => flow_name
              }
            },
            %{
              "range" => %{
                "@timestamp": %{
                  "gt" => from_date,
                  "lt" => to_date
                }
              }
            }
          ]
        }
      },
      "sort" => [%{"@timestamp" => %{"order" => "desc"}}],
      "_source" => ["cat", "flow_instance_id", "@timestamp", "args"]
    }
  end

  # Generating base flow_history query
  defp generate_query(%{
         "flow_name" => flow_name,
         "space_name" => space_name
       }) do
    # Build up ElasticSearch query
    %{
      "size" => 20,
      "query" => %{
        "bool" => %{
          "must" => [
            %{
              "term" => %{
                "kubernetes.namespace_name.keyword" => space_name
              }
            },
            %{
              "term" => %{
                "flow_name.keyword" => flow_name
              }
            }
          ]
        }
      },
      "sort" => [%{"@timestamp" => %{"order" => "desc"}}],
      "_source" => ["cat", "flow_instance_id", "@timestamp", "args"]
    }
  end

  defp generate_query(input_data) do
    {"error",
     %{
       "details" =>
         "Invalid input_data for query generation: #{input_data}. Please check documentation."
     }}
  end

  defp generate_detail_query(%{
         "flow_instance_id" => flow_instance_id,
         "space_name" => space_name
       }) do
    # Build up ElasticSearch query
    %{
      "size" => 20,
      "query" => %{
        "bool" => %{
          "must" => [
            %{
              "term" => %{
                "kubernetes.namespace_name.keyword" => space_name
              }
            },
            %{
              "term" => %{
                "flow_instance_id.keyword" => flow_instance_id
              }
            }
          ]
        }
      },
      "sort" => [%{"@timestamp" => %{"order" => "desc"}}],
      "_source" => ["cat", "@timestamp", "args"]
    }
  end

  defp generate_detail_query(input_data) do
    {"error",
     %{
       "details" =>
         "Invalid input_data for query generation: #{input_data}. Please check documentation."
     }}
  end
end
