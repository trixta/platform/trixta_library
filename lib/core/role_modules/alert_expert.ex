defmodule TrixtaLibrary.Core.AlertExpert do
  @moduledoc """
  Helper functions for alerts
  """
  alias TrixtaSpace.{AgentServer}
  require Logger

  # Struct logic And Parsing
  defstruct [
    :id,
    :name,
    :dataset,
    :address,
    :type,
    :condition,
    :value,
    :query,
    :select_fields,
    :aggregation,
    :period,
    :contract_event,
    :cooldown_timeout_seconds,
    :last_notification_sent_time
  ]

  @type t :: %__MODULE__{
          id: String.t(),
          name: String.t(),
          dataset: String.t(),
          address: String.t(),
          type: boolean(),
          condition: String.t(),
          value: String.t(),
          query: String.t(),
          select_fields: String.t(),
          aggregation: String.t(),
          period: String.t() | nil,
          contract_event: String.t() | nil,
          cooldown_timeout_seconds: integer() | nil,
          last_notification_sent_time: String.t() | nil
        }

  @spec contract_event_spec(String) :: String
  def contract_event_spec(a) when a in ["value_count", "avg", "max", "min", "sum"], do: a
  def contract_event_spec(_), do: nil

  @spec condition_spec(String) :: String
  def condition_spec(a) when a in ["greater_than", "less_than"], do: a
  def condition_spec(_), do: nil

  @spec dataset_spec(String) :: String
  def dataset_spec(a) when a in ["contract_logs", "transaction"], do: a
  def dataset_spec(_), do: nil

  @doc """
  Converts a given json structure into an alert_struct to be used within the AlertsExpert
  """
  defp json_to_alert_struct(alert_json) do
    # Given a json structure, attempt to decode it using Poison into our AlertExpert struct.
    case Poison.decode(alert_json, as: %TrixtaLibrary.Core.AlertExpert{}) do
      {:ok, alert} ->
        alert |> sanitise_alert_values()

      _ ->
        Logger.error("Could not parse json alert data into alert struct. Json: #{alert_json}")
        nil
    end
  end

  @doc """
  Converts a map with "string" keys to an alert_struct. This is different than from an atom list as struct/2 doesn't handle non-atom values.
  """
  defp string_map_to_alert_struct(alert_map) do
    # Given an alert map with string keys, cast it to a map with atom keys
    atom_alert_map =
      alert_map
      |> Map.new(fn {k, v} -> {String.to_existing_atom(k), v} end)

    # then into an alert struct
    struct(%TrixtaLibrary.Core.AlertExpert{}, atom_alert_map) |> sanitise_alert_values()
  end

  @doc """
  This sanitises values from an alert by ensuring they match the @spec values above.
  """
  defp sanitise_alert_values(alert) do
    alert
    |> Map.update!(:contract_event, fn value ->
      contract_event_spec(value)
    end)
    |> Map.update!(:condition, fn value ->
      condition_spec(value)
    end)
    |> Map.update!(:dataset, fn value ->
      dataset_spec(value)
    end)
  end

  # ********************************************
  # External functions to be used in steps.
  # ********************************************
  def add_alert_to_user(current_agent_id, json_alert) do
    case string_map_to_alert_struct(json_alert) do
      alert ->
        AgentServer.update_internal_state(current_agent_id, "alert:" <> alert.id, alert)

      nil ->
        Logger.error(
          "Could not add alert to user: Error while parsing payload into alert struct. Payload #{
            inspect(json_alert)
          }"
        )

        {"error",
         %{
           "details" =>
             "Could not add alert to user: Error while parsing payload into alert struct. Payload #{
               inspect(json_alert)
             }"
         }}
    end
  end

  def remove_alert_from_user(agent_id, alert_id) do
    AgentServer.remove_current_state_kv(
      agent_id,
      :internal_state,
      "alert:" <> alert_id
    )

    {"success", %{}}
  end

  def get_user_alerts(agent_id) do
    case AgentServer.get_all_matching_state(
           agent_id,
           :internal_state,
           "alert:*"
         ) do
      {:ok, alerts} ->
        {"success", %{"alerts" => alerts}}

      err ->
        Logger.error(
          "Error while fetching all alerts for agent #{agent_id} . AgentServer returned: #{
            inspect(err)
          }"
        )

        {"error",
         %{
           "details" => "Error while fetching all alerts for agent #{agent_id}"
         }}
    end
  end

  def get_user_alert_by_id(agent_id, alert_id) do
    case AgentServer.get_all_matching_state(
           agent_id,
           :internal_state,
           "alert:" <> alert_id
         ) do
      {:ok, %TrixtaLibrary.Core.AlertExpert{} = alert} ->
        {"success", %{"alert" => alert}}

      {:ok, [%TrixtaLibrary.Core.AlertExpert{} = alert | _rest]} ->
        {"success", %{"alert" => alert}}

      # Alert not found
      {:ok, _} ->
        {"success", %{"alert" => nil}}

      err ->
        Logger.error(
          "Error while fetching all alerts for agent #{agent_id} . AgentServer returned: #{
            inspect(err)
          }"
        )

        {"error",
         %{
           "details" => "Error while fetching all alerts for agent #{agent_id}"
         }}
    end
  end

  def check_alert_cooloff_in_effect(%{"agent_id" => _, "alert" => alert}) do
    # cooldown defaults to 1 day
    cooloff_period = Map.get(alert, :cooldown_timeout_seconds, 60 * 60 * 24)
    last_send_time = Map.get(alert, :last_notification_sent_time)

    if last_send_time do
      date_compare_result =
        last_send_time
        |> DateTime.from_iso8601()
        |> elem(1)
        |> DateTime.add(cooloff_period)
        |> DateTime.compare(DateTime.utc_now())

      case date_compare_result do
        :lt -> {"send_notification", nil}
        _ -> {"dont_send_notification", nil}
      end
    else
      {"send_notification", nil}
    end
  end

  def check_alert_cooloff_in_effect(data) do
    error_string =
      "Could not check alert.cooldown_timeout_seconds because step received invalid data: #{
        inspect(data)
      })"

    Logger.error(error_string)
    raise error_string
  end

  def update_alert_last_notification_sent_time(%{"agent_id" => agent_id, "alert" => alert}) do
    case AgentServer.get_all_matching_state(
           agent_id,
           :internal_state,
           "alert:" <> alert["id"]
         ) do
      {:ok, %TrixtaLibrary.Core.AlertExpert{} = alert} ->
        alert =
          Map.put(
            alert,
            :last_notification_sent_time,
            DateTime.utc_now() |> DateTime.to_iso8601()
          )

        AgentServer.update_internal_state(agent_id, "alert:" <> alert.id, alert)

        {"success", %{}}

      {:ok, [%TrixtaLibrary.Core.AlertExpert{} = alert | _rest]} ->
        alert =
          Map.put(
            alert,
            "last_notification_sent_time",
            DateTime.utc_now() |> DateTime.to_iso8601()
          )

        AgentServer.update_internal_state(agent_id, "alert:" <> alert["id"], alert)
        
        {"success", %{}}

      err ->
        Logger.error(
          "Error while fetching all alerts for agent #{agent_id} . AgentServer returned: #{
            inspect(err)
          }"
        )

        {"error",
         %{
           "details" => "Error while fetching all alerts for agent #{agent_id}"
         }}
    end
  end

  def update_alert_last_notification_sent_time(data) do
    error_string =
      "Could not update alert.last_send_time because step received invalid input data: #{
        inspect(data)
      })"

    Logger.error(error_string)
    raise error_string
  end
end
