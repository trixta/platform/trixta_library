defmodule TrixtaLibrary.Core.UserExpert do
  @moduledoc """
  Helper funtions for agents
  """
  alias TrixtaSpace.{AgentServer}
  alias TrixtaSpace.{PrimarySpaceServer}
  require Logger

  @verification_data "verification_data:"

  def is_new_password_provided(_flow_source_data, input_data) do
    if Map.has_key?(input_data, "password") and Map.get(input_data, "password") != "" do
      {"is_provided", %{}}
    else
      {"is_empty", %{}}
    end
  end

  def update_user_password(flow_source_data, input_data) do
    AgentServer.set_password(flow_source_data.current_agent_id, input_data["password"])
    {"success", %{}}
  end

  def get_user_profile(flow_source_data, _input_data) do
    {:ok, profile} =
      AgentServer.get_state(flow_source_data.current_agent_id, :internal_state, "profile")

    case profile do
      nil ->
        {"success", %{"username" => flow_source_data.current_agent_id}}

      profile when is_map(profile) ->
        {"success", Map.put(profile, "username", flow_source_data.current_agent_id)}
    end
  end

  def create_agent(
        %{"email_address" => email_address, "password" => password},
        suppress_welcome_email,
        password_already_hashed,
        space \\ nil,
        trixta_web_ui_endpoint \\ nil
      ) do
    agent_details = %{
      "identity" => email_address,
      "name" => email_address,
      "password" => password
    }

    TrixtaSpaceWeb.Role.Helpers.create_agent(
      agent_details,
      suppress_welcome_email,
      password_already_hashed,
      space,
      trixta_web_ui_endpoint
    )

    {:ok, nil}
  end

  def grant_agent_access(%{"email_address" => email_address}, role_id) do
    TrixtaSpaceWeb.Role.Helpers.grant_agent_access(%{
      "role_id" => role_id,
      "agent_identity" => email_address
    })

    {:ok, nil}
  end

  def save_verification_data(input_data) do
    details = %{
      "profile" => %{
        "firstname" => input_data["firstname"],
        "lastname" => input_data["lastname"],
        "organization" => input_data["organization"],
        "email_address" => String.downcase(input_data["email_address"])
      },
      "trixta_web_ui_endpoint" => input_data["trixta_web_ui_endpoint"],
      "space" => input_data["space"],
      "email_address" => String.downcase(input_data["email_address"]),
      "verify_token" => UUID.uuid4(),
      # Self-signup users currently get the trixta_ide_user role by default.
      # Invited users can be added to a specific role.
      # default role is now a list because we could potentially invite on multiple roles
      "role_id" => input_data["role_id"] || ["trixta_ide_user"]
    }

    # If the new user has chosen a password, set it.
    # If not, generate a reset token on the agent so they can set the password later.
    final_details =
      case input_data do
        %{"password" => new_password} when is_binary(new_password) and new_password != "" ->
          # The anonymous user has specified a new password. Set it on the verification details:
          details |> Map.put("password", Argon2.hash_pwd_salt(new_password))

        _ ->
          # The user hasn't chosen a password. This is probably an invite initiated by another agent.
          # Generate a reset token so that they can set their password when the agent is created:
          details |> Map.put("reset_token", UUID.uuid4())
      end

    update_internal_state(@verification_data <> input_data["email_address"], final_details)
    {"success", %{}}
  end

  def save_password_in_verification_data(plaintext_password, existing_verification_data) do
    updated_verification_data =
      existing_verification_data |> Map.put("password", Argon2.hash_pwd_salt(plaintext_password))

    update_internal_state(
      @verification_data <> existing_verification_data["email_address"],
      updated_verification_data
    )

    {"success", %{}}
  end

  # obsolete
  def get_verification_data(%{"email_address" => email_address}) do
    {:ok, existing_entry} =
      PrimarySpaceServer.get_state(:internal_state, @verification_data <> email_address)

    unless existing_entry do
      raise "Verification data '#{@verification_data <> email_address}' not found in space state"
    end

    case existing_entry do
      %{"password" => hashed_password} when is_binary(hashed_password) ->
        {"password_already_saved", existing_entry}

      _ ->
        {"no_password_saved", existing_entry}
    end
  end

  def get_verification_data(%{"identity" => identity}) do
    {:ok, existing_entry} =
      PrimarySpaceServer.get_state(:internal_state, @verification_data <> identity)

    unless existing_entry do
      raise "Verification data '#{@verification_data <> identity}' not found in space state"
    end

    case existing_entry do
      %{"password" => hashed_password} when is_binary(hashed_password) ->
        {"password_already_saved", existing_entry}

      _ ->
        {"no_password_saved", existing_entry}
    end
  end

  def get_all_verification_data() do
    case PrimarySpaceServer.get_state(:internal_state, @verification_data <> "*") do
      {:ok, all_verification_data} ->
        {:ok, all_verification_data}

      _ ->
        {:error, nil}
    end
  end

  def delete_verification_data(%{"email_address" => email_address}) do
    PrimarySpaceServer.delete_kv_pair(@verification_data <> email_address)

    {:ok, nil}
  end

  # obsolete
  def check_verification(%{"verify_token" => input_verify_token}, %{"verify_token" => saved_verify_token}) do
    if input_verify_token == saved_verify_token do
      {"verified", nil}
    else
      raise "The following token(#{saved_verify_token}) is invalid. "
    end
  end

  def check_verification(%{"token" => input_verify_token}, %{"verify_token" => saved_verify_token}) do
    if input_verify_token == saved_verify_token do
      {"verified", nil}
    else
      raise "The following token(#{saved_verify_token}) is invalid. "
    end
  end

  defp update_internal_state(key, value) do
    PrimarySpaceServer.update_internal_state(%{key => value})
  end
end
