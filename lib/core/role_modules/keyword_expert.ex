defmodule TrixtaLibrary.Core.KeywordExpert do
  def create_keyword_args(map_to_convert) when is_map(map_to_convert) do
    {"ok",
     %{
       "result" =>
         Enum.map(map_to_convert, fn {key, value} -> {String.to_existing_atom(key), value} end)
     }}
  end
end
