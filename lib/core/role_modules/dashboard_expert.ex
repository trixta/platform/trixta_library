defmodule TrixtaLibrary.Core.DashboardExpert do
  def default_system_dashboard() do
    {:ok,
     %{
       "dashboard_id" => "dashboard:system-default",
       "data" => %{
         "updated_at" => "2020-03-31T15:26:47.417Z",
         "type" => "dashboard",
         "dashboard" => %{
           "queryType" => "loki",
           "optionsJSON" => """
           {
             "darkTheme":false,
             "useMargins":true,
             "hidePanelTitles":false
           }
           """,
           "timeTo" => "now",
           "layouts" =>
             """
              {
                "lg":[
                  {
                     "maxH":4,
                     "minW":3,
                     "h":2,
                     "i":"DhKBEZTzu",
                     "static":false,
                     "moved":false,
                     "minH":2,
                     "w":4,
                     "x":0,
                     "y":0
                  },
                  {
                     "maxH":4,
                     "minW":3,
                     "h":2,
                     "i":"NXDEnhYVtm",
                     "static":false,
                     "moved":false,
                     "minH":2,
                     "w":4,
                     "x":4,
                     "y":0
                  },
                  {
                     "maxH":4,
                     "minW":3,
                     "h":2,
                     "i":"Nw05QD2D8",
                     "static":false,
                     "moved":false,
                     "minH":2,
                     "w":12,
                     "x":0,
                     "y":2
                  },
                  {
                     "maxH":4,
                     "minW":3,
                     "h":2,
                     "i":"cKskpevPc",
                     "static":false,
                     "moved":false,
                     "minH":2,
                     "w":4,
                     "x":4,
                     "y":6
                  },
                  {
                     "maxH":4,
                     "minW":3,
                     "h":2,
                     "i":"1yPYJarCu",
                     "static":false,
                     "moved":false,
                     "minH":2,
                     "w":4,
                     "x":8,
                     "y":6
                  },
                  {
                     "maxH":4,
                     "minW":3,
                     "h":2,
                     "i":"TEJWjBWueW",
                     "static":false,
                     "moved":false,
                     "minH":2,
                     "w":4,
                     "x":0,
                     "y":6
                  },
                  {
                     "maxH":4,
                     "minW":3,
                     "h":2,
                     "i":"jeTi4rPmj",
                     "static":false,
                     "moved":false,
                     "minH":2,
                     "w":12,
                     "x":0,
                     "y":4
                  },
                  {
                     "maxH":4,
                     "minW":3,
                     "h":2,
                     "i":"3IKDncY4G",
                     "static":false,
                     "moved":false,
                     "minH":2,
                     "w":4,
                     "x":8,
                     "y":0
                  }]
              }
             """
             |> Poison.decode!(),
           "metric_meta" => %{},
           "timeFrom" => "now-30m",
           "panelsJSON" => """
           [\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"DhKBEZTzu\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"DhKBEZTzu\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"pieChartLabel\": \"Flows Started\",\n\t\t\t\"query\": \"[\\n    \\\"|=\\\\\\\"flow_name\\\\\\\"\\\",\\n    \\\"|=\\\\\\\"flow_started\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"query_loki\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"get_dashboard\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"load_dashboard\\\\\\\"\\\",\\n    \\\"|=\\\\\\\"trixta_sys\\\\\\\"\\\"\\n]\",\n\t\t\t\"graphType\": \"Pie Chart\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 2,\n\t\t\t\t\"unit\": \"w\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"h\",\n\t\t\t\"xAggregationFieldType\": \"Group by Field\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"title\": \"Flows Started (2 Weeks) \",\n\t\t\t\"description\": \"\"\n\t\t},\n\t\t\"visualization\": false\n\t},\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"NXDEnhYVtm\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"NXDEnhYVtm\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"query\": \"[\\n  \\\"|=\\\\\\\"channel_joined\\\\\\\"\\\",\\n  \\\"|~\\\\\\\"root-agent\\\\\\\"\\\"\\n]\",\n\t\t\t\"graphType\": \"Counter\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 3,\n\t\t\t\t\"unit\": \"M\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"D\",\n\t\t\t\"xAggregationFieldType\": \"Group by Field\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"counterLabel\": \"Root-Agent Logins\",\n\t\t\t\"title\": \"Root-Agent Logins (3 Months)\",\n\t\t\t\"description\": \"\"\n\t\t},\n\t\t\"visualization\": false\n\t},\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"Nw05QD2D8\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"Nw05QD2D8\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"query\": \"[\\n    \\\"|=\\\\\\\"flow_name\\\\\\\"\\\",\\n    \\\"|=\\\\\\\"flow_started\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"query_loki\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"get_dashboard\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"load_dashboard\\\\\\\"\\\",\\n    \\\"|=\\\\\\\"trixta_sys\\\\\\\"\\\"\\n]\",\n\t\t\t\"graphType\": \"Line Chart\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 2,\n\t\t\t\t\"unit\": \"w\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"h\",\n\t\t\t\"xAggregationFieldType\": \"Group by interval\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"title\": \"Flows Started (2 Weeks)\",\n\t\t\t\"description\": \"\"\n\t\t},\n\t\t\"visualization\": false\n\t},\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"cKskpevPc\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"cKskpevPc\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"pieChartLabel\": \"Info Log by Module\",\n\t\t\t\"query\": \"[\\\"|=\\\\\\\"level=info\\\\\\\"\\\"]\",\n\t\t\t\"graphType\": \"Pie Chart\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 7,\n\t\t\t\t\"unit\": \"d\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"D\",\n\t\t\t\"xAggregationFieldType\": \"Group by Field\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"title\": \"Info Logs by Module (7 Days)\",\n\t\t\t\"description\": \" (Copy)\",\n\t\t\t\"xAggregationTargetField\": \"category\"\n\t\t},\n\t\t\"visualization\": false\n\t},\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"1yPYJarCu\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"1yPYJarCu\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"pieChartLabel\": \"Warn Log by Module\",\n\t\t\t\"query\": \"[\\\"|=\\\\\\\"level=warn\\\\\\\"\\\"]\",\n\t\t\t\"graphType\": \"Pie Chart\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 7,\n\t\t\t\t\"unit\": \"d\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"D\",\n\t\t\t\"xAggregationFieldType\": \"Group by Field\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"title\": \"Warn Logs by Module (7 Days)\",\n\t\t\t\"description\": \" (Copy) (Copy)\",\n\t\t\t\"xAggregationTargetField\": \"category\"\n\t\t},\n\t\t\"visualization\": false\n\t},\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"TEJWjBWueW\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"TEJWjBWueW\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"pieChartLabel\": \"Errors by Module\",\n\t\t\t\"query\": \"[\\\"|=\\\\\\\"level=error\\\\\\\"\\\"]\",\n\t\t\t\"graphType\": \"Pie Chart\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 7,\n\t\t\t\t\"unit\": \"d\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"D\",\n\t\t\t\"xAggregationFieldType\": \"Group by Field\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"title\": \"Errors Logs by Module (7 Days)\",\n\t\t\t\"description\": \"\",\n\t\t\t\"xAggregationTargetField\": \"category\"\n\t\t},\n\t\t\"visualization\": false\n\t},\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"jeTi4rPmj\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"jeTi4rPmj\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"query\": \"[\\n    \\\"|=\\\\\\\"level=error\\\\\\\"\\\"\\n]\",\n\t\t\t\"graphType\": \"Line Chart\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 3,\n\t\t\t\t\"unit\": \"M\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"h\",\n\t\t\t\"xAggregationFieldType\": \"Group by interval\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"title\": \"Errors (90 Days)\",\n\t\t\t\"description\": \"\"\n\t\t},\n\t\t\"visualization\": false\n\t},\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"3IKDncY4G\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"3IKDncY4G\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"query\": \"[\\n    \\\"|=\\\\\\\"level=error\\\\\\\"\\\"\\n]\",\n\t\t\t\"graphType\": \"Counter\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 7,\n\t\t\t\t\"unit\": \"d\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"D\",\n\t\t\t\"xAggregationFieldType\": \"Group by Field\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"counterLabel\": \"Errors\",\n\t\t\t\"title\": \"Errors 7 Days\",\n\t\t\t\"description\": \"\"\n\t\t},\n\t\t\"visualization\": false\n\t}\n]
           """,
           "version" => 1,
           "title" => "System Dashboard",
           "tags" => [],
           "description" => "A useful Dashboard describing system usage and statistics."
         }
       }
     }}
  end

  # Note that this is the same as the above, but is all provided as json
  def default_usage_dashboard() do
    {:ok,
     %{
       "dashboard_id" => "dashboard:usage_dashboard",
       "data" => %{
         "updated_at" => "2020-03-31T15:26:47.417Z",
         "type" => "dashboard",
         "dashboard" => %{
           "queryType" => "loki",
           "optionsJSON" => """
           {
             "darkTheme":false,
             "useMargins":true,
             "hidePanelTitles":false
           }
           """,
           "timeTo" => "now",
           "layouts" =>
             """
               {
                 "lg":[
                   {
                       "maxH":4,
                       "minW":3,
                       "h":2,
                       "i":"DmsPs86bn",
                       "static":false,
                       "moved":false,
                       "minH":2,
                       "w":4,
                       "x":0,
                       "y":0
                   },
                   {
                       "maxH":4,
                       "minW":3,
                       "h":2,
                       "i":"VUkTF2wED",
                       "static":false,
                       "moved":false,
                       "minH":2,
                       "w":6,
                       "x":6,
                       "y":2
                   },
                   {
                       "maxH":4,
                       "minW":3,
                       "h":2,
                       "i":"874hS-pSu",
                       "static":false,
                       "moved":false,
                       "minH":2,
                       "w":4,
                       "x":8,
                       "y":0
                   },
                   {
                       "maxH":4,
                       "minW":3,
                       "h":2,
                       "i":"9TBoAqv2W",
                       "static":false,
                       "moved":false,
                       "minH":2,
                       "w":6,
                       "x":0,
                       "y":2
                   },
                   {
                       "maxH":4,
                       "minW":3,
                       "h":2,
                       "i":"O1MPYf8CS",
                       "static":false,
                       "moved":false,
                       "minH":2,
                       "w":4,
                       "x":4,
                       "y":0
                   }
                 ]
             }
             """
             |> Poison.decode!(),
           "metric_meta" => %{},
           "timeFrom" => "now-30m",
           "panelsJSON" => """
           [\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"DmsPs86bn\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"DmsPs86bn\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"query\": \"[\\n    \\\"|=\\\\\\\"flow_name\\\\\\\"\\\",\\n    \\\"|=\\\\\\\"flow_started\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"query_loki\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"get_dashboard\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"load_dashboard\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"trixta_sys\\\\\\\"\\\"\\n]\",\n\t\t\t\"graphType\": \"Line Chart\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 2,\n\t\t\t\t\"unit\": \"w\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"h\",\n\t\t\t\"xAggregationFieldType\": \"Group by interval\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"title\": \"Flows Run (2 Weeks)\",\n\t\t\t\"description\": \"\"\n\t\t},\n\t\t\"visualization\": false\n\t},\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"VUkTF2wED\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"VUkTF2wED\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"pieChartLabel\": \"Flows Started\",\n\t\t\t\"query\": \"[\\n    \\\"|=\\\\\\\"flow_name\\\\\\\"\\\",\\n    \\\"|=\\\\\\\"flow_started\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"query_loki\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"get_dashboard\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"load_dashboard\\\\\\\"\\\",\\n    \\\"!=\\\\\\\"trixta_sys\\\\\\\"\\\"\\n]\",\n\t\t\t\"graphType\": \"Pie Chart\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 2,\n\t\t\t\t\"unit\": \"w\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"h\",\n\t\t\t\"xAggregationFieldType\": \"Group by Field\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"title\": \"Flows Run (2 Weeks)\",\n\t\t\t\"description\": \" (Copy)\"\n\t\t},\n\t\t\"visualization\": false\n\t},\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"874hS-pSu\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"874hS-pSu\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"pieChartLabel\": \"Flows Started\",\n\t\t\t\"query\": \"[\\n    \\\"|=\\\\\\\"started_new_flow\\\\\\\"\\\"\\n]\",\n\t\t\t\"graphType\": \"Counter\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 2,\n\t\t\t\t\"unit\": \"w\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"h\",\n\t\t\t\"xAggregationFieldType\": \"Group by Field\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"counterLabel\": \"New Flows\",\n\t\t\t\"title\": \"New Flows Created (7 Days) \",\n\t\t\t\"description\": \"\"\n\t\t},\n\t\t\"visualization\": false\n\t},\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"9TBoAqv2W\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"9TBoAqv2W\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"pieChartLabel\": \"Flows Started\",\n\t\t\t\"query\": \"[\\\"|=\\\\\\\"channel_joined\\\\\\\"\\\",\\n  \\\"|=\\\\\\\"root-agent\\\\\\\"\\\"]\",\n\t\t\t\"graphType\": \"Line Chart\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 3,\n\t\t\t\t\"unit\": \"M\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"5t\",\n\t\t\t\"xAggregationFieldType\": \"Group by interval\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"title\": \"Root-Agent Logins (3 Months) \",\n\t\t\t\"description\": \"\"\n\t\t},\n\t\t\"visualization\": false\n\t},\n\t{\n\t\t\"panelData\": {\n\t\t\t\"i\": \"O1MPYf8CS\",\n\t\t\t\"x\": 0,\n\t\t\t\"y\": 1.7976931348623157e+308,\n\t\t\t\"w\": 4,\n\t\t\t\"h\": 2,\n\t\t\t\"minW\": 3,\n\t\t\t\"minH\": 2,\n\t\t\t\"maxH\": 4\n\t\t},\n\t\t\"id\": \"O1MPYf8CS\",\n\t\t\"meta\": {\n\t\t\t\"legendVerticalAlignment\": \"bottom\",\n\t\t\t\"pieChartLabel\": \"Agent Logins\",\n\t\t\t\"query\": \"[\\\"|=\\\\\\\"channel_joined\\\\\\\"\\\"]\",\n\t\t\t\"graphType\": \"Pie Chart\",\n\t\t\t\"timeRangeFilter\": {\n\t\t\t\t\"amount\": 3,\n\t\t\t\t\"unit\": \"M\"\n\t\t\t},\n\t\t\t\"xAggregationTypeInterval\": \"5t\",\n\t\t\t\"xAggregationFieldType\": \"Group by Field\",\n\t\t\t\"legendHorizontalAlignment\": \"center\",\n\t\t\t\"title\": \" Logins (3 Months) \",\n\t\t\t\"description\": \" (Copy)\"\n\t\t},\n\t\t\"visualization\": false\n\t}\n]
           """,
           "version" => 1,
           "title" => "Usage Dashboard",
           "tags" => [],
           "description" =>
             "This Dashboard shows general usage of the system by users and related roles. This does not include any back-end or Trixta system usage."
         }
       }
     }}
  end
end
