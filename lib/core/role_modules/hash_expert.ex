defmodule TrixtaLibrary.Core.HashExpert do
  def phash2(cache_key_value) do
    # using the maximum range size of 2^32
    {"ok", :erlang.phash2(cache_key_value, 4_294_967_296)}
  end

  def phash2(cache_key_value, range) do
    {"ok", :erlang.phash2(cache_key_value, range)}
  end

  def hashids_encode(salt, term) do
    {"ok", Hashids.encode(Hashids.new(salt: salt), term)}
  end

  def hashids_decode(salt, term) do
    {"ok", Hashids.decode!(Hashids.new(salt: salt), term)}
  end
end
