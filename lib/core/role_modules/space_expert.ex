defmodule TrixtaLibrary.Core.SpaceExpert do
  @moduledoc """
  Helper funtions for spaces
  """
  alias TrixtaSpaceWeb.SpaceConnections
  alias TrixtaSpace.PrimarySpaceServer
  require Logger

  @doc """
  Returns some metrics about this space.
  """
  def get_this_space_metrics(_settings) do
    # TODO: Customize the metrics using the settings passed in.
    metrics = %{
      "process_count" => Process.list() |> length(),
      "memory_used" => :erlang.memory(:total)
    }

    {"success", metrics}
  end

  @doc """
  Returns a particular value from this space's key / value pairs table.
  """
  def get_single_state_record(key) do
    case PrimarySpaceServer.get_all_matching_state(:internal_state, key) do
      {:ok, []} -> {"not_found", nil}
      {:ok, [first_record | _rest]} -> {"success", first_record}
      {:ok, only_record} -> {"success", only_record}
      _ -> {"not_found", nil}
    end
  end

  @doc """
  Removes a particular value from this space's key / value pairs table.
  """
  def delete_kv_pair(key) do
    case PrimarySpaceServer.delete_kv_pair(key) do
      :ok -> {:ok, "success"}
      _ -> {:error, "error"}
    end
  end

  def get_state_records(key) do
    case PrimarySpaceServer.get_all_matching_state(:internal_state, key) do
      {:ok, []} -> {"not_found", nil}
      {:ok, records} -> {"success", records}
      _ -> {"not_found", nil}
    end
  end

  def get_step_setup_record(step_module, key) do
    get_single_state_record("setup_#{step_module}:#{key}")
  end

  @doc """
  Saves or updates a KV pair on the space state.
  """
  def save_space_kv_pair(key, value) do
    PrimarySpaceServer.update_internal_state(%{key => value})
  end

  def save_step_setup_record(step_module, key, value) do
    save_space_kv_pair("setup_#{step_module}:#{key}", value)
  end

  @doc """
  Flow step function to pass along some agent credentials to the given remote space.
  The remote space should store these credentials when it wants to connect back to this space later.

  A connection to the remote space must already exist, with permission to join the root channel.

  The input map contains the following:
   - "local_agent_details" : Credentials for an agent on this space which the remote space will use to authenticate back to us. We pass along these credentials for storage on the remote space.
   - "local_space_name" : The name this space is known by from the perspective of the remote space.
   - "remote_space_name": The name of the remote space from this space's perspective.
  """
  def furnish_remote_space_with_credentials(%{
        "local_agent_details" => creds,
        "local_space_name" => local_space_name,
        "remote_space_name" => remote_space_name
      }) do
    updated_creds = email_address_to_identity(creds)

    Logger.info(
      "Furnishing remote space #{remote_space_name} with credentials for agent id #{
        updated_creds["identity"]
      }"
    )

    furnish_credentials_result =
      case SpaceConnections.join_channel(remote_space_name, "root") do
        :ok ->
          # Send the credentials to the remote space so that it can save them against our name:

          SpaceConnections.send_action(
            remote_space_name,
            "root",
            "store_remote_space_credentials",
            %{
              # Our local space name is called 'remote_space_name' from the perspective of the other space in the context of this action.
              "remote_space_name" => local_space_name,
              # Sometimes the identity is passed as 'email_address', but the rest of the system expects 'identity'.
              "remote_agent_credentials" => updated_creds
            }
          )

        err ->
          Logger.error(
            "Could not join root channel on remote space #{remote_space_name} to furnish agent credentials. Join error: #{
              inspect(err)
            }"
          )

          {:error, :join_error}
      end

    case furnish_credentials_result do
      {:ok, %{}} ->
        # Remote space synchrounous action has returned ok.
        {"success", %{}}

      err ->
        Logger.error(
          "Could not furnish credentials to space #{remote_space_name}. The 'store_remote_space_credentials' action returned an error: #{
            inspect(err)
          }"
        )

        {"error",
         %{"details" => "Could not furnish remote space #{remote_space_name} with credentials."}}
    end
  end

  def furnish_remote_space_with_credentials(_) do
    Logger.error(
      "Could not furnish remote space with credentials. Invalid inputs to TrixtaLibrary.Core.SpaceExpert.furnish_remote_space_with_credentials/2"
    )

    {"error",
     %{"details" => "Could not furnish remote space with credentials. Invalid input format."}}
  end

  @doc """
  Invites the given remote space to connect back to this space by sending it a 'invite_to_connect' action.
  An socket from us to the remote space should already be open.

  The remote space will try to connect to us using the 'local_agent_details' passed in here.

  This does not invite the remote to join any channels yet, just to open a socket connection.
  The join invitation is done in invite_remote_to_join.
  """
  def invite_remote_to_connect(%{
        "remote_space_name" => remote_space_name,
        "local_space_name" => local_space_name,
        "local_agent_details" => local_agent_details
      }) do
    connect_invitation_result =
      case SpaceConnections.join_channel(remote_space_name, "root") do
        :ok ->
          SpaceConnections.send_action(
            remote_space_name,
            "root",
            "invite_to_connect",
            %{
              # Our local space name is called 'remote_space_name' from the perspective of the other space in the context of this action.
              "remote_space_name" => local_space_name,
              # We need to tell the remote space where to find us:
              "remote_space_ws_url" => TrixtaSpaceWeb.Endpoint.public_ws_url(),
              "remote_agent_id" =>
                local_agent_details |> email_address_to_identity() |> Map.get("identity")
              # We're only assing along the identity and not the password, because the password would have already been sent earlier by sending a
              # 'store_remote_space_credentials' action.
            }
          )

        err ->
          Logger.error(
            "Could not join root channel on remote space #{remote_space_name} to send connect invitation. Join error: #{
              inspect(err)
            }"
          )

          {:error, :join_error}
      end

    case connect_invitation_result do
      {:ok, %{}} ->
        # Remote space synchrounous action has returned ok.
        {"success", %{}}

      err ->
        Logger.error(
          "Could not send connect invitation to space #{remote_space_name}. The 'invite_to_connect' action returned an error: #{
            inspect(err)
          }"
        )

        {"error",
         %{"details" => "Sending connect invitation to remote space #{remote_space_name} failed."}}
    end
  end

  @doc """
  Invites a remote space to join the specified role on this space as the given agent.
  The remote space should already have credentials stored for this agent.
  There should also already be a connection to the remote space.
  """
  def invite_remote_to_join(remote_space_name, local_space_name, local_agent_id, role_to_join) do
    invitation_result =
      case SpaceConnections.join_channel(remote_space_name, "root") do
        :ok ->
          # Ask the remote space to join our role:

          SpaceConnections.send_action(
            remote_space_name,
            "root",
            "invite_to_join",
            %{
              # Our local space name is called 'remote_space_name' from the perspective of the other space in the context of this action.
              "remote_space_name" => local_space_name,
              "remote_agent_id" => local_agent_id,
              "role_to_join" => role_to_join
            }
          )

        err ->
          Logger.error(
            "Could not join root channel on remote space #{remote_space_name} to send join invitation. Root join error: #{
              inspect(err)
            }"
          )

          {:error, :join_error}
      end

    case invitation_result do
      {:ok, %{}} ->
        # Remote space synchrounous action has returned ok.
        {"success", %{}}

      err ->
        Logger.error(
          "Could not send channel join invitation to space #{remote_space_name}. The 'invite_to_join' action returned an error: #{
            inspect(err)
          }"
        )

        {"error",
         %{"details" => "Sending join invitation to remote space #{remote_space_name} failed."}}
    end
  end

  def invite_remote_to_join(
        %{
          "remote_space_name" => remote_space_name,
          "local_space_name" => local_space_name,
          "local_agent_details" => local_agent_details
        },
        role_to_join
      ) do
    local_agent_id = local_agent_details |> email_address_to_identity() |> Map.get("identity")
    invite_remote_to_join(remote_space_name, local_space_name, local_agent_id, role_to_join)
  end

  def invite_remote_to_join(_, _, _) do
    Logger.error(
      "Invalid input passed in to SpaceExpert.invite_remote_to_join/3 . Please check the flow RFR step inputs."
    )

    {:error, :invalid_input}
  end

  @doc """
  Applies the given seed to the remote space with the given name.
  A socket connection must already exist to the remote space.
  One can be established by calling connect_to_remote_space/2
  """
  def seed_remote_space(
        %{
          "remote_space_name" => space_name,
          "trixta_seed" => trixta_seed
        } = details
      ) do
    Logger.info("Trying to seed remote space #{space_name}")

    seed_action_payload = %{
      "trixta_seed" => trixta_seed,
      "trixta_app_builder_credentials" => remote_credentials_from_input(details)
    }

    # TODO: Reference from trixta_library to trixta_space makes this harder to test.
    # Move this supervision tree into trixta_utils or some new repo when there's time.
    seeding_result =
      case SpaceConnections.join_channel(space_name, "space:trixta_app_builder") do
        :ok ->
          # Send the seed action and wait 75 seconds for it to complete.

          SpaceConnections.send_action(
            space_name,
            "space:trixta_app_builder",
            "apply_seed_from_gitlab",
            seed_action_payload,
            timeout: 75_000
          )

        err ->
          Logger.error(
            "Could not join trixta_app_builder channel on remote space #{space_name} to apply seed. Join error: #{
              inspect(err)
            }"
          )

          {:error, :join_error}
      end

    case seeding_result do
      {:ok, %{}} ->
        # Remote space synchrounous action has returned ok.
        Logger.debug("Seeding of remote space #{space_name} completed.")
        {"success", %{}}

      err ->
        Logger.error(
          "Seeding of remote space #{space_name} failed. Error returned during 'apply_seed_from_gitlab' action: #{
            inspect(err)
          }"
        )

        {"error", %{"details" => "Seeding of remote space #{space_name} failed."}}
    end
  end

  def seed_remote_space(_) do
    Logger.error(
      "Invalid input for SpaceExpert.seed_remote_space/2 function. Please check the step input data."
    )

    {"error", %{"details" => "Invalid input to seed_remote_space/2"}}
  end

  def check_space_is_ready(space_http_url) do
    health_response = fn ->
      HTTPotion.get("#{space_http_url}/health/status")
    end

    case health_response.() do
      %HTTPotion.ErrorResponse{message: message} ->
        {"not_ready", %{"message" => message}}

      %HTTPotion.Response{status_code: status_code, body: response_body} ->
        case status_code do
          200 ->
            case Poison.decode(response_body) do
              {:ok, %{"status" => "ready"}} ->
                {"ready", %{}}

              response ->
                {"not_ready", %{"response" => response}}
            end

          404 ->
            {"not_ready", %{"status_code" => status_code}}

          _any ->
            {"not_ready", %{"status_code" => status_code}}
        end

      _something_else ->
        {"not_ready", %{}}
    end
  end

  @doc """
    Opens a socket connection to a remote space with the given name, socket details and credentials.
    Doesn't join any channels or send any actions.
  """
  def connect_to_remote_space(
        %{
          "remote_space_name" => space_name,
          "remote_root_credentials" => %{"identity" => _id, "password" => _pwd},
          "remote_space_ws_url" => remote_space_ws_url
        } = details
      ) do
    # TODO: Reference from trixta_library to trixta_space makes this harder to test.
    # Move this supervision tree into trixta_utils or some new repo when there's time.
    case TrixtaSpaceWeb.SpaceConnections.connect_to_space(details) do
      {:ok, _socket_pid} ->
        # We now have a supervised socket connection to this space.
        {"success", %{}}

      err ->
        Logger.error(
          "Could not connect to remote space #{space_name} . (Remote socket connection failed). Details: #{
            inspect(err)
          }"
        )

        {"error", %{"details" => "Remote space socket connection failed."}}
    end
  end

  def connect_to_remote_space(_) do
    Logger.error("Cannot connect to remote space. Invalid input values.")
    {"error", %{"details" => "Invalid input"}}
  end

  @doc """
  Determines remote space agent credentials using the fallowing fallback sequence:
    1) trixta_app_builder_credentials explicitly defined in input.
    2) remote_root_credentials explicitly defined in input.
    3) remote_agent_credentials if it is neither root nor trixta_app_builder.
    4) use the stored credentials for this space's websocket connection.
  """
  def remote_credentials_from_input(input) do
    case input do
      %{
        "trixta_app_builder_credentials" =>
          %{
            "identity" => _trixta_app_builder_agent_id,
            "password" => _trixta_app_builder_pwd
          } = trixta_app_builder_creds
      } ->
        trixta_app_builder_creds

      %{
        "remote_root_credentials" =>
          %{
            "identity" => _root_agent_id,
            "password" => _root_pwd
          } = root_creds
      } ->
        root_creds

      %{
        "remote_agent_credentials" =>
          %{"identity" => _agent_id, "password" => _agent_pwd} = agent_creds
      } ->
        agent_creds

      # If we are given only a 'remote_agent_id' in our input,
      # try to load credentials for that agent:
      %{"remote_agent_id" => agent_id} ->
        SpaceConnections.load_remote_credentials_from_state(input["remote_space_name"], agent_id)

      _ ->
        # If nothing was overridden in our input, try and look up the root-agent credentials we may have stored:
        Logger.warn(
          "No agent credentials given while trying to seed remote space #{
            input["remote_space_name"]
          } . Falling back on stored root credentials in local state, if any"
        )

        SpaceConnections.load_remote_credentials_from_state(
          input["remote_space_name"],
          "root-agent"
        )
    end
  end

  # Sometimes an agent identity is passed in as 'email_address'.
  # Many parts of the system still expects an 'identity', so do the conversion here if needed.
  defp email_address_to_identity(creds) do
    case creds do
      %{"email_address" => identity} ->
        creds
        |> Map.put("identity", identity)
        |> Map.delete("email_address")

      %{"identity" => _id, "password" => _pwd} = creds_with_id ->
        # Already in correct format, don't change anything.
        creds_with_id

      other ->
        # We don't recognize this credential format. Leave it as-is but log an error.
        Logger.error(
          "Invalid agent credentials format passed into SpaceExpert. Keeping it as-is, but this might cause issues down the line."
        )

        other
    end
  end
end
