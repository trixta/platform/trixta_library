defmodule TrixtaLibrary.Core.SqlExpert do
  def convert_map_to_value_list_sorted_by_keys(input_map) do
    {
      "ok",
      input_map
      |> Enum.into([], fn {k, v} -> {k, v} end)
      |> Enum.sort_by(fn {k, _v} -> k end)
      |> Enum.map(fn {_k, v} -> v end)
    }
  end
end
