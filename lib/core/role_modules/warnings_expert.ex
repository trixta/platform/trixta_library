defmodule TrixtaLibrary.Core.WarningsExpert do
    def flatten_flow_warnings(enum) do
      {"ok", Enum.reduce(enum, [], fn {key, value}, acc -> acc ++ Enum.map(value, fn item -> Map.merge(item, %{"flow" => key}) end) end)}
    end

    def count_by_field(enum, field, value) do
        Enum.count(enum, fn %{^field => item_value} -> item_value == value end)
    end
end