defmodule TrixtaLibrary.Core.DiffExpert do
    require Logger

    def condense_myers_output(input) do
        Enum.group_by(input, 
            fn [key, value] -> value end, 
            fn [key, value] -> 
                key 
            end
        )
        |> Enum.map(fn {key, value} ->
            case value do
                ["del",  "ins"] ->
                    {key, "changed"}
                [other] ->
                    {key, other}
                unknown ->
                    Logger.warn("Unknown diff grouping, please investigate: #{inspect({key, unknown})}")
                    {key, unknown}
            end
        end)
        |> Enum.into(%{})
        |> Enum.group_by(fn {key, value} -> value end,
            fn {key, value} -> key end
        )
    end
end

