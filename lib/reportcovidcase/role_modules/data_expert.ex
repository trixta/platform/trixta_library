defmodule TrixtaLibrary.ReportCovidCase.DataExpert do
  def filter_cases(nil, _filter_map) do
    nil
  end

  def filter_cases(covid_cases, %{
        "filters" => %{
          "suburb" => filter_suburb,
          "city" => filter_city
        },
        "from" => filter_from,
        "to" => filter_to
      })
      when is_list(covid_cases) do
    Enum.filter(covid_cases, fn %{
                                  "city" => city,
                                  "patient" => %{
                                    "suburb" => suburb
                                  },
                                  "timestamp" => timestamp
                                } ->
      {:ok, case_timestamp, _} = DateTime.from_iso8601(timestamp)

      {:ok, filter_from, _} = (filter_from <> "T00:00:00.000000Z") |> DateTime.from_iso8601()
      {:ok, filter_to, _} = (filter_to <> "T23:59:59.999999Z") |> DateTime.from_iso8601()

      (filter_suburb == nil || filter_suburb == "" ||
         String.downcase(filter_suburb) == String.downcase(suburb)) &&
        (filter_city == nil || filter_city == "" ||
           String.downcase(filter_city) == String.downcase(city)) &&
        (DateTime.compare(filter_from, case_timestamp) == :lt ||
           DateTime.compare(filter_from, case_timestamp) == :eq) &&
        (DateTime.compare(filter_to, case_timestamp) == :gt ||
           DateTime.compare(filter_to, case_timestamp) == :eq)
    end)
  end

  def filter_cases(covid_cases, %{
        "filters" => %{},
        "from" => filter_from,
        "to" => filter_to
      })
      when is_list(covid_cases) do
    filter_cases(covid_cases, %{
      "filters" => %{
        "suburb" => "",
        "city" => ""
      },
      "from" => filter_from,
      "to" => filter_to
    })
  end

  def filter_cases(covid_cases, %{
        "from" => filter_from,
        "to" => filter_to
      })
      when is_list(covid_cases) do
    filter_cases(covid_cases, %{
      "filters" => %{
        "suburb" => "",
        "city" => ""
      },
      "from" => filter_from,
      "to" => filter_to
    })
  end
end
