defmodule TrixtaLibrary.Escrow.EscrowTradeListExpert do
  @moduledoc false
  require Logger
  alias TrixtaSpace.PrimarySpaceServer

  @escrow_key_prefix "escrow_item:"

  def verify_buyer_validation(source_data, input_data) do
    {"success", %{}}
  end

  def update_trade_list_item(source_data, input_data) do
    {"success", %{}}
  end

  def send_escrow_seller_notification(source_data, input_data) do
    {"success", %{}}
  end

  def get_trade_list() do
    {:ok, trade_items} =
      PrimarySpaceServer.get_all_matching_state(
        :internal_state,
        @escrow_key_prefix <> "*"
      )

    {"success", trade_items}
  end

  @doc """
  Records the fact that this item has been created on the smart contract.
  """
  def set_trade_item_status(artwork_id, status)
      when status in [:not_on_blockchain_yet, :created, :paid, :cancelled, :completed, :deleted] do
    key = @escrow_key_prefix <> artwork_id

    {:ok, existing_item} =
      PrimarySpaceServer.get_all_matching_state(
        :internal_state,
        key
      )

    updated_item = existing_item |> Map.put("status", status)
    PrimarySpaceServer.update_internal_state(%{key => updated_item})
    {"success", updated_item}
  end

  def set_trade_item_status(artwork_id, invalid_status) do
    Logger.error(
      "Cannot set status on trade item with artwork id #{artwork_id} . Invalid status given: #{
        inspect(invalid_status)
      }"
    )

    {"error", %{"details" => "Invalid status given: #{inspect(invalid_status)}"}}
  end

  @doc """
  Deletes a trade list item from the space state if it couldn't be saved to the blockchain.
  """
  def delete_item(artwork_id) do
    PrimarySpaceServer.delete_kv_pair(@escrow_key_prefix <> artwork_id)
    {"success", %{}}
  end

  def create_trade_item(artwork_id, item_value_wei, seller_wallet_address) do
    unique_item_id = UUID.uuid4()

    item_record = %{
      "trixta_id" => unique_item_id,
      "artwork_id" => artwork_id,
      "item_value_wei" => item_value_wei,
      "seller_wallet_address" => seller_wallet_address,
      "status" => :not_on_blockchain_yet
    }

    key = @escrow_key_prefix <> artwork_id
    PrimarySpaceServer.update_internal_state(%{key => item_record})

    {"success", item_record}
  end
end
