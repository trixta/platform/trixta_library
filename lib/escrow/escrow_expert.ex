defmodule TrixtaLibrary.Escrow.EscrowExpert do
  @moduledoc false
  require Logger
  alias TrixtaLibrary.Escrow.PhysicalGoodsEscrow
  alias TrixtaLibrary.BlockChain.HexHelpers

  def validate_trade_item(reference_no, opts) do
    case PhysicalGoodsEscrow.validate(reference_no, opts) do
      {:ok, result} -> %{"success" => %{}}
      {:error, error} -> %{"error" => error}
    end
  end

  def get_trade_item_status(artwork_id, opts) do
    case PhysicalGoodsEscrow.status_for_reference(artwork_id, opts) do
      {:ok, status} -> {"success", %{"status" => status}}
      {:error, error} -> {"error", %{"details" => error}}
    end
  end

  def create_item(hash, reference_no, value, seller_address, opts) do
    case PhysicalGoodsEscrow.create(
           hash,
           reference_no,
           value,
           HexHelpers.hex_address_to_bytes(seller_address),
           opts
         ) do
      {:ok, tx_hash} ->
        {"submitted", %{"tx_hash" => tx_hash}}

      err ->
        error_message = get_error_message(err)
        Logger.error("Error while creating trade item on blockchain: #{error_message}")
        {"error", %{"details" => error_message}}
    end
  end

  def check_transaction_status(opts) do
    {"success", %{}}
  end

  def cancel_trade_item(reference_no, opts) do
    case PhysicalGoodsEscrow.cancel(reference_no, opts) do
      {:ok, result} -> %{"success" => %{status: result}}
      {:error, error} -> %{"error" => error}
    end
  end

  @doc """
  Converts a numeric item status to an atom matching the enum values in the smart contract.
  """
  def decode_escrow_status(status_number) when is_integer(status_number) do
    status =
      case status_number do
        0 -> :not_found
        1 -> :created
        2 -> :paid
        3 -> :success
        4 -> :failed
        5 -> :canceled
        6 -> :completed
      end

    {"success", %{"status" => status}}
  end

  def decode_escrow_status(status_number) when is_binary(status_number) do
    status_number
    |> String.replace("0x", "")
    |> Integer.parse(16)
    |> elem(0)
    |> decode_escrow_status()
  end

  def decode_escrow_status({:ok, status_number}) do
    decode_escrow_status(status_number)
  end

  defp get_error_message({:error, err}) do
    get_error_message(err)
  end

  defp get_error_message(%{"data" => %{} = data_map}) do
    case data_map |> Map.values() do
      [first | _rest] -> get_error_message(first)
      _ -> "Unknown error"
    end
  end

  defp get_error_message(%{"reason" => reason}) when is_binary(reason) do
    reason
  end

  defp get_error_message(%{"error" => "revert"}) do
    "Transaction reverted for unknown reason"
  end

  defp get_error_message(_) do
    "Unknown error"
  end
end
