defmodule TrixtaLibrary.Escrow.PhysicalGoodsEscrow do
  @moduledoc false
  alias TrixtaLibrary.BlockChain.HexHelpers
  alias TrixtaLibrary.BlockChain.EthereumExpert

  # Raw transactions start

  @doc """
  Creates an escrow item and adds it to the contract. This call uses a signed raw transaction under the hood

  ## Parameters
    - hash: String identity profile hash/id provided by external service
    - reference_no: String Trixta generated reference for escrow item
    - value: Integer value to be held in escrow in WEI
    - seller_address: Binary representation of address in bytes. Utility
      TrixtaLibrary.BlockChain.HexHelpers.hex_address_to_bytes can be used
      for conversions

  ## Example

    TrixtaLibrary.Escrow.PhysicalGoodsEscrow.create("test2", "test2", 100, "0x607CBb1372e18E8d196d1c0f4ab0D25BDe436119" |>
      TrixtaLibrary.BlockChain.HexHelpers.hex_address_to_bytes)
    {:error,
    %{"code" => -32000, "message" => "insufficient funds for gas * price + value"}}
    iex(2)>
  """
  def create(hash, reference_no, value, seller_address, opts \\ %{})
      when is_binary(hash) and is_binary(reference_no) and is_integer(value) and
             is_binary(seller_address) do
    EthereumExpert.eth_send_raw_transaction(
      "create(string,string,uint,address)",
      [
        hash,
        reference_no,
        value,
        seller_address |> :binary.decode_unsigned()
      ],
      opts
    )
  end

  @doc """
  Marks and escrow item as valid. This call uses a signed raw transaction under the hood and will consume gas

  ## Parameters
     - reference_no: String Trixta generated reference for escrow item to target
     - valid: Boolean reference to mark the item with

  ## Example

    TrixtaLibrary.Escrow.PhysicalGoodsEscrow.validate("test2", true)
    {:error,
    %{"code" => -32000, "message" => "insufficient funds for gas * price + value"}}
  """
  def validate(reference_no, valid, opts \\ %{})
      when is_binary(reference_no) and is_boolean(valid) do
    EthereumExpert.eth_send_raw_transaction("validate(string,bool)", [reference_no, valid], opts)
  end

  @doc """
  Cancels an escrow item. This call uses a signed raw transaction under the hood and will consume gas

  ## Parameters
     - reference_no: String Trixta generated reference for escrow item to target

  ## Example

    TrixtaLibrary.Escrow.PhysicalGoodsEscrow.cancel("test2")
    {:error,
    %{"code" => -32000, "message" => "insufficient funds for gas * price + value"}}
  """
  def cancel(reference_no, opts \\ %{})
      when is_binary(reference_no) do
    EthereumExpert.eth_send_raw_transaction("cancel(string)", [reference_no], opts)
  end

  # Raw transactions end

  # Views/call start

  @doc """
  Retrieve status for escrow item

  ## Parameters
     - reference_no: String Trixta generated reference for escrow item to target

  ## Example

    iex> TrixtaLibrary.Escrow.PhysicalGoodsEscrow.status_for_reference("test")
    :paid
  """
  def status_for_reference(reference_no, opts \\ %{})
      when is_binary(reference_no) do
    {:ok, status} = EthereumExpert.eth_call("statusForReferenceNo(string)", [reference_no], opts)

    TrixtaLibrary.Escrow.EscrowExpert.decode_escrow_status(status) |> elem(1)
  end

  @doc """
  Retrieve buyer for escrow item

  ## Parameters
     - reference_no: String Trixta generated reference for escrow item to target

  ## Example

    iex> TrixtaLibrary.Escrow.PhysicalGoodsEscrow.buyer_for_reference_no("test")
    {:ok, "0x0000000000000000000000000000000000000000000000000000000000000000"}
  """
  def buyer_for_reference_no(reference_no, opts \\ %{})
      when is_binary(reference_no) do
    EthereumExpert.eth_call("buyerForReferenceNo(string)", [reference_no], opts)
  end

  @doc """
  Retrieve seller for escrow item

  ## Parameters
     - reference_no: String Trixta generated reference for escrow item to target

  ## Example

    iex> TrixtaLibrary.Escrow.PhysicalGoodsEscrow.seller_for_reference_no("test")
    {:ok, "0x000000000000000000000000607cbb1372e18e8d196d1c0f4ab0d25bde436119"}
  """
  def seller_for_reference_no(reference_no, opts \\ %{})
      when is_binary(reference_no) do
    EthereumExpert.eth_call("sellerForReferenceNo(string)", [reference_no], opts)
  end
end
