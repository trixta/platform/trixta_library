defmodule TrixtaLibrary.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      TrixtaLibrary.Scheduler
    ]

    opts = [strategy: :one_for_one, name: TrixtaLibrary.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
