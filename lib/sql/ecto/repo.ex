defmodule TrixtaLibrary.SQL.Ecto.Repo do
  @moduledoc """
  Manages a dynamic Ecto Repo without the need for hard coded configuration and processes.
  """
  use Ecto.Repo,
    otp_app: :trixta_library,
    adapter: Ecto.Adapters.Postgres

  alias TrixtaLibrary.SQL.Ecto.Repo

  def with_dynamic_repo(repo_config, callback) do
    default_dynamic_repo = get_dynamic_repo()
    start_opts = [name: nil] ++ repo_config
    {:ok, repo} = Repo.start_link(start_opts)

    try do
      Repo.put_dynamic_repo(repo)
      callback.()
    rescue
      err ->
        {:error, {"DB ERROR: Ensure all params are correct and DB is available.", IO.inspect(err)}}
    after
      Repo.put_dynamic_repo(default_dynamic_repo)
      Supervisor.stop(repo)
    end
  end
end
