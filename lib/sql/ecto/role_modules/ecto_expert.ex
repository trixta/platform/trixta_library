defmodule TrixtaLibrary.SQL.Ecto.EctoExpert do
  @moduledoc """
  For dynamically working with SQL DBs using Ecto.
  """

  alias TrixtaLibrary.SQL.Ecto.Repo

  # EXAMPLE: Construct, execute and get results using a raw SQL query.
  def apply_raw_sql_statement(repo_config, sql_statement, sql_params \\ [], sql_options \\ []) do
    raw_sql_statement(repo_config, sql_statement, sql_params, sql_options)
  end

  # EXAMPLE: Construct, execute and get results using inputs from query_config.
  def apply_sql_statement(repo_config, query_config) do
    query = "SELECT #{query_config.select} FROM #{query_config.from} WHERE #{query_config.where}"

    raw_sql_statement(repo_config, query)
  end

  # Parse the given DB to obtain the *public* tables and their respective columns, types and descriptions.
  def get_db_schema(repo_config) do
    Repo.with_dynamic_repo(repo_config, fn ->
      {:ok, result} =
        Repo.query(
          "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'"
        )

      Enum.reduce(result.rows, [], fn [table], acc ->
        {:ok, primary_keys} =
          Repo.query(
            "SELECT c.column_name FROM information_schema.table_constraints tc JOIN information_schema.constraint_column_usage AS ccu USING (constraint_schema, constraint_name) JOIN information_schema.columns AS c ON c.table_schema = tc.constraint_schema AND tc.table_name = c.table_name AND ccu.column_name = c.column_name where constraint_type = 'PRIMARY KEY' and tc.table_name = '#{table}'"
          )

        {:ok, description} =
          Repo.query(
            "SELECT column_name, data_type FROM information_schema.columns WHERE table_name ='#{table}'"
          )

        columns =
          Enum.map(description.rows, fn [column_name, column_type] ->
            found =
              Enum.find(List.flatten(primary_keys.rows), {:not_found}, fn x ->
                x == column_name
              end)

            case found do
              {:not_found} ->
                {column_name, get_type(List.first(String.split(String.downcase(column_type)))),
                 nil}

              _ ->
                {column_name, get_type(column_type), true}
            end
          end)

        [[%{"table" => table, "columns" => columns}] | acc]
      end)
    end)
  end

  # Given a DB data type we try to convert it to the equivalent Ecto type.
  defp get_type(row) do
    case row do
      type when type in ["int", "integer", "bigint", "mediumint", "smallint", "tinyint"] ->
        ":integer"

      type
      when type in [
             "varchar",
             "text",
             "char",
             "year",
             "mediumtext",
             "longtext",
             "tinytext",
             "character"
           ] ->
        ":string"

      type when type in ["decimal", "float", "double", "real"] ->
        ":float"

      type when type in ["boolean", "bit", "bit varying"] ->
        ":boolean"

      type when type in ["datetime", "timestamp", "date", "time"] ->
        "Ecto.DateTime"

      type when type in ["blob"] ->
        ":binary"

      type when type in ["uuid"] ->
        ":uuid"

      _type ->
        # type is not supported so fallback to :string
        ":string"
    end
  end

  # Applies a raw SQL statement and return the raw result.
  # NOTE: INSECURE - does no sanitization or injection checking.
  # FIXME: DO NOT RUN THIS IN PRODUCTION WITHOUT FURTHER CHECKS!
  defp raw_sql_statement(repo_config, raw_sql, params \\ [], options \\ []) do
    Repo.with_dynamic_repo(repo_config, fn ->
      {:ok, result} = Repo.query(raw_sql, params, options)

      case result do
        %Postgrex.Result{command: command, columns: nil, rows: nil} ->
          [%{command: command, result: :ok}]

        _ ->
          columns = Enum.map(result.columns, &String.to_atom(&1))

          Enum.map(result.rows, fn row ->
            Enum.zip(columns, row)
            |> Enum.map(fn {k, v} -> {Atom.to_string(k), v} end)
            |> Enum.into(%{})
          end)
      end
    end)
  end
end
