defmodule TrixtaLibrary.BlockChain.DashboardHelpers do
  alias TrixtaSpace.AgentServer
  alias TrixtaSpace.PrimarySpaceServer

  @shared_dashboard "shared_dashboard:"

  def load_dashboard_summaries(source_data) do
    {:ok, dashboards} =
      AgentServer.get_all_matching_state(
        source_data.current_agent_id,
        :internal_state,
        "dashboard:*"
      )

    {"success",
     %{
       "hits" => %{
         "hits" => dashboards
       }
     }}
  end

  def get_dashboard(agent_id, input_data) do
    get_dashboard(agent_id, input_data)
  end

  def get_dashboard(source_data, input_data) do
    get_dashboard(source_data.current_agent_id, input_data)
  end

  def get_dashboard(agent_id, input_data) do
    {:ok, dashboard} =
      AgentServer.get_state(
        agent_id,
        :internal_state,
        input_data["dashboard_id"]
      )

    {"success", dashboard}
  end

  def create_or_update_dashboard(current_agent_id, input_data) when is_binary(current_agent_id) do
    create_or_update_dashboard(
      %{
        :current_agent_id => current_agent_id
      },
      input_data
    )
  end

  def create_or_update_dashboard(source_data, input_data) when is_map(source_data) do
    AgentServer.update_internal_state(source_data.current_agent_id,
      input_data["dashboard_id"],
      %{
        "_id" => input_data["dashboard_id"],
        "_index" => ".trixta_analytics",
        "_source" => input_data["data"],
        "_type" => "doc",
        "_version" => 26,
        "found" => true
      }
    )

    {"success", %{}}
  end

  def delete_dashboard(source_data, input_data) do
    AgentServer.remove_current_state_kv(
      source_data.current_agent_id,
      :internal_state,
      input_data["dashboard_id"]
    )

    {"success", %{}}
  end

  def share_dashboard(source_data, input_data) do
    shared_dashboard = %{
      "shared" => true,
      "owner" => source_data.current_agent_id
    }

    update_internal_state(@shared_dashboard <> input_data["dashboard_id"], shared_dashboard)
  end

  def shared_dashboard_details(input_data) do
    {:ok, existing_entry} =
      PrimarySpaceServer.get_state(
        :internal_state,
        @shared_dashboard <> input_data["dashboard_id"]
      )

    if existing_entry do
      {"success", existing_entry}
    else
      raise "Shared dashboard #{input_data["dashboard_id"]} not found in space state"
    end
  end

  defp update_internal_state(key, value) do
    PrimarySpaceServer.update_internal_state(%{key => value})
  end
end
