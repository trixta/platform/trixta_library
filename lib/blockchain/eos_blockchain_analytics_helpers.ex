defmodule TrixtaLibrary.BlockChain.EOSCryptoAnalyticsHelpers do
  @moduledoc """
  Helper functions to access flow execution logs from the rebel engine.
  """
  require Logger

  #
  # Get accounts from a public_key
  #
  def get_accounts(public_key) do
    {:ok, result} = EOSRPC.History.get_key_accounts(public_key)
    result.body
  end

  #
  # Get actions for an account
  #
  def get_actions(account) do
    {:ok, result} = EOSRPC.History.get_actions(account)
    # hd(result.body["actions"])["action_trace"]["act"]["account"]
    result.body["actions"]
  end

  #
  # Get transaction details from transaction id
  #
  def get_transaction(transaction_id) do
    {:ok, result} = EOSRPC.History.get_transaction(transaction_id)
    result.body
  end
end
