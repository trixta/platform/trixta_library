defmodule TrixtaLibrary.BlockChain.SourceHelpers do
  alias TrixtaSpace.AgentServer

  def load_contract_source_list(flow_source_data) do
    {:ok, sources} =
      AgentServer.get_all_matching_state(
        flow_source_data.current_agent_id,
        :internal_state,
        "source:*"
      )

    {"success", %{"eth_contract_sources" => sources}}
  end

  def get_user_contract_details(source_data, input_data) do
    {:ok, contract_details} =
      AgentServer.get_state(
        source_data.current_agent_id,
        :internal_state,
        "source:" <> input_data["address"]
      )

    {"success", contract_details}
  end

  def fetch_contract_details(input_data) do
    # fetch details about the contract here.
    # input data should include a contract address
    {:ok, contract_details} =
      TrixtaLibrary.BlockChain.ETHCryptoAnalyticsHelpers.get_contract_details(
        input_data["address"],
        input_data["network"]
      )

    {"success", contract_details}
  end

  def remove_contract_source_from_user(flow_source_data, input_data) do
    AgentServer.remove_current_state_kv(
      flow_source_data.current_agent_id,
      :internal_state,
      "source:" <> input_data["address"]
    )

    {"success", %{}}
  end
end
