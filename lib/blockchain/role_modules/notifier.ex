defmodule TrixtaLibrary.BlockChain.Notifier do
  require Logger

  def send_alert_email(
        situation_data,
        %{"timestamp" => timestamp, "triggering_value" => triggering_value},
        %{
          # This should be the agent's e-mail address. If it isn't a valid e-mail, the Mail Helper will catch it.
          "agent_id" => agent_email,
          "alert" =>
            %{
              "value" => _,
              "address" => _,
              "condition" => _,
              "query" => _
            } = alert
        } = alert_criteria_details
      ) do
    email_content = %{
      alert_name: Map.get(alert, "name", "Alert name unknown"),
      event_description: event_description(alert_criteria_details, situation_data),
      timestamp: timestamp,
      triggering_value: triggering_value
    }

    case TrixtaLibrary.Mail.Helper.send_alert_triggered_email(agent_email, email_content) do
      %Bamboo.Email{} ->
        {"success", %{}}

      err ->
        Logger.error("Could not send alert email. Mail helper returned: #{inspect(err)}")

        {"error",
         %{"details" => "Could not send alert email. Mail helper returned: #{inspect(err)}"}}
    end
  end

  def send_alert_email(situation_data, _processing_result, alert_criteria) do
    Logger.error(
      "Cannot send alert e-mail. Invalid alert criteria or situation data: Criteria: #{
        inspect(alert_criteria)
      } , Situation data: #{inspect(situation_data)}"
    )

    {"error", %{"details" => "Invalid situation data or alert criteria"}}
  end

  # Builds a human-readable description of 'what happened'. The wording will be different for each type of condition.
  defp event_description(%{"alert" => %{"query" => query_json}} = alert_details, situation_data)
       when is_binary(query_json) do
    # Query is represented as json. Parse it and call the function again.
    case Poison.decode(query_json) do
      {:ok, parsed_query} ->
        event_description(put_in(alert_details, ["alert", "query"], parsed_query), situation_data)

      # We can't build condition-specific wording. Return a generic message.
      _ ->
        "One or more values triggered your alert condition"
    end
  end

  defp event_description(
         %{"alert" => %{"condition" => "greater_than", "value" => threshold, "query" => query}} =
           _alert_details,
         _situation_data
       ) do
    "The #{field_name(query)} went above the threshold of #{threshold}"
  end

  defp event_description(
         %{"alert" => %{"condition" => "less_than", "value" => threshold, "query" => query}} =
           _alert_details,
         _situation_data
       ) do
    "The #{field_name(query)} went below the threshold of #{threshold}"
  end

  defp event_description(%{"alert" => %{"condition" => alert_condition}}, %{} = _parsed_query) do
    Logger.warn(
      "Alert condition not supported yet: #{inspect(alert_condition)} . Using a generic message for the notification."
    )

    # We can't build condition-specific wording. Return a generic message.
    "One or more values triggered your alert condition"
  end

  defp field_name(
         %{"aggs" => %{"bucket" => %{"aggs" => %{"0" => %{"avg" => %{"field" => field}}}}}} =
           _parsed_query
       ) do
    field
  end

  defp field_name(_) do
    ""
  end
end
