# defmodule TrixtaLibrary.BlockChain.SourceWatcherExpert do
#   @moduledoc """
#   Helper funtions for everything source watcher related
#   """
#   alias TrixtaSpace.{PrimarySpaceServer}
#   require Logger

#   @eth_contract_source_watcher "eth_contract_source_watcher:"
#   @eth_contract_source_priority_watcher "eth_contract_source_priority_watcher:"
#   @eth_transaction_source_watcher "eth_transaction_source_watcher:"
#   @eth_transaction_source_priority_watcher "eth_transaction_source_priority_watcher:"
#   @eth_erc20_token_event_source_watcher "eth_erc20_token_event_source_watcher:"
#   @eth_erc20_token_event_source_priority_watcher "eth_erc20_token_event_source_priority_watcher:"

#   @type_eth_contract_event "eth_contract_event"
#   @type_eth_transaction "eth_tx"
#   @type_eth_erc20_token_event "eth_erc20_token_event"

#   def get_eth_contract_sources() do
#     {:ok, eth_contract_events} =
#       PrimarySpaceServer.get_all_matching_state(
#         :internal_state,
#         # * is for wildcard lookup by prefix.
#         @eth_contract_source_watcher <> "*"
#       )

#     {:ok, priority_eth_contract_events} =
#       PrimarySpaceServer.get_all_matching_state(
#         :internal_state,
#         # * is for wildcard lookup by prefix.
#         @eth_contract_source_priority_watcher <> "*"
#       )

#     {:ok, eth_transaction_sources} =
#       PrimarySpaceServer.get_all_matching_state(
#         :internal_state,
#         # * is for wildcard lookup by prefix.
#         @eth_transaction_source_watcher <> "*"
#       )

#     {:ok, priority_eth_transaction_sources} =
#       PrimarySpaceServer.get_all_matching_state(
#         :internal_state,
#         # * is for wildcard lookup by prefix.
#         @eth_transaction_source_priority_watcher <> "*"
#       )

#     {:ok, eth_erc20_token_event_sources} =
#       PrimarySpaceServer.get_all_matching_state(
#         :internal_state,
#         # * is for wildcard lookup by prefix.
#         @eth_erc20_token_event_source_watcher <> "*"
#       )

#     {:ok, priority_eth_erc20_token_event_sources} =
#       PrimarySpaceServer.get_all_matching_state(
#         :internal_state,
#         # * is for wildcard lookup by prefix.
#         @eth_erc20_token_event_source_priority_watcher <> "*"
#       )

#     combined_results =
#       eth_contract_events ++
#         priority_eth_contract_events ++
#         eth_transaction_sources ++
#         priority_eth_transaction_sources ++
#         eth_erc20_token_event_sources ++ priority_eth_erc20_token_event_sources

#     # This prevents us from unnecessarily including watchers that have already finished their search range
#     filtered_combined_results =
#       Enum.filter(combined_results, fn item ->
#         item["from"] != item["to"] and item["users"] != %{}
#       end)

#     {:ok, %{"eth_contract_sources" => filtered_combined_results}}
#   end

#   def switch_on_log_type(%{"type" => type}) do
#     {type, nil}
#   end

#   def set_next_from_block_number(%{
#         "address" => address,
#         "next_from_block_number" => next_from_block_number,
#         "storage_key" => storage_key
#       }) do
#     {:ok, existing_entry} = PrimarySpaceServer.get_state(:internal_state, storage_key <> address)
#     existing_entry = Map.put(existing_entry, "from", next_from_block_number)
#     PrimarySpaceServer.update_internal_state(%{(storage_key <> address) => existing_entry})
#     {:ok, %{}}
#   end

#   def add_contract_source_watcher(
#         source_data,
#         input_data,
#         from_block,
#         to_block
#       ) do
#     save_source_watcher(
#       source_data,
#       input_data,
#       from_block,
#       to_block,
#       @eth_contract_source_watcher,
#       @type_eth_contract_event
#     )
#   end

#   def add_contract_source_priority_watcher(
#         source_data,
#         input_data,
#         from_block,
#         to_block
#       ) do
#     save_source_watcher(
#       source_data,
#       input_data,
#       from_block + 1,
#       to_block,
#       @eth_contract_source_priority_watcher,
#       @type_eth_contract_event
#     )
#   end

#   def add_transaction_source_watcher(
#         source_data,
#         input_data,
#         from_block,
#         to_block
#       ) do
#     save_source_watcher(
#       source_data,
#       input_data,
#       from_block,
#       to_block,
#       @eth_transaction_source_watcher,
#       @type_eth_transaction
#     )
#   end

#   def add_priority_transaction_source_watcher(
#         source_data,
#         input_data,
#         from_block,
#         to_block
#       ) do
#     save_source_watcher(
#       source_data,
#       input_data,
#       from_block + 1,
#       to_block,
#       @eth_transaction_source_priority_watcher,
#       @type_eth_transaction
#     )
#   end

#   def add_erc20_token_event_source_watcher(
#         source_data,
#         input_data,
#         from_block,
#         to_block
#       ) do
#     save_source_watcher(
#       source_data,
#       input_data,
#       from_block,
#       to_block,
#       @eth_erc20_token_event_source_watcher,
#       @type_eth_erc20_token_event
#     )
#   end

#   def add_priority_erc20_token_event_source_watcher(
#         source_data,
#         input_data,
#         from_block,
#         to_block
#       ) do
#     save_source_watcher(
#       source_data,
#       input_data,
#       from_block + 1,
#       to_block,
#       @eth_erc20_token_event_source_priority_watcher,
#       @type_eth_erc20_token_event
#     )
#   end

#   defp save_source_watcher(source_data, input_data, from_block, to_block, prefix, type) do
#     {:ok, existing_entry} =
#       PrimarySpaceServer.get_state(:internal_state, prefix <> input_data["address"])

#     contract =
#       if existing_entry do
#         updated_entry =
#           Map.put(
#             existing_entry,
#             "users",
#             Map.put(existing_entry["users"], source_data.current_agent_id, true)
#           )

#         Map.put(updated_entry, "network", input_data["network"])
#       else
#         %{
#           "address" => input_data["address"],
#           "network" => input_data["network"],
#           "type" => type,
#           "storage_key" => prefix,
#           "from" => from_block,
#           "to" => to_block,
#           "users" => %{
#             source_data.current_agent_id => true
#           }
#         }
#       end

#     update_internal_state(prefix <> contract["address"], contract)
#   end

#   def remove_contract_source_watcher(source_data, input_data) do
#     delete_source_watcher(source_data, input_data, @eth_contract_source_watcher)
#   end

#   def remove_contract_source_priority_watcher(source_data, input_data) do
#     delete_source_watcher(source_data, input_data, @eth_contract_source_priority_watcher)
#   end

#   def remove_transaction_source_watcher(source_data, input_data) do
#     delete_source_watcher(source_data, input_data, @eth_transaction_source_watcher)
#   end

#   def remove_transaction_source_priority_watcher(source_data, input_data) do
#     delete_source_watcher(source_data, input_data, @eth_transaction_source_priority_watcher)
#   end

#   defp delete_source_watcher(source_data, input_data, prefix) do
#     {:ok, existing_entry} =
#       PrimarySpaceServer.get_state(:internal_state, prefix <> input_data["address"])

#     contract =
#       if existing_entry do
#         Map.put(
#           existing_entry,
#           "users",
#           Map.delete(existing_entry["users"], source_data.current_agent_id)
#         )
#       else
#         raise "Contract source #{input_data["address"]} not found in space state"
#       end

#     update_internal_state(prefix <> contract["address"], contract)
#   end

#   def ensure_transaction_source_watchers() do
#     Logger.info("Ensuring transaction source watchers are set")

#     {:ok, eth_contract_events} =
#       PrimarySpaceServer.get_all_matching_state(
#         :internal_state,
#         # * is for wildcard lookup by prefix.
#         @eth_contract_source_watcher <> "*"
#       )

#     Enum.each(eth_contract_events, fn source ->
#       input_data = %{
#         "address" => source["address"]
#       }

#       if Map.keys(source["users"]) != [] do
#         Enum.each(Map.keys(source["users"]), fn user_key ->
#           source_data = %{
#             current_agent_id: user_key
#           }

#           {"success", %{"result" => latest_block_offset}} =
#             TrixtaLibrary.BlockChain.ETHCryptoAnalyticsHelpers.get_latest_block_offset(-10000)

#           save_source_watcher(
#             source_data,
#             input_data,
#             0,
#             latest_block_offset,
#             @eth_transaction_source_watcher,
#             @type_eth_transaction
#           )

#           save_source_watcher(
#             source_data,
#             input_data,
#             latest_block_offset + 1,
#             "latest",
#             @eth_transaction_source_priority_watcher,
#             @type_eth_transaction
#           )
#         end)
#       else
#         # no users, delete these entries
#         TrixtaStorage.delete(
#           :"space_kv_pairs_#{TrixtaSpaces.SpacesSupervisor.first_space_id()}",
#           @eth_contract_source_watcher <> source["address"]
#         )

#         TrixtaStorage.delete(
#           :"space_kv_pairs_#{TrixtaSpaces.SpacesSupervisor.first_space_id()}",
#           @eth_contract_source_priority_watcher <> source["address"]
#         )
#       end
#     end)

#     {:ok, %{}}
#   end

#   def ensure_contract_event_watchers_have_type_storage() do
#     Logger.info("Ensuring source watcher type and storage key")

#     {:ok, eth_contract_events} =
#       PrimarySpaceServer.get_all_matching_state(
#         :internal_state,
#         # * is for wildcard lookup by prefix.
#         @eth_contract_source_watcher <> "*"
#       )

#     Enum.each(eth_contract_events, fn source ->
#       input_data = %{
#         "address" => source["address"]
#       }

#       update_watcher_with_type_storage(
#         input_data,
#         @type_eth_contract_event,
#         @eth_contract_source_watcher
#       )

#       update_watcher_with_type_storage(
#         input_data,
#         @type_eth_contract_event,
#         @eth_contract_source_priority_watcher
#       )
#     end)

#     {:ok, %{}}
#   end

#   defp update_watcher_with_type_storage(input_data, type, prefix) do
#     {:ok, existing_entry} =
#       PrimarySpaceServer.get_state(:internal_state, prefix <> input_data["address"])

#     contract =
#       if existing_entry do
#         existing_entry
#         |> Map.put("type", type)
#         |> Map.put("storage_key", prefix)
#       else
#         raise "Source '#{prefix <> input_data["address"]}' not found in space state"
#       end

#     update_internal_state(prefix <> contract["address"], contract)
#   end

#   def reset_source_watcher_from_to_blocks() do
#     Logger.info("Resetting source watcher blocks")

#     {:ok, eth_contract_events} =
#       PrimarySpaceServer.get_all_matching_state(
#         :internal_state,
#         # * is for wildcard lookup by prefix.
#         @eth_contract_source_watcher <> "*"
#       )

#     Enum.each(eth_contract_events, fn source ->
#       input_data = %{
#         "address" => source["address"]
#       }

#       {"success", %{"result" => latest_block_offset}} =
#         TrixtaLibrary.BlockChain.ETHCryptoAnalyticsHelpers.get_latest_block_offset(-10000)

#       reset_source_watcher_blocks(
#         input_data,
#         0,
#         latest_block_offset,
#         @eth_contract_source_watcher
#       )

#       reset_source_watcher_blocks(
#         input_data,
#         0,
#         latest_block_offset,
#         @eth_transaction_source_watcher
#       )

#       reset_source_watcher_blocks(
#         input_data,
#         latest_block_offset + 1,
#         "latest",
#         @eth_contract_source_priority_watcher
#       )

#       reset_source_watcher_blocks(
#         input_data,
#         latest_block_offset + 1,
#         "latest",
#         @eth_transaction_source_priority_watcher
#       )
#     end)

#     {:ok, %{}}
#   end

#   defp reset_source_watcher_blocks(input_data, from_block, to_block, prefix) do
#     {:ok, existing_entry} =
#       PrimarySpaceServer.get_state(:internal_state, prefix <> input_data["address"])

#     contract =
#       if existing_entry do
#         existing_entry
#         |> Map.put("from", from_block)
#         |> Map.put("to", to_block)
#       else
#         raise "Source '#{prefix <> input_data["address"]}' not found in space state"
#       end

#     update_internal_state(prefix <> contract["address"], contract)
#   end

#   defp update_internal_state(key, value) do
#     PrimarySpaceServer.update_internal_state(%{key => value})
#   end
# end
