# defmodule TrixtaLibrary.BlockChain.SituationDataExpert do
#   @moduledoc """
#   Helper funtions for situation data
#   """
#   alias TrixtaSpace.{PrimarySpaceServer}

#   alias TrixtaLibrary.BlockChain.AlertCriteriaHelpers
#   require Logger

#   @situation_watcher "situation_watcher:"

#   def get_situation_data_queries() do
#     results =
#       TrixtaSpace.AgentsSupervisor.list_agent_ids()
#       |> Enum.flat_map(fn agent_id ->
#         {:ok, alerts} =
#           TrixtaSpace.AgentServer.get_all_matching_state(agent_id, :internal_state, "alert:*")

#         alerts
#         |> Enum.map(fn alert ->
#           %{
#             "agent_id" => agent_id,
#             "alert" => alert
#           }
#         end)
#       end)

#     {:ok, %{"situation_data_queries" => results}}
#   end

#   def query_for_situation_data(%{"alert" => %{"query" => query_json}})
#       when is_binary(query_json) do
#     # The actual ElasticSearch query is provided as JSON:
#     case Poison.decode(query_json) do
#       {:ok, parsed_query} ->
#         query_for_situation_data(parsed_query)

#       _ ->
#         Logger.error("Could not parse situation data query from json: #{inspect(query_json)}")
#         {"error", %{"details" => "Could not parse situation data query"}}
#     end
#   end

#   def query_for_situation_data(%{"query" => %{}} = query) do
#     # The query has already been parsed from json. We can send it to ElasticSearch.
#     # TrixtaMetrics expects the query to be wrapped in "data":
#     case TrixtaMetrics.raw_search(%{"data" => query}) do
#       %{"took" => _took_value} = results ->
#         # Is there a better way to check whether a query has run successfully?
#         {"success", results}

#       _ ->
#         Logger.error("Invalid response from TrixtaMetrics while querying situation data")

#         {"error",
#          %{"details" => "Invalid response from TrixtaMetrics while querying situation data"}}
#     end
#   end

#   def query_for_situation_data(invalid_query) do
#     Logger.error(
#       "Could not query for situation data. Invalid query format: #{inspect(invalid_query)}"
#     )

#     {"error", %{"details" => "Could not query for situation data. Invalid query format."}}
#   end

#   def process_situation_data(situation_data, alert_criteria_details) do
#     case AlertCriteriaHelpers.is_condition_met(situation_data, alert_criteria_details) do
#       {"criteria_met", timestamp, triggering_value} ->
#         {
#           "criteria_met",
#           %{"timestamp" => timestamp, "triggering_value" => triggering_value}
#         }

#       _ ->
#         {"criteria_not_met", %{}}
#     end
#   end

#   defp update_internal_state(key, value) do
#     PrimarySpaceServer.update_internal_state(%{key => value})
#   end
# end
