defmodule TrixtaLibrary.BlockChain.AlertCriteriaHelpers do
  require Logger

  def is_condition_met(
        %{"aggregations" => %{"bucket" => %{"0" => %{"value" => _value}} = single_bucket}} =
          _data,
        %{
          "alert" => %{"condition" => _condition, "value" => _condition_value}
        } = whole_alert_map
      ) do
    is_condition_met(
      %{"aggregations" => %{"bucket" => %{"buckets" => [single_bucket]}}},
      whole_alert_map
    )
  end

  def is_condition_met(%{"aggregations" => %{"bucket" => %{"buckets" => buckets}}} = _data, %{
        "alert" => %{"condition" => condition, "value" => condition_value}
      })
      when is_list(buckets) do
    # Returns the first bucket that meets the alert's criteria. Ignores all the others.
    # TODO: Is there a better way to process histogram buckets than 'any value must match the criteria'? Should it depend on the alert properties...?
    buckets
    |> Enum.reduce_while(false, fn bucket, _acc ->
      case is_condition_met(bucket, condition, condition_value) do
        {"criteria_met", timestamp, value} -> {:halt, {"criteria_met", timestamp, value}}
        _ -> {:cont, "criteria_not_met"}
      end
    end)
  end

  def is_condition_met(value, condition) do
    Logger.error(
      "Alert criteria: Condition '#{condition}' not supported on value: #{inspect(value)}"
    )

    "error"
  end

  def is_condition_met(
        %{"0" => %{"value" => value}} = bucket,
        condition,
        condition_value
      ) do
    {is_condition_met(value, condition, condition_value), timestamp_from_bucket(bucket), value}
  end

  def is_condition_met(value, "greater_than", threshold) do
    value = ensure_number(value)
    threshold = ensure_number(threshold)

    if is_number(value) and is_number(threshold) do
      if value > threshold, do: "criteria_met", else: "criteria_not_met"
    else
      "error"
    end
  end

  def is_condition_met(value, "less_than", threshold) do
    value = ensure_number(value)
    threshold = ensure_number(threshold)

    if is_number(value) and is_number(threshold) do
      if value < threshold, do: "criteria_met", else: "criteria_not_met"
    else
      "error"
    end
  end

  def is_condition_met(value, condition, _threshold) do
    Logger.error(
      "Alert criteria: Condition '#{condition}' not supported on value: #{inspect(value)}"
    )

    "error"
  end

  def is_condition_met(value, condition) do
    Logger.error(
      "Alert criteria: Condition '#{condition}' not supported on value: #{inspect(value)}"
    )

    "error"
  end

  defp timestamp_from_bucket(%{"key_as_string" => key_as_string} = _bucket)
       when is_binary(key_as_string) do
    case DateTime.from_iso8601(key_as_string) do
      {:ok, _, _} ->
        key_as_string

      _ ->
        Logger.warn(
          "Could not parse ElasticSearch result bucket timestamp from key_as_string: #{
            key_as_string
          } while evaluating alert trigger. Returning the current time."
        )

        DateTime.utc_now() |> DateTime.to_iso8601()
    end
  end

  defp timestamp_from_bucket(_bucket) do
    # No key_as_string field on the bucket.
    # This might have been a count-only query (i.e. no histogram) so we just return the current timestamp.
    # TODO: Do we really want to default to the current time in such cases? Not sure about the requirements.
    DateTime.utc_now() |> DateTime.to_iso8601()
  end

  defp ensure_number(value) when is_number(value) do
    value
  end

  defp ensure_number(value) when is_binary(value) do
    case Float.parse(value) do
      {parsed_val, _} ->
        parsed_val

      _ ->
        Logger.error(
          "Error evaluating alert trigger: Could not parse value '#{value}' to float when comparing against greater_than criteria."
        )

        :error
    end
  end

  defp ensure_number(value) do
    Logger.error(
      "Error evaluating alert trigger: Could not parse value '#{value}' to float when comparing against greater_than criteria."
    )

    :error
  end
end
