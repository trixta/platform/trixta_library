# defmodule TrixtaLibrary.Services.ServiceManager.Service do
#   @enforce_keys [:type, :name]
#   defstruct [:type, :name, :reactions, :config]
# end

# defmodule TrixtaLibrary.Services.ServiceManager do
#   @moduledoc """
#   Facilitates the configuration of services and running service calls.
#   """

#   require Logger
#   alias TrixtaLibrary.Encrypt.Encrypt
#   alias TrixtaLibrary.Core.SpaceExpert
#   alias TrixtaLibrary.Services.HttpService
#   alias TrixtaLibrary.Services.AwsService
#   alias TrixtaLibrary.Services.ConfigService

#   @secure_service_fields_list ["key_value", "token", "password", "aws_access_key"]
#   def get_secure_service_fields_list, do: @secure_service_fields_list

#   # def register_service(name, type, source, payload) do
#   #   case {type, source} do
#   #     {"http", "swagger"} ->
#   #       swagger_version = Map.get(payload, "openapi") || Map.get(payload, "swagger") || ""

#   #       cond do
#   #         String.match?(swagger_version, ~r/^2./) ->
#   #           {response, message} =
#   #             TrixtaLibrary.Services.Ingesters.Swagger.register_service(:http, name, payload)

#   #         String.match?(swagger_version, ~r/^3./) ->
#   #           {response, message} =
#   #             TrixtaLibrary.Services.Ingesters.SwaggerV3.register_service(:http, name, payload)

#   #         true ->
#   #           Logger.error("Could assign parser for Swagger version: #{swagger_version}")
#   #       end

#   #     {"http", "swagger_lambda"} ->
#   #       {response, message} =
#   #         TrixtaLibrary.Services.Ingesters.SwaggerLambda.register_service(:http, name, payload)

#   #     {"config_service", _} ->
#   #       {response, message} =
#   #         TrixtaLibrary.Services.Ingesters.ConfigService.register_service(
#   #           :config_service,
#   #           name,
#   #           payload
#   #         )

#   #     {"aws_service", _} ->
#   #       {response, message} =
#   #         TrixtaLibrary.Services.Ingesters.AwsService.register_service(
#   #           :aws_service,
#   #           name,
#   #           payload
#   #         )

#   #     _ ->
#   #       Logger.error("Unsupported service type: #{type}")
#   #       {:error, %{"details" => "Unsupported service type: #{type}"}}
#   #   end
#   # end

#   ##
#   # Accessor Functions
#   ##

#   def get_service(service_name) do
#     with {"success", service} <-
#            SpaceExpert.get_single_state_record("service:#{service_name}") do
#       {:ok, service}
#     else
#       err ->
#         Logger.error("Could not find service, not found: #{service_name}")
#         {:error, %{"details" => "Service not found"}}
#     end
#   end

#   def get_service_config_schema(service_name) do
#     case SpaceExpert.get_single_state_record("service_config_schema:#{service_name}") do
#       {"success", schema} ->
#         {"success", %{"schema" => schema}}

#       {"not_found", _} ->
#         {"success", %{}}

#       _ ->
#         Logger.error("Failed to lookup service schema")
#         {:error, %{"details" => "Unknown error"}}
#     end
#   end

#   ##
#   # Space State functions
#   ##
#   def set_service(service_name, service_config, service_type, reactions) do
#     encrypted_config = target_service_config(service_name, service_config, true)

#     service = %{
#       "name" => service_name,
#       "type" => service_type,
#       "config" => encrypted_config,
#       "reactions" => reactions
#     }

#     SpaceExpert.save_space_kv_pair("service:#{service_name}", service)
#     FastGlobal.put("config_#{service_name}", nil)
#   end

#   def set_service_reaction(service_name, reaction_name, reaction_settings) do
#     SpaceExpert.save_space_kv_pair(
#       "service_reaction:#{service_name}:#{reaction_name}",
#       reaction_settings
#     )
#   end

#   def set_service_config_schema(service_name, schema) do
#     SpaceExpert.save_space_kv_pair(
#       "service_config_schema:#{service_name}",
#       schema
#     )
#   end

#   ##
#   # Running Services functions
#   ##

#   def lookup_service_call_settings(role_name, reaction_name) do
#     with {"success", service_settings} <-
#            SpaceExpert.get_single_state_record("service:#{role_name}"),
#          {"success", call_settings} <-
#            SpaceExpert.get_single_state_record("service_reaction:#{role_name}:#{reaction_name}") do
#       {:ok, service_settings, call_settings}
#     else
#       err ->
#         Logger.error(
#           "Service Call settings not found for role: #{role_name} and reaction #{reaction_name}"
#         )

#         {:error,
#          %{
#            "details" =>
#              "Service Call settings not found for role: #{role_name} and reaction #{reaction_name}"
#          }}
#     end
#   end

#   # def run_service_call(service_name, reaction_name, payload)
#   #     when is_binary(service_name) and is_binary(reaction_name) do
#   #   case lookup_service_call_settings(service_name, reaction_name) do
#   #     {:ok, service, service_reaction} ->
#   #       decrypted_config =
#   #         target_service_config(
#   #           service_name,
#   #           Map.get(service, "config", Map.get(service, :config)),
#   #           false
#   #         )

#   #       decrypted_service = Map.replace(service, "config", decrypted_config)
#   #       run_service_call(decrypted_service, service_reaction, payload)

#   #     _ ->
#   #       {:error,
#   #        %{
#   #          "details" =>
#   #            "Could not find service settings for the given role + reaction combination."
#   #        }}
#   #   end
#   # end

#   # def run_service_call(%{:type => :http} = service, service_reaction, payload) do
#   #   # Behaves normally with {status, message}
#   #   HttpService.run_service_call(service, service_reaction, payload)
#   # end

#   # def run_service_call(%{:type => :aws_service} = service, service_reaction, payload) do
#   #   AwsService.run_service_call(service, service_reaction, payload)
#   # end

#   # def run_service_call(%{"type" => :http} = service, service_reaction, payload) do
#   #   # Behaves normally with {status, message}
#   #   HttpService.run_service_call(service, service_reaction, payload)
#   # end

#   # def run_service_call(%{"type" => :aws_service} = service, service_reaction, payload) do
#   #   AwsService.run_service_call(service, service_reaction, payload)
#   # end

#   # def run_service_call(service_call_settings, call_settings, input_data) do
#   #   Logger.error("Service type not implemented yet.")
#   #   {:error, %{"reason" => :service_type_not_implemented}}
#   # end

#   # first item of auth list is the one that is used
#   def filter_service_auth([auth | _]) do
#     Enum.map(get_secure_service_fields_list, fn key ->
#       if Map.has_key?(auth, key) do
#         Map.replace!(auth, key, "***")
#       end
#     end)
#     |> Enum.reject(&is_nil/1)
#   end

#   def filter_service_auth([]) do
#     []
#   end

#   # AWS services have different auth structures
#   def filter_service_auth(credentials) when is_map(credentials) do
#     cond do
#       Map.has_key?(credentials, "aws_access_key") ->
#         Map.replace!(credentials, "aws_access_key", "***")

#       true ->
#         credentials
#     end
#   end

#   def filter_service_auth(nil) do
#     nil
#   end

#   def filter_service_auth(target_fields, auth) do
#     if Enum.any?(target_fields) do
#       Enum.map(target_fields, fn key ->
#         #  TODO: handle nested objects
#         if Map.has_key?(auth, key) do
#           Map.replace!(auth, key, "***")
#         end
#       end)
#       |> Enum.reject(&is_nil/1)
#     else
#       auth
#     end
#   end

#   def target_service_config(service_name, config, is_encrypting) do
#     cond do
#       Map.has_key?(config, "auth") ->
#         Map.merge(config, %{"auth" => target_service_auth(config["auth"], is_encrypting)})

#       Map.has_key?(config, "credentials") ->
#         Map.merge(config, %{
#           "credentials" => target_service_auth(config["credentials"], is_encrypting)
#         })

#       true ->
#         case get_service_config_schema(service_name) do
#           {"success", schema_record} ->
#             target_fields =
#               List.flatten(get_secure_fields_from_schema(nil, schema_record["schema"], []))

#             target_service_auth(target_fields, config, is_encrypting)

#           _ ->
#             # doesn't have a defined schema
#             config
#         end
#     end
#   end

#   # first item of auth list is the one that is used
#   def target_service_auth([auth | _], is_encrypting) do
#     {"ok", secret} = Encrypt.get_secret("services")

#     Enum.map(get_secure_service_fields_list, fn key ->
#       if Map.has_key?(auth, key) do
#         if is_encrypting do
#           val = Encrypt.encrypt(Map.get(auth, key), secret)
#           Map.replace!(auth, key, val)
#         else
#           val = Encrypt.decrypt(Map.get(auth, key), secret)
#           Map.replace!(auth, key, val)
#         end
#       end
#     end)
#     |> Enum.reject(&is_nil/1)
#   end

#   # AWS services have different auth structures
#   def target_service_auth(credentials, is_encrypting) when is_map(credentials) do
#     {"ok", secret} = Encrypt.get_secret("services")

#     cond do
#       Map.has_key?(credentials, "aws_access_key") ->
#         if is_encrypting do
#           val = Encrypt.encrypt(Map.get(credentials, "aws_access_key"), secret)
#           Map.replace!(credentials, "aws_access_key", val)
#         else
#           val = Encrypt.decrypt(Map.get(credentials, "aws_access_key"), secret)
#           Map.replace!(credentials, "aws_access_key", val)
#         end

#       true ->
#         credentials
#     end
#   end

#   def target_service_auth(nil) do
#     nil
#   end

#   def target_service_auth(target_fields, auth, is_encrypting) do
#     {"ok", secret} = Encrypt.get_secret("services")

#     if Enum.any?(target_fields) do
#       Enum.map(target_fields, fn key ->
#         #  TODO: handle nested objects
#         if Map.has_key?(auth, key) do
#           if is_encrypting do
#             val = Encrypt.encrypt(Map.get(auth, key), secret)
#             Map.replace!(auth, key, val)
#           else
#             val = Encrypt.decrypt(Map.get(auth, key), secret)
#             Map.replace!(auth, key, val)
#           end
#         end
#       end)
#       |> Enum.reject(&is_nil/1)
#     else
#       # TODO: target everything if there are no specific target options?
#       # Roger feedback is to just target everything
#       auth
#     end
#   end

#   def get_secure_fields_from_schema(key, schema, fields) do
#     type = schema["type"] || ""

#     case String.downcase(type) do
#       "string" ->
#         if schema["format"] == "password" do
#           fields ++ key
#         else
#           fields
#         end

#       "object" ->
#         fields ++
#           Enum.map(
#             Map.to_list(schema["properties"] || %{}),
#             fn {lookup_key, value} ->
#               get_secure_fields_from_schema(lookup_key, value, fields)
#             end
#           )

#       # Ignore other field types
#       _ ->
#         fields
#     end
#   end

#   def get_ui_examples() do
#     {:ok,
#      %{
#        service_templates: %{
#          http: HttpService.ui_example(),
#          config_service: ConfigService.ui_example(),
#          aws_service: AwsService.ui_example()
#        }
#      }}
#   end
# end
