# defmodule TrixtaLibrary.Services.HttpService do
#   require Logger

#   @allowed_http_methods [:get, :put, :post, :delete]

#   def run_service_call(service, reaction, nil) do
#     run_service_call(service, reaction)
#   end

#   def run_service_call(service, reaction, payload \\ %{}) do
#     try do
#       request_url =
#         url_from_call_settings(service, reaction, payload)
#         |> replace_url_placeholders(payload)

#       should_debug = should_debug_from_call_settings(service, reaction)
#       queries = generate_request_queries(service, reaction, payload)
#       headers = generate_request_headers(service, reaction, payload)

#       {authed_queries, authed_headers} = authenticate_request(service, queries, headers, payload)

#       authed_headers =
#         Map.merge(authed_headers, custom_headers_from_call_settings(service, reaction))

#       ref = UUID.uuid4()

#       Logger.info(
#         "Running http '#{reaction["method"]}' request with url: #{request_url}, ref:#{ref}"
#       )

#       response =
#         case reaction["method"] do
#           # TODO: Handle xml properly
#           # TODO: Ensure payload are being treated properly.
#           :get ->
#             if should_debug do
#               inspect(
#                 Logger.info(%{
#                   "ref" => ref,
#                   "url" => request_url,
#                   "headers" => authed_headers,
#                   "query" => authed_queries
#                 })
#               )
#             end

#             HTTPotion.get(request_url,
#               headers: authed_headers,
#               query: authed_queries,
#               timeout: 30_000
#             )

#           :post ->
#             if should_debug do
#               Logger.info(
#                 inspect(%{
#                   "ref" => ref,
#                   "url" => request_url,
#                   "headers" => authed_headers,
#                   "query" => authed_queries,
#                   "body" => Poison.encode!(payload)
#                 })
#               )
#             end

#             HTTPotion.post(request_url,
#               body: Poison.encode!(payload),
#               headers: authed_headers,
#               query: authed_queries,
#               timeout: 30_000
#             )

#           :delete ->
#             if should_debug do
#               Logger.info(
#                 inspect(%{
#                   "ref" => ref,
#                   "url" => request_url,
#                   "headers" => authed_headers,
#                   "query" => authed_queries,
#                   "body" => Poison.encode!(payload)
#                 })
#               )
#             end

#             HTTPotion.delete(request_url,
#               body: Poison.encode!(payload),
#               headers: authed_headers,
#               query: authed_queries,
#               timeout: 30_000
#             )

#           :put ->
#             if should_debug do
#               Logger.info(
#                 inspect(%{
#                   "ref" => ref,
#                   "url" => request_url,
#                   "headers" => authed_headers,
#                   "query" => authed_queries,
#                   "body" => Poison.encode!(payload)
#                 })
#               )
#             end

#             HTTPotion.put(request_url,
#               body: Poison.encode!(payload),
#               headers: authed_headers,
#               query: authed_queries,
#               timeout: 30_000
#             )

#           _ ->
#             Logger.error("HTTP method #{reaction["method"]} not implemented yet.")

#             # This return value format is what's expected by the flow engine.
#             {:error, %{"details" => :http_method_not_implemented}}
#         end

#       if should_debug do
#         Logger.info(
#           inspect(%{
#             "ref" => ref,
#             "response" => response
#           })
#         )
#       end

#       case parse_http_response(response) do
#         {:redirect, correct_url} ->
#           # The server returned a 301. Do the call again but with the correct location.
#           run_service_call(
#             %{"type" => :http, :absolute_url => correct_url},
#             reaction,
#             payload
#           )

#         {:ok, parsed_response} ->
#           {"success", %{"response" => parsed_response}}

#         invalid_parsed_response ->
#           Logger.error(
#             "Could not parse response from API service. Response: #{
#               inspect(invalid_parsed_response)
#             }"
#           )

#           {:error,
#            %{
#              "details" =>
#                "Could not parse response from API service. Response: #{
#                  inspect(invalid_parsed_response)
#                }"
#            }}
#       end
#     rescue
#       e ->
#         Logger.error("HTTP Service call failed with error: #{e.message}")
#         {:error, %{"details" => "HTTP Service call failed with error: #{e.message}"}}
#     end
#   end

#   defp authenticate_request(service, queries, nil, _) do
#     authenticate_request(service, queries)
#   end

#   defp authenticate_request(service, nil, headers, _) do
#     authenticate_request(service, %{}, headers)
#   end

#   defp authenticate_request(service, nil, nil, _) do
#     authenticate_request(service)
#   end

#   defp authenticate_request(%{"config" => %{"auth" => value}} = service, queries, headers, _)
#        when value == %{} do
#     {queries, headers}
#   end

#   defp authenticate_request(service, queries \\ %{}, headers \\ %{}, payload \\ %{}) do
#     auth_override = payload["auth"] || %{}

#     auth =
#       Map.get(service, "config")
#       |> Map.get("auth")
#       |> List.first()
#       |> Map.merge(auth_override)

#     case auth["type"] do
#       "api_key" ->
#         case auth["in"] do
#           "header" ->
#             {queries, Map.merge(headers, %{auth["key_name"] => auth["key_value"]})}

#           "query" ->
#             {Map.merge(queries, %{auth["key_name"] => auth["key_value"]}), headers}

#           _ ->
#             {queries, headers}
#         end

#       "oauth2" ->
#         {queries, Map.merge(headers, %{"Authorization" => "Bearer #{auth["token"]}"})}

#       "basic" ->
#         auth_token = "#{auth["username"]}:#{auth["password"]}"
#         encoded_auth_token = Base.encode64(auth_token)
#         {queries, Map.merge(headers, %{"Authorization" => "Basic #{encoded_auth_token}"})}

#       _ ->
#         Logger.info("Unsupported auth type")
#         {queries, headers}
#     end
#   end

#   defp generate_request_queries(service, reaction, nil) do
#     generate_request_headers(service, reaction)
#   end

#   defp generate_request_queries(service, reaction, payload \\ %{}) do
#     # Get all values for parameters
#     parameters = Map.get(reaction, "parameters")

#     keys_to_take =
#       parameters
#       |> Enum.filter(fn item -> item["in"] === "query" end)
#       |> Enum.map(fn item -> item["name"] end)

#     queries =
#       Map.take(payload, keys_to_take)
#       |> Enum.reject(fn {_key, val} -> is_nil(val) end)
#       |> Map.new()
#   end

#   defp generate_request_headers(service, reaction, nil) do
#     generate_request_headers(service, reaction)
#   end

#   defp generate_request_headers(service, reaction, payload \\ %{}) do
#     parameters = Map.get(reaction, "parameters")

#     keys_to_take =
#       parameters
#       |> Enum.filter(fn item -> item["in"] === "header" end)
#       |> Enum.map(fn item -> item["name"] end)

#     headers =
#       Map.take(payload, keys_to_take)
#       |> Enum.reject(fn {_key, val} -> is_nil(val) end)
#       |> Map.new()
#       |> Map.merge(%{"Content-Type" => "application/json"})
#   end

#   defp url_from_call_settings(%{"type" => :http, :absolute_url => absolute_url}, _, _) do
#     absolute_url
#   end

#   defp url_from_call_settings(
#          %{
#            "config" => %{
#              "host" => host,
#              "base_path" => base_path,
#              "http_schemes" => http_schemes
#            }
#          } = _service_settings,
#          %{"path" => path, "method" => http_method} = _call_settings,
#          input_data
#        ) do
#     # Determine what http scheme to use, default to https
#     http_scheme =
#       if is_list(http_schemes) and "http" in http_schemes, do: "http://", else: "https://"

#     case base_path do
#       "/" ->
#         Path.join([http_scheme, host, path])

#       _ ->
#         Path.join([http_scheme, host, base_path, path])
#     end
#     |> replace_url_placeholders(input_data)
#   end

#   defp should_debug_from_call_settings(
#          %{
#            "config" => %{
#              "debug" => debug
#            }
#          } = _service_settings,
#          %{"name" => name} = _call_settings
#        ) do
#     case debug do
#       true ->
#         true

#       endpoints when is_list(endpoints) ->
#         name in endpoints

#       _ ->
#         false
#     end
#   end

#   defp should_debug_from_call_settings(_service_settings, _call_settings) do
#     false
#   end

#   defp custom_headers_from_call_settings(
#          %{
#            "config" => %{
#              "custom_request_headers" => custom_request_headers
#            }
#          } = _service_settings,
#          _call_settings
#        ) do
#     custom_request_headers
#   end

#   defp custom_headers_from_call_settings(_service_settings, _call_settings) do
#     %{}
#   end

#   # HTTP Response Handling

#   defp parse_http_response(%HTTPotion.Response{
#          :status_code => 301,
#          headers: %{:hdrs => %{"location" => correct_location}}
#        }) do
#     # The API returned a 301, so we re-do the request using the correct location as indicated by the server:
#     {:redirect, correct_location}
#   end

#   defp parse_http_response(%HTTPotion.Response{} = response) do
#     if response.headers["content-type"] =~ "application/json" do
#       parse_http_response(response.body, :json)
#     else
#       Logger.error("Successful HTTP request but response is not in json format.")
#       {:error, %{"details" => :unhandled_http_response}}
#     end
#   end

#   defp parse_http_response(%HTTPotion.ErrorResponse{} = response) do
#     {:error, %{"details" => "Could not parse http response: #{response.message}"}}
#   end

#   defp parse_http_response(response_body, :json)
#        when is_binary(response_body) and response_body == "" do
#     {:ok, %{"message" => "No reponse body returned."}}
#   end

#   defp parse_http_response(response_body, :json) when is_binary(response_body) do
#     # TODO: Maybe move this into an http-specific module.
#     case Poison.decode(response_body) do
#       {:ok, parsed_body} ->
#         {:ok, parsed_body}

#       _ ->
#         Logger.error("Could not parse json body from HTTP service response.")
#         {:error, %{"details" => "Could not parse json body from HTTP service response"}}
#     end
#   end

#   defp parse_http_response(_response) do
#     Logger.error("Unhandled HTTPotion response type.")
#     {:error, %{"details" => "Unhandled HTTPotion response type."}}
#   end

#   ##
#   # Utility Methods
#   ##

#   defp replace_url_placeholders(url, input_data) do
#     Regex.replace(
#       ~r/{(.+?)}/,
#       url,
#       fn _whole, placeholder ->
#         case value = Map.get(input_data, placeholder) do
#           nil ->
#             Logger.error(
#               "SUBSTITUTION ERROR: Missing parameter substitution for URL request: #{url}. Value for '#{
#                 placeholder
#               }' not found in input_parameters. Defaulting to empty string which may cause request errors."
#             )

#             ""

#           int_value when is_integer(int_value) ->
#             to_string(int_value)

#           _ ->
#             value
#         end
#       end,
#       global: true
#     )
#   end

#   def ui_example() do
#     %{
#       petstore: %{
#         "basePath" => "/v2",
#         "definitions" => %{
#           "ApiResponse" => %{
#             "properties" => %{
#               "code" => %{"format" => "int32", "type" => "integer"},
#               "message" => %{"type" => "string"},
#               "type" => %{"type" => "string"}
#             },
#             "type" => "object"
#           },
#           "Category" => %{
#             "properties" => %{
#               "id" => %{"format" => "int64", "type" => "integer"},
#               "name" => %{"type" => "string"}
#             },
#             "type" => "object",
#             "xml" => %{"name" => "Category"}
#           },
#           "Order" => %{
#             "properties" => %{
#               "complete" => %{"type" => "boolean"},
#               "id" => %{"format" => "int64", "type" => "integer"},
#               "petId" => %{"format" => "int64", "type" => "integer"},
#               "quantity" => %{"format" => "int32", "type" => "integer"},
#               "shipDate" => %{"format" => "date-time", "type" => "string"},
#               "status" => %{
#                 "description" => "Order Status",
#                 "enum" => ["placed", "approved", "delivered"],
#                 "type" => "string"
#               }
#             },
#             "type" => "object",
#             "xml" => %{"name" => "Order"}
#           },
#           "Pet" => %{
#             "properties" => %{
#               "category" => %{"$ref" => "#/definitions/Category"},
#               "id" => %{"format" => "int64", "type" => "integer"},
#               "name" => %{"example" => "doggie", "type" => "string"},
#               "photoUrls" => %{
#                 "items" => %{"type" => "string", "xml" => %{"name" => "photoUrl"}},
#                 "type" => "array",
#                 "xml" => %{"wrapped" => true}
#               },
#               "status" => %{
#                 "description" => "pet status in the store",
#                 "enum" => ["available", "pending", "sold"],
#                 "type" => "string"
#               },
#               "tags" => %{
#                 "items" => %{
#                   "$ref" => "#/definitions/Tag",
#                   "xml" => %{"name" => "tag"}
#                 },
#                 "type" => "array",
#                 "xml" => %{"wrapped" => true}
#               }
#             },
#             "required" => ["name", "photoUrls"],
#             "type" => "object",
#             "xml" => %{"name" => "Pet"}
#           },
#           "Tag" => %{
#             "properties" => %{
#               "id" => %{"format" => "int64", "type" => "integer"},
#               "name" => %{"type" => "string"}
#             },
#             "type" => "object",
#             "xml" => %{"name" => "Tag"}
#           },
#           "User" => %{
#             "properties" => %{
#               "email" => %{"type" => "string"},
#               "firstName" => %{"type" => "string"},
#               "id" => %{"format" => "int64", "type" => "integer"},
#               "lastName" => %{"type" => "string"},
#               "password" => "[FILTERED]",
#               "phone" => %{"type" => "string"},
#               "userStatus" => %{
#                 "description" => "User Status",
#                 "format" => "int32",
#                 "type" => "integer"
#               },
#               "username" => %{"type" => "string"}
#             },
#             "type" => "object",
#             "xml" => %{"name" => "User"}
#           }
#         },
#         "externalDocs" => %{
#           "description" => "Find out more about Swagger",
#           "url" => "http://swagger.io"
#         },
#         "host" => "petstore.swagger.io",
#         "info" => %{
#           "contact" => %{"email" => "apiteam@swagger.io"},
#           "description" =>
#             "This is a sample server Petstore server.  You can find out more about Swagger at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).  For this sample, you can use the api key `special-key` to test the authorization filters.",
#           "license" => %{
#             "name" => "Apache 2.0",
#             "url" => "http://www.apache.org/licenses/LICENSE-2.0.html"
#           },
#           "termsOfService" => "http://swagger.io/terms/",
#           "title" => "Swagger Petstore",
#           "version" => "1.0.5"
#         },
#         "paths" => %{
#           "/pet" => %{
#             "post" => %{
#               "consumes" => ["application/json", "application/xml"],
#               "description" => "",
#               "operationId" => "addPet",
#               "parameters" => [
#                 %{
#                   "description" => "Pet object that needs to be added to the store",
#                   "in" => "body",
#                   "name" => "body",
#                   "required" => true,
#                   "schema" => %{"$ref" => "#/definitions/Pet"}
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{"405" => %{"description" => "Invalid input"}},
#               "security" => [%{"petstore_auth" => ["write:pets", "read:pets"]}],
#               "summary" => "Add a new pet to the store",
#               "tags" => ["pet"]
#             },
#             "put" => %{
#               "consumes" => ["application/json", "application/xml"],
#               "description" => "",
#               "operationId" => "updatePet",
#               "parameters" => [
#                 %{
#                   "description" => "Pet object that needs to be added to the store",
#                   "in" => "body",
#                   "name" => "body",
#                   "required" => true,
#                   "schema" => %{"$ref" => "#/definitions/Pet"}
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "400" => %{"description" => "Invalid ID supplied"},
#                 "404" => %{"description" => "Pet not found"},
#                 "405" => %{"description" => "Validation exception"}
#               },
#               "security" => [%{"petstore_auth" => ["write:pets", "read:pets"]}],
#               "summary" => "Update an existing pet",
#               "tags" => ["pet"]
#             }
#           },
#           "/pet/findByStatus" => %{
#             "get" => %{
#               "description" =>
#                 "Multiple status values can be provided with comma separated strings",
#               "operationId" => "findPetsByStatus",
#               "parameters" => [
#                 %{
#                   "collectionFormat" => "multi",
#                   "description" => "Status values that need to be considered for filter",
#                   "in" => "query",
#                   "items" => %{
#                     "default" => "available",
#                     "enum" => ["available", "pending", "sold"],
#                     "type" => "string"
#                   },
#                   "name" => "status",
#                   "required" => true,
#                   "type" => "array"
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "200" => %{
#                   "description" => "successful operation",
#                   "schema" => %{
#                     "items" => %{"$ref" => "#/definitions/Pet"},
#                     "type" => "array"
#                   }
#                 },
#                 "400" => %{"description" => "Invalid status value"}
#               },
#               "security" => [%{"petstore_auth" => ["write:pets", "read:pets"]}],
#               "summary" => "Finds Pets by status",
#               "tags" => ["pet"]
#             }
#           },
#           "/pet/findByTags" => %{
#             "get" => %{
#               "deprecated" => true,
#               "description" =>
#                 "Multiple tags can be provided with comma separated strings. Use tag1, tag2, tag3 for testing.",
#               "operationId" => "findPetsByTags",
#               "parameters" => [
#                 %{
#                   "collectionFormat" => "multi",
#                   "description" => "Tags to filter by",
#                   "in" => "query",
#                   "items" => %{"type" => "string"},
#                   "name" => "tags",
#                   "required" => true,
#                   "type" => "array"
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "200" => %{
#                   "description" => "successful operation",
#                   "schema" => %{
#                     "items" => %{"$ref" => "#/definitions/Pet"},
#                     "type" => "array"
#                   }
#                 },
#                 "400" => %{"description" => "Invalid tag value"}
#               },
#               "security" => [%{"petstore_auth" => ["write:pets", "read:pets"]}],
#               "summary" => "Finds Pets by tags",
#               "tags" => ["pet"]
#             }
#           },
#           "/pet/{petId}" => %{
#             "delete" => %{
#               "description" => "",
#               "operationId" => "deletePet",
#               "parameters" => [
#                 %{
#                   "in" => "header",
#                   "name" => "api_key",
#                   "required" => false,
#                   "type" => "string"
#                 },
#                 %{
#                   "description" => "Pet id to delete",
#                   "format" => "int64",
#                   "in" => "path",
#                   "name" => "petId",
#                   "required" => true,
#                   "type" => "integer"
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "400" => %{"description" => "Invalid ID supplied"},
#                 "404" => %{"description" => "Pet not found"}
#               },
#               "security" => [%{"petstore_auth" => ["write:pets", "read:pets"]}],
#               "summary" => "Deletes a pet",
#               "tags" => ["pet"]
#             },
#             "get" => %{
#               "description" => "Returns a single pet",
#               "operationId" => "getPetById",
#               "parameters" => [
#                 %{
#                   "description" => "ID of pet to return",
#                   "format" => "int64",
#                   "in" => "path",
#                   "name" => "petId",
#                   "required" => true,
#                   "type" => "integer"
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "200" => %{
#                   "description" => "successful operation",
#                   "schema" => %{"$ref" => "#/definitions/Pet"}
#                 },
#                 "400" => %{"description" => "Invalid ID supplied"},
#                 "404" => %{"description" => "Pet not found"}
#               },
#               "security" => [%{"api_key" => []}],
#               "summary" => "Find pet by ID",
#               "tags" => ["pet"]
#             },
#             "post" => %{
#               "consumes" => ["application/x-www-form-urlencoded"],
#               "description" => "",
#               "operationId" => "updatePetWithForm",
#               "parameters" => [
#                 %{
#                   "description" => "ID of pet that needs to be updated",
#                   "format" => "int64",
#                   "in" => "path",
#                   "name" => "petId",
#                   "required" => true,
#                   "type" => "integer"
#                 },
#                 %{
#                   "description" => "Updated name of the pet",
#                   "in" => "formData",
#                   "name" => "name",
#                   "required" => false,
#                   "type" => "string"
#                 },
#                 %{
#                   "description" => "Updated status of the pet",
#                   "in" => "formData",
#                   "name" => "status",
#                   "required" => false,
#                   "type" => "string"
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{"405" => %{"description" => "Invalid input"}},
#               "security" => [%{"petstore_auth" => ["write:pets", "read:pets"]}],
#               "summary" => "Updates a pet in the store with form data",
#               "tags" => ["pet"]
#             }
#           },
#           "/pet/{petId}/uploadImage" => %{
#             "post" => %{
#               "consumes" => ["multipart/form-data"],
#               "description" => "",
#               "operationId" => "uploadFile",
#               "parameters" => [
#                 %{
#                   "description" => "ID of pet to update",
#                   "format" => "int64",
#                   "in" => "path",
#                   "name" => "petId",
#                   "required" => true,
#                   "type" => "integer"
#                 },
#                 %{
#                   "description" => "Additional data to pass to server",
#                   "in" => "formData",
#                   "name" => "additionalMetadata",
#                   "required" => false,
#                   "type" => "string"
#                 },
#                 %{
#                   "description" => "file to upload",
#                   "in" => "formData",
#                   "name" => "file",
#                   "required" => false,
#                   "type" => "file"
#                 }
#               ],
#               "produces" => ["application/json"],
#               "responses" => %{
#                 "200" => %{
#                   "description" => "successful operation",
#                   "schema" => %{"$ref" => "#/definitions/ApiResponse"}
#                 }
#               },
#               "security" => [%{"petstore_auth" => ["write:pets", "read:pets"]}],
#               "summary" => "uploads an image",
#               "tags" => ["pet"]
#             }
#           },
#           "/store/inventory" => %{
#             "get" => %{
#               "description" => "Returns a map of status codes to quantities",
#               "operationId" => "getInventory",
#               "parameters" => [],
#               "produces" => ["application/json"],
#               "responses" => %{
#                 "200" => %{
#                   "description" => "successful operation",
#                   "schema" => %{
#                     "additionalProperties" => %{
#                       "format" => "int32",
#                       "type" => "integer"
#                     },
#                     "type" => "object"
#                   }
#                 }
#               },
#               "security" => [%{"api_key" => []}],
#               "summary" => "Returns pet inventories by status",
#               "tags" => ["store"]
#             }
#           },
#           "/store/order" => %{
#             "post" => %{
#               "consumes" => ["application/json"],
#               "description" => "",
#               "operationId" => "placeOrder",
#               "parameters" => [
#                 %{
#                   "description" => "order placed for purchasing the pet",
#                   "in" => "body",
#                   "name" => "body",
#                   "required" => true,
#                   "schema" => %{"$ref" => "#/definitions/Order"}
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "200" => %{
#                   "description" => "successful operation",
#                   "schema" => %{"$ref" => "#/definitions/Order"}
#                 },
#                 "400" => %{"description" => "Invalid Order"}
#               },
#               "summary" => "Place an order for a pet",
#               "tags" => ["store"]
#             }
#           },
#           "/store/order/{orderId}" => %{
#             "delete" => %{
#               "description" =>
#                 "For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors",
#               "operationId" => "deleteOrder",
#               "parameters" => [
#                 %{
#                   "description" => "ID of the order that needs to be deleted",
#                   "format" => "int64",
#                   "in" => "path",
#                   "minimum" => 1,
#                   "name" => "orderId",
#                   "required" => true,
#                   "type" => "integer"
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "400" => %{"description" => "Invalid ID supplied"},
#                 "404" => %{"description" => "Order not found"}
#               },
#               "summary" => "Delete purchase order by ID",
#               "tags" => ["store"]
#             },
#             "get" => %{
#               "description" =>
#                 "For valid response try integer IDs with value >= 1 and <= 10. Other values will generated exceptions",
#               "operationId" => "getOrderById",
#               "parameters" => [
#                 %{
#                   "description" => "ID of pet that needs to be fetched",
#                   "format" => "int64",
#                   "in" => "path",
#                   "maximum" => 10,
#                   "minimum" => 1,
#                   "name" => "orderId",
#                   "required" => true,
#                   "type" => "integer"
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "200" => %{
#                   "description" => "successful operation",
#                   "schema" => %{"$ref" => "#/definitions/Order"}
#                 },
#                 "400" => %{"description" => "Invalid ID supplied"},
#                 "404" => %{"description" => "Order not found"}
#               },
#               "summary" => "Find purchase order by ID",
#               "tags" => ["store"]
#             }
#           },
#           "/user" => %{
#             "post" => %{
#               "consumes" => ["application/json"],
#               "description" => "This can only be done by the logged in user.",
#               "operationId" => "createUser",
#               "parameters" => [
#                 %{
#                   "description" => "Created user object",
#                   "in" => "body",
#                   "name" => "body",
#                   "required" => true,
#                   "schema" => %{"$ref" => "#/definitions/User"}
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "default" => %{"description" => "successful operation"}
#               },
#               "summary" => "Create user",
#               "tags" => ["user"]
#             }
#           },
#           "/user/createWithArray" => %{
#             "post" => %{
#               "consumes" => ["application/json"],
#               "description" => "",
#               "operationId" => "createUsersWithArrayInput",
#               "parameters" => [
#                 %{
#                   "description" => "List of user object",
#                   "in" => "body",
#                   "name" => "body",
#                   "required" => true,
#                   "schema" => %{
#                     "items" => %{"$ref" => "#/definitions/User"},
#                     "type" => "array"
#                   }
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "default" => %{"description" => "successful operation"}
#               },
#               "summary" => "Creates list of users with given input array",
#               "tags" => ["user"]
#             }
#           },
#           "/user/createWithList" => %{
#             "post" => %{
#               "consumes" => ["application/json"],
#               "description" => "",
#               "operationId" => "createUsersWithListInput",
#               "parameters" => [
#                 %{
#                   "description" => "List of user object",
#                   "in" => "body",
#                   "name" => "body",
#                   "required" => true,
#                   "schema" => %{
#                     "items" => %{"$ref" => "#/definitions/User"},
#                     "type" => "array"
#                   }
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "default" => %{"description" => "successful operation"}
#               },
#               "summary" => "Creates list of users with given input array",
#               "tags" => ["user"]
#             }
#           },
#           "/user/login" => %{
#             "get" => %{
#               "description" => "",
#               "operationId" => "loginUser",
#               "parameters" => [
#                 %{
#                   "description" => "The user name for login",
#                   "in" => "query",
#                   "name" => "username",
#                   "required" => true,
#                   "type" => "string"
#                 },
#                 %{
#                   "description" => "The password for login in clear text",
#                   "in" => "query",
#                   "name" => "password",
#                   "required" => true,
#                   "type" => "string"
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "200" => %{
#                   "description" => "successful operation",
#                   "headers" => %{
#                     "X-Expires-After" => %{
#                       "description" => "date in UTC when token expires",
#                       "format" => "date-time",
#                       "type" => "string"
#                     },
#                     "X-Rate-Limit" => %{
#                       "description" => "calls per hour allowed by the user",
#                       "format" => "int32",
#                       "type" => "integer"
#                     }
#                   },
#                   "schema" => %{"type" => "string"}
#                 },
#                 "400" => %{"description" => "Invalid username/password supplied"}
#               },
#               "summary" => "Logs user into the system",
#               "tags" => ["user"]
#             }
#           },
#           "/user/logout" => %{
#             "get" => %{
#               "description" => "",
#               "operationId" => "logoutUser",
#               "parameters" => [],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "default" => %{"description" => "successful operation"}
#               },
#               "summary" => "Logs out current logged in user session",
#               "tags" => ["user"]
#             }
#           },
#           "/user/{username}" => %{
#             "delete" => %{
#               "description" => "This can only be done by the logged in user.",
#               "operationId" => "deleteUser",
#               "parameters" => [
#                 %{
#                   "description" => "The name that needs to be deleted",
#                   "in" => "path",
#                   "name" => "username",
#                   "required" => true,
#                   "type" => "string"
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "400" => %{"description" => "Invalid username supplied"},
#                 "404" => %{"description" => "User not found"}
#               },
#               "summary" => "Delete user",
#               "tags" => ["user"]
#             },
#             "get" => %{
#               "description" => "",
#               "operationId" => "getUserByName",
#               "parameters" => [
#                 %{
#                   "description" => "The name that needs to be fetched. Use user1 for testing. ",
#                   "in" => "path",
#                   "name" => "username",
#                   "required" => true,
#                   "type" => "string"
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "200" => %{
#                   "description" => "successful operation",
#                   "schema" => %{"$ref" => "#/definitions/User"}
#                 },
#                 "400" => %{"description" => "Invalid username supplied"},
#                 "404" => %{"description" => "User not found"}
#               },
#               "summary" => "Get user by user name",
#               "tags" => ["user"]
#             },
#             "put" => %{
#               "consumes" => ["application/json"],
#               "description" => "This can only be done by the logged in user.",
#               "operationId" => "updateUser",
#               "parameters" => [
#                 %{
#                   "description" => "name that need to be updated",
#                   "in" => "path",
#                   "name" => "username",
#                   "required" => true,
#                   "type" => "string"
#                 },
#                 %{
#                   "description" => "Updated user object",
#                   "in" => "body",
#                   "name" => "body",
#                   "required" => true,
#                   "schema" => %{"$ref" => "#/definitions/User"}
#                 }
#               ],
#               "produces" => ["application/json", "application/xml"],
#               "responses" => %{
#                 "400" => %{"description" => "Invalid user supplied"},
#                 "404" => %{"description" => "User not found"}
#               },
#               "summary" => "Updated user",
#               "tags" => ["user"]
#             }
#           }
#         },
#         "schemes" => ["https", "http"],
#         "securityDefinitions" => %{
#           "api_key" => %{"in" => "header", "name" => "api_key", "type" => "apiKey"},
#           "petstore_auth" => %{
#             "authorizationUrl" => "https://petstore.swagger.io/oauth/authorize",
#             "flow" => "implicit",
#             "scopes" => %{
#               "read:pets" => "read your pets",
#               "write:pets" => "modify pets in your account"
#             },
#             "type" => "oauth2"
#           }
#         },
#         "swagger" => "2.0",
#         "tags" => [
#           %{
#             "description" => "Everything about your Pets",
#             "externalDocs" => %{
#               "description" => "Find out more",
#               "url" => "http://swagger.io"
#             },
#             "name" => "pet"
#           },
#           %{"description" => "Access to Petstore orders", "name" => "store"},
#           %{
#             "description" => "Operations about user",
#             "externalDocs" => %{
#               "description" => "Find out more about our store",
#               "url" => "http://swagger.io"
#             },
#             "name" => "user"
#           }
#         ]
#       }
#     }
#   end

#   def default_config_schema() do
#     %{
#       "$schema" => "http=>//json-schema.org/draft-07/schema",
#       "$id" => "http=>//example.com/example.json",
#       "type" => "object",
#       "title" => "The Root Schema",
#       "description" => "Schema to describe the Trixta configuration for a Swagger service",
#       "default" => %{},
#       "additionalProperties" => true,
#       "required" => [
#         "tags",
#         "http_schemes",
#         "host",
#         "base_path",
#         "auth"
#       ],
#       "properties" => %{
#         "tags" => %{
#           "$id" => "#/properties/tags",
#           "type" => "array",
#           "title" => "Service Tags",
#           "description" => "Tags that can be used to describe a service.",
#           "default" => [],
#           "examples" => [
#             [
#               "swagger",
#               "service"
#             ]
#           ],
#           "additionalItems" => true,
#           "items" => %{
#             "$id" => "#/properties/tags/items",
#             "type" => "string",
#             "title" => "Service Tags",
#             "description" => "Tags that can be used to describe a service.",
#             "default" => "",
#             "examples" => [
#               "swagger",
#               "service"
#             ]
#           }
#         },
#         "http_schemes" => %{
#           "$id" => "#/properties/http_schemes",
#           "type" => "array",
#           "title" => "The Http_schemes Schema",
#           "description" => "The protocol of communication.",
#           "default" => [],
#           "examples" => [
#             [
#               "https"
#             ]
#           ],
#           "additionalItems" => true,
#           "items" => %{
#             "$id" => "#/properties/http_schemes/items",
#             "type" => "string",
#             "title" => "The Items Schema",
#             "description" => "Protocol options.",
#             "default" => "",
#             "examples" => [
#               "https",
#               "http"
#             ]
#           }
#         },
#         "host" => %{
#           "$id" => "#/properties/host",
#           "type" => "string",
#           "title" => "The Host Schema",
#           "description" => "Host url",
#           "default" => "",
#           "examples" => [
#             "api.x.com"
#           ]
#         },
#         "description" => %{
#           "$id" => "#/properties/description",
#           "type" => "string",
#           "title" => "The Description Schema",
#           "description" => "Description of the given service.",
#           "default" => "",
#           "examples" => [
#             "This API provides current weather information for US Cities, including temperatures, wind speeds, wind direction, relative humidity, and visibility."
#           ]
#         },
#         "base_path" => %{
#           "$id" => "#/properties/base_path",
#           "type" => "string",
#           "title" => "The Base_path Schema",
#           "description" => "Description of the given service.",
#           "default" => "",
#           "examples" => [
#             ""
#           ]
#         },
#         "auth" => %{
#           "$id" => "#/properties/auth",
#           "type" => "array",
#           "title" => "The Auth Schema",
#           "description" => "Authentication options for the service",
#           "default" => [],
#           "examples" => [
#             [
#               %{
#                 "key_name" => "key_value",
#                 "key_value" => "1234",
#                 "type" => "api_key",
#                 "in" => "header"
#               },
#               %{
#                 "authorizationUrl" => "api.x.com",
#                 "token" => "1234",
#                 "type" => "oauth2"
#               },
#               %{
#                 "username" => "name",
#                 "type" => "basic",
#                 "password" => "1234"
#               }
#             ]
#           ],
#           "additionalItems" => true,
#           "items" => %{
#             "$id" => "#/properties/auth/items",
#             "type" => "object",
#             "title" => "The Items Schema",
#             "description" => "Authentication option examples.",
#             "default" => [],
#             "examples" => [
#               %{
#                 "key_name" => "key_value",
#                 "key_value" => "1234",
#                 "type" => "api_key",
#                 "in" => "header"
#               },
#               %{
#                 "type" => "oauth2",
#                 "authorizationUrl" => "api.x.com",
#                 "token" => "1234"
#               },
#               %{
#                 "type" => "basic",
#                 "password" => "1234",
#                 "username" => "name"
#               }
#             ],
#             "additionalProperties" => true,
#             "required" => [
#               "type"
#             ],
#             "properties" => %{
#               "type" => %{
#                 "$id" => "#/properties/auth/items/properties/type",
#                 "type" => "string",
#                 "title" => "The Type Schema",
#                 "description" => "Type of authentication.",
#                 "default" => "",
#                 "examples" => [
#                   "api_key",
#                   "oauth2",
#                   "basic"
#                 ]
#               },
#               "key_name" => %{
#                 "$id" => "#/properties/auth/items/properties/key_name",
#                 "type" => "string",
#                 "title" => "The Key_name Schema",
#                 "description" => "Key of the value used in this type of auth.",
#                 "default" => "",
#                 "examples" => [
#                   "key_value"
#                 ]
#               },
#               "in" => %{
#                 "$id" => "#/properties/auth/items/properties/in",
#                 "type" => "string",
#                 "title" => "The In Schema",
#                 "description" => "Location of api key in http request",
#                 "default" => "",
#                 "examples" => [
#                   "header"
#                 ]
#               },
#               "key_value" => %{
#                 "$id" => "#/properties/auth/items/properties/key_value",
#                 "type" => "string",
#                 "title" => "The Key_value Schema",
#                 "format" => "password",
#                 "description" => "An explanation about the purpose of this instance.",
#                 "default" => "",
#                 "examples" => [
#                   "1234"
#                 ]
#               }
#             }
#           }
#         }
#       }
#     }
#   end
# end
