# defmodule TrixtaLibrary.Services.Ingesters.AwsService do
#   require Logger

#   '''
#   NEEDS:
#   - service_name :comprehend
#   - target: ComprehendMedical_20181030.DetectEntitiesV2
#   - target_namespace: comprehendmedical
#   - region: us-west-2
#   - credentials:
#     - aws_access_id
#     - aws_access_key

#   Per Call:
#   - Text
#   '''

#   def register_service(
#         type,
#         name,
#         %{
#           "service_name" => service_name,
#           "target_namespace" => target_namespace,
#           "targets" => targets,
#           "region" => region,
#           "credentials" =>
#             %{"aws_access_id" => aws_access_id, "aws_access_key" => aws_access_key} = credentials
#         } = payload
#       ) do
#     try do
#       # Service Settings
#       service_config = %{
#         "service_name" => service_name,
#         "target_namespace" => target_namespace,
#         "region" => region,
#         "credentials" => credentials,
#         "tags" => ["aws"]
#       }

#       reaction_names =
#         Enum.map(targets, fn reaction ->
#           reaction_name =
#             reaction
#             |> String.replace("/", "_")
#             |> String.replace("}", "")
#             |> String.replace("{", "")
#             |> String.replace_trailing("_", "")
#             |> String.downcase()

#           TrixtaLibrary.Services.ServiceManager.set_service_reaction(
#             name,
#             reaction_name,
#             %{"reaction" => reaction}
#           )

#           reaction_name
#         end)

#       # Generate auth_settings and append it to the :auth section of the service_config
#       TrixtaLibrary.Services.ServiceManager.set_service(
#         name,
#         service_config,
#         type,
#         reaction_names
#       )

#       TrixtaLibrary.Services.ServiceManager.set_service_config_schema(
#         name,
#         TrixtaLibrary.Services.AwsService.default_config_schema()
#       )

#       {:ok, %{"message" => "Successfully registered AWS service: #{name}"}}
#     rescue
#       e ->
#         Logger.error("Could not register AWS service. #{e.message}")
#         {:error, %{"details" => "Could not register AWS service. #{e.message}"}}
#     end
#   end

#   def register_service(type, name, _) do
#     example_format = %{
#       "service_name" => "comprehend",
#       "target_namespace" => "comprehendmedical",
#       "targets" => [
#         "ComprehendMedical_20181030.DetectEntitiesV2",
#         "ComprehendMedical_20181030.DetectPHI"
#       ],
#       "region" => "us-west-2",
#       "credentials" => %{"aws_access_id" => "123", "aws_access_key" => "1234"}
#     }

#     Logger.error(
#       "Unable to register service as the payload was incorrect. Please use the format: #{
#         inspect(example_format)
#       }"
#     )

#     {:error,
#      "Unable to register service as the payload was incorrect. Please use the format: #{
#        inspect(example_format)
#      }"}
#   end
# end
