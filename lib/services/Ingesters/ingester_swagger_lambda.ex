# defmodule TrixtaLibrary.Services.Ingesters.SwaggerLambda do
#   require Logger

#   def register_service(type, name, payload) do
#     try do
#       # Service Settings
#       service_config = %{
#         "host" => payload["host"],
#         "base_path" => payload["basePath"] || "",
#         "http_schemes" => payload["schemes"] || ["https"],
#         "auth" => populate_auth(payload["securityDefinitions"] || ""),
#         "tags" => ["swagger"]
#       }

#       # Generate paths
#       reaction_names =
#         payload["paths"]
#         |> Enum.map(fn {path, http_methods} ->
#           reaction_name =
#             path
#             |> String.replace("/", "_")
#             |> String.replace("}", "")
#             |> String.replace("{", "")
#             |> String.replace_trailing("_", "")
#             |> String.downcase()

#           http_methods
#           |> Enum.map(fn {method, method_settings} ->
#             parameters = generate_parameters(method_settings["parameters"])
#             schema = generate_schema(payload["definitions"], method_settings["parameters"])

#             case method do
#               "x-amazon-apigateway-any-method" ->
#                 Enum.map([:get, :put, :delete, :post], fn http_method ->
#                   reaction_settings = %{
#                     "name" => "#{http_method}#{reaction_name}",
#                     "path" => path,
#                     "method" => http_method,
#                     "parameters" => parameters,
#                     "schema" => schema
#                   }

#                   TrixtaLibrary.Services.ServiceManager.set_service_reaction(
#                     name,
#                     reaction_settings.name,
#                     reaction_settings
#                   )

#                   reaction_settings.name
#                 end)

#               _ ->
#                 reaction_settings = %{
#                   "name" => "#{method}#{reaction_name}",
#                   "path" => path,
#                   "method" => String.to_existing_atom(method),
#                   "parameters" => parameters,
#                   "schema" => schema
#                 }

#                 TrixtaLibrary.Services.ServiceManager.set_service_reaction(
#                   name,
#                   reaction_settings["name"],
#                   reaction_settings
#                 )

#                 reaction_settings["name"]
#             end
#           end)
#         end)
#         |> List.flatten()

#       # Generate auth_settings and append it to the :auth section of the service_config
#       TrixtaLibrary.Services.ServiceManager.set_service(
#         name,
#         service_config,
#         type,
#         reaction_names
#       )

#       TrixtaLibrary.Services.ServiceManager.set_service_config_schema(
#         name,
#         TrixtaLibrary.Services.HttpService.default_config_schema()
#       )

#       {:ok, %{"message" => "Successfully registered Lambda service: #{name}"}}
#     rescue
#       e ->
#         Logger.error("Could not register Lambda service. #{e.message}")
#         {:error, %{"details" => "Could not register Lambda service. #{e.message}"}}
#     end
#   end

#   defp generate_parameters(nil) do
#     %{}
#   end

#   defp generate_parameters(parameters \\ %{}) do
#     Enum.map(parameters, fn param ->
#       %{"name" => param["name"], "in" => param["in"]}
#     end)
#   end

#   defp generate_schema(definitions, nil) do
#     %{}
#   end

#   defp generate_schema(definitions, schema \\ %{}) do
#     # TODO: Collin, lots of error handling and nil value parsing needed here.
#     Enum.reduce(schema, %{"type" => "object"}, fn input_value, acc ->
#       if input_value["type"] == nil do
#         schema_key =
#           input_value["schema"]["$ref"]
#           |> String.split("/")
#           |> List.last()

#         Map.put(acc, input_value["name"], definitions[schema_key])
#       else
#         Map.put(acc, input_value["name"], %{"type" => input_value["type"]})
#       end
#     end)
#   end

#   defp populate_auth("") do
#     Logger.info("No authentication supplied for service.")
#     %{}
#   end

#   defp populate_auth(nil) do
#     Logger.info("No authentication supplied for service.")
#     %{}
#   end

#   defp populate_auth(payload) do
#     Enum.map(payload, fn {auth_name, auth_body} ->
#       case auth_body["type"] do
#         "apiKey" ->
#           %{
#             "type" => "api_key",
#             "key_name" => Map.get(auth_body, "name"),
#             "in" => Map.get(auth_body, "in"),
#             "key_value" => ""
#           }

#         "oauth2" ->
#           %{
#             "type" => "oauth2",
#             "authorizationUrl" => Map.get(auth_body, "authorizationUrl"),
#             "token" => ""
#           }

#         "basic" ->
#           %{
#             "type" => "basic",
#             "in" => "header",
#             "token" => ""
#           }

#         _ ->
#           Logger.info("Unsupported auth type")
#           %{}
#       end
#     end)
#     |> Enum.filter(&(!is_nil(&1)))
#   end
# end
