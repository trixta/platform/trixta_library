# defmodule TrixtaLibrary.Services.Ingesters.SwaggerV3 do
#   require Logger

#   def register_service(type, name, payload) do
#     try do
#       [scheme, host] =
#         case Map.get(payload, "servers") do
#           nil ->
#             raise "Unable to create service. Could not get server information from payload, nil value."

#           servers ->
#             List.first(servers)
#             |> Map.get("url")
#             |> String.split("://")
#         end

#       components = payload["components"] || ""
#       # Service Settings
#       service_config = %{
#         "host" => host,
#         "base_path" => "",
#         "http_schemes" => scheme,
#         "auth" => populate_auth(components["securitySchemes"] || ""),
#         "description" => payload["info"]["description"] || "",
#         "tags" => ["swagger"]
#       }

#       # Generate Paths
#       reaction_names =
#         payload["paths"]
#         |> Enum.map(fn {path, http_methods} ->
#           reaction_name =
#             path
#             |> String.replace("/", "_")
#             |> String.replace("}", "")
#             |> String.replace("{", "")
#             |> String.replace_trailing("_", "")
#             |> String.downcase()

#           http_methods
#           |> Enum.map(fn {method, method_settings} ->
#             reaction_settings = %{
#               "name" => "#{method}#{reaction_name}",
#               "path" => path,
#               "method" => String.to_existing_atom(method),
#               "parameters" => generate_parameters(method_settings["parameters"]),
#               "schema" => generate_schema(components, method_settings),
#               "description" => method_settings["description"] || ""
#             }

#             TrixtaLibrary.Services.ServiceManager.set_service_reaction(
#               name,
#               reaction_settings["name"],
#               reaction_settings
#             )

#             reaction_settings["name"]
#           end)
#         end)
#         |> List.flatten()

#       # Generate auth_settings and append it to the :auth section of the service_config
#       TrixtaLibrary.Services.ServiceManager.set_service(
#         name,
#         service_config,
#         type,
#         reaction_names
#       )

#       TrixtaLibrary.Services.ServiceManager.set_service_config_schema(
#         name,
#         TrixtaLibrary.Services.HttpService.default_config_schema()
#       )

#       {:ok, %{"message" => "Successfully registered HTTP service: #{name}"}}
#     rescue
#       e ->
#         Logger.error("Could not register HTTP service. #{e.message}")
#         {:error, %{"details" => "Could not register HTTP service. #{e.message}"}}
#     end
#   end

#   defp generate_parameters(nil) do
#     %{}
#   end

#   defp generate_parameters(parameters) do
#     Enum.map(parameters || %{}, fn param ->
#       %{"name" => param["name"], "in" => param["in"]}
#     end)
#   end

#   def generate_schema(components, nil) do
#     %{}
#   end

#   def generate_schema(components, method_settings) do
#     # TODO: Collin, lots of error handling and nil value parsing needed here.
#     schema = %{}

#     # Handling for parameter types. These will all have a schema and name.
#     if Map.has_key?(method_settings, "parameters") do
#       Enum.each(method_settings["parameters"], fn parameter ->
#         Map.put(schema, parameter["name"], %{"type" => parameter["schema"]["type"]})
#       end)
#     end

#     # Handling for RequestBody types. These will be references in components.
#     # NOTE: This needs improvement and proper parsing. For now we're just including the request body as a full object
#     # without parsing our the $ref values.
#     if Map.has_key?(method_settings, "requestBody") do
#       Map.merge(schema, method_settings["requestBody"])
#     end
#   end

#   defp populate_auth("") do
#     Logger.info("No authentication supplied for service.")
#     %{}
#   end

#   defp populate_auth(nil) do
#     Logger.info("No authentication supplied for service.")
#     %{}
#   end

#   defp populate_auth(payload) do
#     Enum.map(payload, fn {auth_name, auth_body} ->
#       case auth_body["type"] do
#         "apiKey" ->
#           %{
#             "type" => "api_key",
#             "key_name" => Map.get(auth_body, "name"),
#             "in" => Map.get(auth_body, "in"),
#             "key_value" => ""
#           }

#         "oauth2" ->
#           %{
#             "type" => "oauth2",
#             "authorizationUrl" => Map.get(auth_body, "authorizationUrl"),
#             "token" => ""
#           }

#         "BasicAuth" ->
#           %{
#             "type" => "basic",
#             "username" => Map.get(auth_body, "username"),
#             "password" => Map.get(auth_body, "password"),
#             "description" => Map.get(auth_body, "description")
#           }

#         _ ->
#           Logger.info("Unsupported auth type")
#           %{}
#       end
#     end)
#     |> Enum.filter(&(!is_nil(&1)))
#   end
# end
