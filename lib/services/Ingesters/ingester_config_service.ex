# defmodule TrixtaLibrary.Services.Ingesters.ConfigService do
#   require Logger

#   def register_service(type, name, payload) do
#     try do
#       payload_with_tags = Map.put(payload, "tags", ["config_service"])

#       TrixtaLibrary.Services.ServiceManager.set_service(
#         name,
#         payload_with_tags,
#         type,
#         []
#       )

#       {:ok, %{"message" => "Successfully registered Config service: #{name}"}}
#     rescue
#       e ->
#         Logger.error("Could not register config service. #{e.message}")
#         {:error, %{"details" => "Could not register config service. #{e.message}"}}
#     end
#   end
# end
