# defmodule TrixtaLibrary.Services.AwsService.Reaction do
#   @enforce_keys [:name, :path, :method]
#   defstruct [:name, :path, :method, :schema]
# end

# defmodule TrixtaLibrary.Services.AwsService do
#   require Logger

#   def run_service_call(
#         %{"config" => %{"target_namespace" => "s3"}} = service,
#         reaction,
#         payload
#       ) do
#     try do
#       service_config = Map.get(service, "config")

#       case generate_ex_aws_s3_config_from_service_config(service_config) do
#         config ->
#           operation =
#             case reaction["reaction"] do
#               "ListObjects" ->
#                 ExAws.S3.list_objects(payload["bucket"])

#               "GetObject" ->
#                 ExAws.S3.get_object(payload["bucket"], payload["filename"])

#               "PutObject" ->
#                 ExAws.S3.put_object(payload["bucket"], payload["filename"], payload["data"])
#             end

#           case ExAws.request(operation, config) do
#             {:ok, response} ->
#               Logger.info("Successful ExAws request!")

#               content_type =
#                 Enum.into(response[:headers], %{})
#                 |> Map.get("Content-Type")

#               body = Map.get(response, :body)

#               if body == "" || body == nil do
#                 body == %{}
#               end

#               case content_type do
#                 x when x in ["image/jpeg", "image/png"] ->
#                   Logger.info("Image content detected, decoding...")
#                   {"success", %{"response" => Base.encode64(IO.iodata_to_binary(response.body))}}

#                 "application/xml" ->
#                   Logger.info("XML content detected, returning normally...")
#                   {"success", %{"response" => body}}

#                 "application/octet-stream" ->
#                   Logger.info("Stream content detected, returning normally...")
#                   {"success", %{"response" => body}}

#                 _ ->
#                   Logger.info("No Content-Type detected, attempting decode to valid objects...")
#                   {"success", %{"response" => Poison.decode!(body)}}
#               end

#             {:error, {_, _, reason}} ->
#               Logger.error("AWS S3 Service request failed: #{reason}")
#               {:error, %{"details" => "AWS Service request failed: #{reason}"}}

#             _ ->
#               Logger.error("AWS S3 Service request failed. Unknown error.")
#               {:error, %{"details" => "AWS Service request failed. Unknown error."}}
#           end

#         nil ->
#           Logger.error("AWS S3 Service request failed. Unknown error, nil config")

#           {:error,
#            %{
#              "details" =>
#                "Could not build ex_aws_config properly, please check your config settings."
#            }}
#       end
#     rescue
#       e ->
#         Logger.error("AWS S3 Service call failed with error: #{e.message}")
#         {:error, %{"details" => "AWS Service call failed with error: #{e.message}"}}
#     end
#   end

#   def run_service_call(service, reaction, payload \\ %{}) do
#     try do
#       service_config = Map.get(service, "config")
#       payload = Map.merge(payload, %{"LanguageCode" => "en"})

#       case generate_ex_aws_config_from_service_config(service_config) do
#         config ->
#           case ExAws.Request.request(
#                  :post,
#                  config["host"],
#                  Poison.encode!(payload),
#                  [
#                    {"X-Amz-Target", reaction["reaction"]},
#                    {"Content-Type", "application/x-amz-json-1.1"}
#                  ],
#                  config,
#                  String.to_atom(service_config["target_namespace"])
#                ) do
#             {:ok, response} ->
#               Logger.info("Successful ExAws request!")
#               {"success", %{"response" => Poison.decode!(Map.get(response, :body))}}

#             {:error, {_, _, reason}} ->
#               Logger.error("AWS Service request failed: #{reason}")
#               {:error, %{"details" => "AWS Service request failed: #{reason}"}}

#             _ ->
#               Logger.error("AWS Service request failed. Unknown error.")
#               {:error, %{"details" => "AWS Service request failed. Unknown error."}}
#           end

#         nil ->
#           Logger.error("AWS Service request failed. Unknown error, nil config")

#           {:error,
#            %{
#              "details" =>
#                "Could not build ex_aws_config properly, please check your config settings."
#            }}
#       end
#     rescue
#       e ->
#         Logger.error("AWS Service call failed with error: #{e.message}")
#         {:error, %{"details" => "AWS Service call failed with error: #{e.message}"}}
#     end
#   end

#   def generate_ex_aws_config_from_service_config(service_config) do
#     ex_aws_config =
#       ExAws.Config.new(String.to_atom(service_config["service_name"]), [
#         {:access_key_id, service_config["credentials"]["aws_access_id"]},
#         {:secret_access_key, service_config["credentials"]["aws_access_key"]},
#         {:region, service_config["region"]}
#       ])
#       |> Map.put(
#         :host,
#         "https://#{service_config["target_namespace"]}.#{service_config["region"]}.amazonaws.com/"
#       )
#   end

#   def generate_ex_aws_s3_config_from_service_config(service_config) do
#     ex_aws_config =
#       ExAws.Config.new(String.to_atom(service_config["service_name"]), [
#         {:access_key_id, service_config["credentials"]["aws_access_id"]},
#         {:secret_access_key, service_config["credentials"]["aws_access_key"]},
#         {:region, service_config["region"]}
#       ])
#   end

#   def ui_example() do
#     %{
#       s3_example: %{
#         service_name: "s3",
#         target_namespace: "s3",
#         targets: [
#           "ListObjects",
#           "GetObject",
#           "PutObject"
#         ],
#         region: "eu-west-2",
#         credentials: %{
#           aws_access_id: "1234",
#           aws_access_key: "1234"
#         }
#       }
#     }
#   end

#   def default_config_schema() do
#     %{
#       "$schema" => "http =>//json-schema.org/draft-07/schema",
#       "$id" => "http =>//example.com/example.json",
#       "type" => "object",
#       "title" => "Configuration for Trixta AWS Service intergration",
#       "description" => "AWS Service integration configuration",
#       "default" => %{},
#       "additionalProperties" => true,
#       "required" => [
#         "target_namespace",
#         "tags",
#         "service_name",
#         "region",
#         "credentials"
#       ],
#       "properties" => %{
#         "target_namespace" => %{
#           "$id" => "#/properties/target_namespace",
#           "type" => "string",
#           "title" => "The target namespace Schema",
#           "description" => "The namespace of the AWS service",
#           "default" => "",
#           "examples" => [
#             "s3"
#           ]
#         },
#         "tags" => %{
#           "$id" => "#/properties/tags",
#           "type" => "array",
#           "title" => "The Tags Schema",
#           "description" => "Trita tags associated with the tags",
#           "default" => [["service", "aws"]],
#           "examples" => [
#             [
#               "aws"
#             ]
#           ],
#           "additionalItems" => true,
#           "items" => %{
#             "$id" => "#/properties/tags/items",
#             "type" => "string",
#             "title" => "The Items Schema",
#             "description" => "Tags for the service",
#             "default" => "service",
#             "examples" => [
#               "aws",
#               "service"
#             ]
#           }
#         },
#         "service_name" => %{
#           "$id" => "#/properties/service_name",
#           "type" => "string",
#           "title" => "The Service Name Schema",
#           "description" => "The name of the service",
#           "default" => "",
#           "examples" => [
#             "s3"
#           ]
#         },
#         "region" => %{
#           "$id" => "#/properties/region",
#           "type" => "string",
#           "title" => "The Region Schema",
#           "description" =>
#             "AWS region used by name space. Mak sure the service is available in the region.",
#           "default" => "",
#           "examples" => [
#             "eu-west-2"
#           ]
#         },
#         "credentials" => %{
#           "$id" => "#/properties/credentials",
#           "type" => "object",
#           "title" => "The Credentials Schema",
#           "description" => "AWS credentials. Includes access id and key.",
#           "default" => %{},
#           "examples" => [
#             %{
#               "aws_access_key" => "1234",
#               "aws_access_id" => "1234"
#             }
#           ],
#           "additionalProperties" => true,
#           "required" => [
#             "aws_access_key",
#             "aws_access_id"
#           ],
#           "properties" => %{
#             "aws_access_key" => %{
#               "$id" => "#/properties/credentials/properties/aws_access_key",
#               "type" => "string",
#               "title" => "The Aws_access_key Schema",
#               "format" => "password",
#               "description" => "AWS access key, generated within AWS permissions",
#               "default" => "",
#               "examples" => [
#                 "1234GSASD1"
#               ]
#             },
#             "aws_access_id" => %{
#               "$id" => "#/properties/credentials/properties/aws_access_id",
#               "type" => "string",
#               "format" => "password",
#               "title" => "The Aws_access_id Schema",
#               "description" => "Identifier for the AWS access, related to AWS access key",
#               "default" => "",
#               "examples" => [
#                 "123SGD4%!ASD45&*GS"
#               ]
#             }
#           }
#         }
#       }
#     }
#   end
# end
