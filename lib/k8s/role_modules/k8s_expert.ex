defmodule TrixtaLibrary.K8s.K8sExpert do
  # INPUT
  # "kubernetes_url":
  # "kubernetes_token":
  # "kubernetes_ca_cert":
  # "namespace_name":
  # "docker_image_path":
  # "gitlab_registry_token":
  # "secrets":
  # "trixta_space_env":
  # "ingress_secrets":

  # These constant values apply to the ingress controller deployment.
  # We don't want these to be passed in from the front-end wizard,
  # because the user shouldn't normally have to care about these.
  @ingress_configmap_data %{
    "proxy-body-size" => "512m",
    "proxy-buffer-size" => "64k",
    "proxy-read-timeout" => "180",
    "proxy-send-timeout" => "180",
    "server-tokens" => "false"
  }
  @ingress_image_path "quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.26.1"
  @ingress_backend_image_path "k8s.gcr.io/defaultbackend-amd64:1.5"
  # Currently all the secrets are within trixta_space. The ingress backend doesn't have any.
  # We keep the field for completeness until we are sure it can be dropped.
  @ingress_backend_secrets %{}

  # Not sure about services cluster_ip

  def get_kazan_server(input_data) do
    kazan_server =
      Kazan.Server.from_env(%{
        url: input_data["kubernetes_url"],
        ca_cert: input_data["kubernetes_ca_cert"],
        auth: %{token: input_data["kubernetes_token"]}
      })

    # kazan_server = Kazan.Server.from_kubeconfig(input_data["kubeconfig_path"], [context: input_data["context"]])

    {"success", kazan_server}
  end

  def create_ingress_namespace(kazan_server, input_data) do
    response =
      Kazan.Apis.Core.V1.create_namespace!(%Kazan.Apis.Core.V1.Namespace{
        api_version: "v1",
        kind: "Namespace",
        metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
          labels: %{"name" => "nginx-ingress-#{input_data["namespace_name"]}"},
          name: "nginx-ingress-#{input_data["namespace_name"]}"
        },
        spec: %Kazan.Apis.Core.V1.NamespaceSpec{finalizers: ["kubernetes"]},
        status: %Kazan.Apis.Core.V1.NamespaceStatus{phase: "Active"}
      })
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_namespace(kazan_server, input_data) do
    response =
      Kazan.Apis.Core.V1.create_namespace!(%Kazan.Apis.Core.V1.Namespace{
        api_version: "v1",
        kind: "Namespace",
        metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
          labels: %{"name" => input_data["namespace_name"]},
          name: input_data["namespace_name"]
        },
        spec: %Kazan.Apis.Core.V1.NamespaceSpec{finalizers: ["kubernetes"]},
        status: %Kazan.Apis.Core.V1.NamespaceStatus{phase: "Active"}
      })
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_secrets(kazan_server, input_data) do
    # "gitlab-registry"
    Kazan.Apis.Core.V1.create_namespaced_secret!(
      %Kazan.Apis.Core.V1.Secret{
        api_version: nil,
        data: %{
          ".dockerconfigjson" => input_data["gitlab_registry_token"]
        },
        kind: nil,
        metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
          name: "gitlab-registry",
          namespace: input_data["namespace_name"]
        },
        string_data: nil,
        type: "kubernetes.io/dockerconfigjson"
      },
      input_data["namespace_name"]
    )
    |> Kazan.run!(server: kazan_server)

    # "trixta-space-env-secret"
    Kazan.Apis.Core.V1.create_namespaced_secret!(
      %Kazan.Apis.Core.V1.Secret{
        api_version: nil,
        data: input_data["secrets"],
        kind: nil,
        metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
          labels: %{"app" => "trixta-space"},
          name: "trixta-space-env-secret",
          namespace: input_data["namespace_name"]
        },
        string_data: nil,
        type: "Opaque"
      },
      input_data["namespace_name"]
    )
    |> Kazan.run!(server: kazan_server)

    {"success", %{}}
  end

  def create_ingress_secrets(kazan_server, input_data) do
    # "ingress-nginx-backend"
    Kazan.Apis.Core.V1.create_namespaced_secret!(
      %Kazan.Apis.Core.V1.Secret{
        api_version: nil,
        data: @ingress_backend_secrets,
        kind: nil,
        metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
          annotations: %{
            "kubernetes.io/service-account.name" =>
              "nginx-ingress-#{input_data["namespace_name"]}-backend"
          },
          name: "nginx-ingress-#{input_data["namespace_name"]}-backend-token",
          namespace: "nginx-ingress-#{input_data["namespace_name"]}"
        },
        string_data: nil,
        type: "kubernetes.io/service-account-token"
      },
      "nginx-ingress-#{input_data["namespace_name"]}"
    )
    |> Kazan.run!(server: kazan_server)

    # We need to add the 'namespace' field to the ingress secrets,
    # which is derived from the namespace in the input data, Base64 encoded:
    encoded_namespace = "nginx-ingress-#{input_data["namespace_name"]}" |> Base.encode64()

    # "ingress-nginx"
    Kazan.Apis.Core.V1.create_namespaced_secret!(
      %Kazan.Apis.Core.V1.Secret{
        api_version: nil,
        data: input_data["ingress_secrets"] |> Map.put("namespace", encoded_namespace),
        kind: nil,
        metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
          annotations: %{
            "kubernetes.io/service-account.name" =>
              "nginx-ingress-#{input_data["namespace_name"]}"
          },
          name: "nginx-ingress-#{input_data["namespace_name"]}-token",
          namespace: "nginx-ingress-#{input_data["namespace_name"]}"
        },
        string_data: nil,
        type: "kubernetes.io/service-account-token"
      },
      "nginx-ingress-#{input_data["namespace_name"]}"
    )
    |> Kazan.run!(server: kazan_server)

    {"success", %{}}
  end

  def create_ingress_config_map(kazan_server, input_data) do
    response =
      Kazan.Apis.Core.V1.create_namespaced_config_map!(
        %Kazan.Apis.Core.V1.ConfigMap{
          api_version: nil,
          binary_data: nil,
          data: @ingress_configmap_data,
          kind: nil,
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            labels: %{
              "app" => "nginx-ingress",
              "release" => "nginx-ingress-#{input_data["namespace_name"]}"
            },
            name: "nginx-ingress-#{input_data["namespace_name"]}-controller",
            namespace: "nginx-ingress-#{input_data["namespace_name"]}"
          }
        },
        "nginx-ingress-#{input_data["namespace_name"]}"
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_config_map(kazan_server, input_data) do
    response =
      Kazan.Apis.Core.V1.create_namespaced_config_map!(
        %Kazan.Apis.Core.V1.ConfigMap{
          api_version: nil,
          binary_data: nil,
          data: input_data["trixta_space_env"],
          kind: nil,
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            labels: %{"app" => "trixta-space"},
            name: "trixta-space-env",
            namespace: input_data["namespace_name"]
          }
        },
        input_data["namespace_name"]
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_persistent_volume_claim(kazan_server, input_data) do
    response =
      Kazan.Apis.Core.V1.create_namespaced_persistent_volume_claim!(
        %Kazan.Apis.Core.V1.PersistentVolumeClaim{
          api_version: nil,
          kind: nil,
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            annotations: %{
              "volume.beta.kubernetes.io/storage-class" => "efs"
            },
            name: "trixta-space-dets-pvc",
            namespace: input_data["namespace_name"]
          },
          spec: %Kazan.Apis.Core.V1.PersistentVolumeClaimSpec{
            access_modes: ["ReadWriteMany"],
            resources: %Kazan.Apis.Core.V1.ResourceRequirements{
              requests: %{"storage" => "8Gi"}
            }
          },
          status: nil
        },
        input_data["namespace_name"]
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_ingress_namespaced_deployment(kazan_server, input_data) do
    response =
      Kazan.Apis.Apps.V1.create_namespaced_deployment!(
        %Kazan.Apis.Apps.V1.Deployment{
          api_version: nil,
          kind: nil,
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            annotations: %{"deployment.kubernetes.io/revision" => "1"},
            labels: %{
              "app" => "nginx-ingress",
              "chart" => "nginx-ingress-1.24.2",
              "component" => "controller",
              "heritage" => "Tiller",
              "release" => "nginx-ingress-#{input_data["namespace_name"]}"
            },
            name: "nginx-ingress-#{input_data["namespace_name"]}-controller",
            namespace: "nginx-ingress-#{input_data["namespace_name"]}"
          },
          spec: %Kazan.Apis.Apps.V1.DeploymentSpec{
            progress_deadline_seconds: 600,
            replicas: 1,
            revision_history_limit: 10,
            selector: %Kazan.Models.Apimachinery.Meta.V1.LabelSelector{
              match_labels: %{
                "app" => "nginx-ingress",
                "release" => "nginx-ingress-#{input_data["namespace_name"]}"
              }
            },
            strategy: %Kazan.Apis.Apps.V1.DeploymentStrategy{
              rolling_update: %Kazan.Apis.Apps.V1.RollingUpdateDeployment{
                max_surge: "25%",
                max_unavailable: "25%"
              },
              type: "RollingUpdate"
            },
            template: %Kazan.Apis.Core.V1.PodTemplateSpec{
              metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
                creation_timestamp: nil,
                labels: %{
                  "app" => "nginx-ingress",
                  "component" => "controller",
                  "release" => "nginx-ingress-#{input_data["namespace_name"]}"
                }
              },
              spec: %Kazan.Apis.Core.V1.PodSpec{
                containers: [
                  %Kazan.Apis.Core.V1.Container{
                    args: [
                      "/nginx-ingress-controller",
                      "--default-backend-service=nginx-ingress-#{input_data["namespace_name"]}/nginx-ingress-#{
                        input_data["namespace_name"]
                      }-default-backend",
                      "--election-id=ingress-controller-leader",
                      "--ingress-class=nginx-ingress-#{input_data["namespace_name"]}",
                      "--configmap=nginx-ingress-#{input_data["namespace_name"]}/nginx-ingress-#{
                        input_data["namespace_name"]
                      }-controller"
                    ],
                    env: [
                      %Kazan.Apis.Core.V1.EnvVar{
                        name: "POD_NAME",
                        value_from: %Kazan.Apis.Core.V1.EnvVarSource{
                          field_ref: %Kazan.Apis.Core.V1.ObjectFieldSelector{
                            api_version: "v1",
                            field_path: "metadata.name"
                          }
                        }
                      },
                      %Kazan.Apis.Core.V1.EnvVar{
                        name: "POD_NAMESPACE",
                        value_from: %Kazan.Apis.Core.V1.EnvVarSource{
                          field_ref: %Kazan.Apis.Core.V1.ObjectFieldSelector{
                            api_version: "v1",
                            field_path: "metadata.namespace"
                          }
                        }
                      }
                    ],
                    image: @ingress_image_path,
                    image_pull_policy: "IfNotPresent",
                    lifecycle: nil,
                    liveness_probe: %Kazan.Apis.Core.V1.Probe{
                      failure_threshold: 3,
                      http_get: %Kazan.Apis.Core.V1.HTTPGetAction{
                        path: "/healthz",
                        port: 10254,
                        scheme: "HTTP"
                      },
                      initial_delay_seconds: 10,
                      period_seconds: 10,
                      success_threshold: 1,
                      timeout_seconds: 1
                    },
                    name: "nginx-ingress-controller",
                    ports: [
                      %Kazan.Apis.Core.V1.ContainerPort{
                        container_port: 80,
                        name: "http",
                        protocol: "TCP"
                      },
                      %Kazan.Apis.Core.V1.ContainerPort{
                        container_port: 443,
                        name: "https",
                        protocol: "TCP"
                      }
                    ],
                    readiness_probe: %Kazan.Apis.Core.V1.Probe{
                      failure_threshold: 3,
                      http_get: %Kazan.Apis.Core.V1.HTTPGetAction{
                        path: "/healthz",
                        port: 10254,
                        scheme: "HTTP"
                      },
                      initial_delay_seconds: 10,
                      period_seconds: 10,
                      success_threshold: 1,
                      timeout_seconds: 1
                    },
                    resources: %Kazan.Apis.Core.V1.ResourceRequirements{
                      limits: %{"memory" => "128Mi"},
                      requests: %{"memory" => "128Mi"}
                    },
                    security_context: %Kazan.Apis.Core.V1.SecurityContext{
                      allow_privilege_escalation: true,
                      capabilities: %Kazan.Apis.Core.V1.Capabilities{
                        add: ["NET_BIND_SERVICE"],
                        drop: ["ALL"]
                      },
                      run_as_user: 33
                    },
                    termination_message_path: "/dev/termination-log",
                    termination_message_policy: "File"
                  }
                ],
                dns_policy: "ClusterFirst",
                restart_policy: "Always",
                scheduler_name: "default-scheduler",
                security_context: %Kazan.Apis.Core.V1.PodSecurityContext{},
                service_account: "nginx-ingress-#{input_data["namespace_name"]}",
                service_account_name: "nginx-ingress-#{input_data["namespace_name"]}",
                termination_grace_period_seconds: 60
              }
            }
          },
          status: nil
        },
        "nginx-ingress-#{input_data["namespace_name"]}"
      )
      |> Kazan.run!(server: kazan_server)

    response =
      Kazan.Apis.Apps.V1.create_namespaced_deployment!(
        %Kazan.Apis.Apps.V1.Deployment{
          api_version: nil,
          kind: nil,
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            annotations: %{"deployment.kubernetes.io/revision" => "1"},
            labels: %{
              "app" => "nginx-ingress",
              "chart" => "nginx-ingress-1.24.2",
              "component" => "default-backend",
              "heritage" => "Tiller",
              "release" => "nginx-ingress-#{input_data["namespace_name"]}"
            },
            name: "nginx-ingress-#{input_data["namespace_name"]}-default-backend",
            namespace: "nginx-ingress-#{input_data["namespace_name"]}"
          },
          spec: %Kazan.Apis.Apps.V1.DeploymentSpec{
            progress_deadline_seconds: 600,
            replicas: 1,
            revision_history_limit: 10,
            selector: %Kazan.Models.Apimachinery.Meta.V1.LabelSelector{
              match_labels: %{
                "app" => "nginx-ingress",
                "release" => "nginx-ingress-#{input_data["namespace_name"]}"
              }
            },
            strategy: %Kazan.Apis.Apps.V1.DeploymentStrategy{
              rolling_update: %Kazan.Apis.Apps.V1.RollingUpdateDeployment{
                max_surge: "25%",
                max_unavailable: "25%"
              },
              type: "RollingUpdate"
            },
            template: %Kazan.Apis.Core.V1.PodTemplateSpec{
              metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
                labels: %{
                  "app" => "nginx-ingress",
                  "component" => "default-backend",
                  "release" => "nginx-ingress-#{input_data["namespace_name"]}"
                }
              },
              spec: %Kazan.Apis.Core.V1.PodSpec{
                containers: [
                  %Kazan.Apis.Core.V1.Container{
                    image: @ingress_backend_image_path,
                    image_pull_policy: "IfNotPresent",
                    liveness_probe: %Kazan.Apis.Core.V1.Probe{
                      failure_threshold: 3,
                      http_get: %Kazan.Apis.Core.V1.HTTPGetAction{
                        path: "/healthz",
                        port: 8080,
                        scheme: "HTTP"
                      },
                      initial_delay_seconds: 30,
                      period_seconds: 10,
                      success_threshold: 1,
                      timeout_seconds: 5
                    },
                    name: "nginx-ingress-default-backend",
                    ports: [
                      %Kazan.Apis.Core.V1.ContainerPort{
                        container_port: 8080,
                        name: "http",
                        protocol: "TCP"
                      }
                    ],
                    readiness_probe: %Kazan.Apis.Core.V1.Probe{
                      failure_threshold: 6,
                      http_get: %Kazan.Apis.Core.V1.HTTPGetAction{
                        path: "/healthz",
                        port: 8080,
                        scheme: "HTTP"
                      },
                      period_seconds: 5,
                      success_threshold: 1,
                      timeout_seconds: 5
                    },
                    resources: %Kazan.Apis.Core.V1.ResourceRequirements{
                      limits: %{"memory" => "16Mi"},
                      requests: %{"memory" => "16Mi"}
                    },
                    security_context: %Kazan.Apis.Core.V1.SecurityContext{
                      run_as_user: 65534
                    },
                    termination_message_path: "/dev/termination-log",
                    termination_message_policy: "File"
                  }
                ],
                dns_policy: "ClusterFirst",
                restart_policy: "Always",
                scheduler_name: "default-scheduler",
                security_context: %Kazan.Apis.Core.V1.PodSecurityContext{},
                service_account: "nginx-ingress-#{input_data["namespace_name"]}-backend",
                service_account_name: "nginx-ingress-#{input_data["namespace_name"]}-backend",
                termination_grace_period_seconds: 60
              }
            }
          },
          status: nil
        },
        "nginx-ingress-#{input_data["namespace_name"]}"
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_namespaced_deployment(kazan_server, input_data) do
    response =
      Kazan.Apis.Apps.V1.create_namespaced_deployment!(
        %Kazan.Apis.Apps.V1.Deployment{
          api_version: nil,
          kind: nil,
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            labels: %{
              "app.kubernetes.io/instance" => input_data["namespace_name"],
              "app.kubernetes.io/managed-by" => "Trixta",
              "app.kubernetes.io/name" => "trixta-space"
            },
            name: "#{input_data["namespace_name"]}-trixta-space",
            namespace: input_data["namespace_name"]
          },
          spec: %Kazan.Apis.Apps.V1.DeploymentSpec{
            progress_deadline_seconds: 600,
            replicas: 1,
            revision_history_limit: 10,
            selector: %Kazan.Models.Apimachinery.Meta.V1.LabelSelector{
              match_labels: %{
                "app.kubernetes.io/instance" => input_data["namespace_name"],
                "app.kubernetes.io/name" => "trixta-space"
              }
            },
            strategy: %Kazan.Apis.Apps.V1.DeploymentStrategy{
              rolling_update: %Kazan.Apis.Apps.V1.RollingUpdateDeployment{
                max_surge: "25%",
                max_unavailable: "25%"
              },
              type: "RollingUpdate"
            },
            template: %Kazan.Apis.Core.V1.PodTemplateSpec{
              metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
                creation_timestamp: nil,
                labels: %{
                  "app" => "trixta-space",
                  "app.kubernetes.io/instance" => input_data["namespace_name"],
                  "app.kubernetes.io/name" => "trixta-space"
                }
              },
              spec: %Kazan.Apis.Core.V1.PodSpec{
                containers: [
                  %Kazan.Apis.Core.V1.Container{
                    args: ["foreground"],
                    env: [
                      %Kazan.Apis.Core.V1.EnvVar{
                        name: "APP_POD_IP",
                        value_from: %Kazan.Apis.Core.V1.EnvVarSource{
                          field_ref: %Kazan.Apis.Core.V1.ObjectFieldSelector{
                            api_version: "v1",
                            field_path: "status.podIP"
                          }
                        }
                      }
                    ],
                    env_from: [
                      %Kazan.Apis.Core.V1.EnvFromSource{
                        config_map_ref: %Kazan.Apis.Core.V1.ConfigMapEnvSource{
                          name: "trixta-space-env"
                        }
                      },
                      %Kazan.Apis.Core.V1.EnvFromSource{
                        secret_ref: %Kazan.Apis.Core.V1.SecretEnvSource{
                          name: "trixta-space-env-secret"
                        }
                      }
                    ],
                    image: input_data["docker_image_path"],
                    image_pull_policy: "Always",
                    lifecycle: nil,
                    liveness_probe: %Kazan.Apis.Core.V1.Probe{
                      failure_threshold: 3,
                      http_get: %Kazan.Apis.Core.V1.HTTPGetAction{
                        path: "/",
                        port: "service",
                        scheme: "HTTP"
                      },
                      period_seconds: 10,
                      success_threshold: 1,
                      timeout_seconds: 1
                    },
                    name: "trixta-space",
                    ports: [
                      %Kazan.Apis.Core.V1.ContainerPort{
                        container_port: 4000,
                        name: "service",
                        protocol: "TCP"
                      }
                    ],
                    readiness_probe: %Kazan.Apis.Core.V1.Probe{
                      failure_threshold: 3,
                      http_get: %Kazan.Apis.Core.V1.HTTPGetAction{
                        path: "/",
                        port: "service",
                        scheme: "HTTP"
                      },
                      period_seconds: 10,
                      success_threshold: 1,
                      timeout_seconds: 1
                    },
                    resources: %Kazan.Apis.Core.V1.ResourceRequirements{},
                    termination_message_path: "/dev/termination-log",
                    termination_message_policy: "File",
                    volume_mounts: [
                      %Kazan.Apis.Core.V1.VolumeMount{
                        mount_path: "/var/tmp",
                        name: "dets-data"
                      }
                    ]
                  }
                ],
                dns_policy: "ClusterFirst",
                image_pull_secrets: [
                  %Kazan.Apis.Core.V1.LocalObjectReference{name: "gitlab-registry"}
                ],
                restart_policy: "Always",
                scheduler_name: "default-scheduler",
                security_context: %Kazan.Apis.Core.V1.PodSecurityContext{},
                termination_grace_period_seconds: 30,
                volumes: [
                  %Kazan.Apis.Core.V1.Volume{
                    name: "dets-data",
                    persistent_volume_claim:
                      %Kazan.Apis.Core.V1.PersistentVolumeClaimVolumeSource{
                        claim_name: "trixta-space-dets-pvc"
                      }
                  }
                ]
              }
            }
          },
          status: nil
        },
        input_data["namespace_name"]
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_ingresses(kazan_server, input_data) do
    response =
      Kazan.Apis.Extensions.V1beta1.create_namespaced_ingress!(
        %Kazan.Apis.Extensions.V1beta1.Ingress{
          api_version: nil,
          kind: nil,
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            labels: %{
              "app.kubernetes.io/instance" => input_data["namespace_name"],
              "app.kubernetes.io/managed-by" => "Trixta",
              "app.kubernetes.io/name" => "trixta-space"
            },
            annotations: %{
              "kubernetes.io/ingress.class" => "nginx-ingress-#{input_data["namespace_name"]}"
            },
            name: "#{input_data["namespace_name"]}-trixta-space"
          },
          spec: %Kazan.Apis.Extensions.V1beta1.IngressSpec{
            rules: [
              %Kazan.Apis.Extensions.V1beta1.IngressRule{
                host: "#{input_data["namespace_name"]}.#{input_data["domain_name"]}",
                http: %Kazan.Apis.Extensions.V1beta1.HTTPIngressRuleValue{
                  paths: [
                    %Kazan.Apis.Extensions.V1beta1.HTTPIngressPath{
                      backend: %Kazan.Apis.Extensions.V1beta1.IngressBackend{
                        service_name: "#{input_data["namespace_name"]}-trixta-space",
                        service_port: "http"
                      },
                      path: "/"
                    }
                  ]
                }
              }
            ]
          },
          status: nil
        },
        input_data["namespace_name"]
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_ingress_service(kazan_server, input_data, node_port) do
    Kazan.Apis.Core.V1.create_namespaced_service!(
      %Kazan.Apis.Core.V1.Service{
        api_version: nil,
        kind: nil,
        metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
          labels: %{
            "app" => "nginx-ingress",
            "chart" => "nginx-ingress-1.24.2",
            "component" => "controller",
            "heritage" => "Tiller",
            "release" => "nginx-ingress-#{input_data["namespace_name"]}"
          },
          name: "nginx-ingress-#{input_data["namespace_name"]}-controller",
          namespace: "nginx-ingress-#{input_data["namespace_name"]}"
        },
        spec: %Kazan.Apis.Core.V1.ServiceSpec{
          ports: [
            %Kazan.Apis.Core.V1.ServicePort{
              name: "http",
              node_port: node_port,
              port: 80,
              protocol: "TCP",
              target_port: "http"
            },
            %Kazan.Apis.Core.V1.ServicePort{
              name: "https",
              node_port: node_port + 1,
              port: 443,
              protocol: "TCP",
              target_port: "https"
            }
          ],
          selector: %{
            "app" => "nginx-ingress",
            "component" => "controller",
            "release" => "nginx-ingress-#{input_data["namespace_name"]}"
          },
          type: "NodePort"
        },
        status: nil
      },
      "nginx-ingress-#{input_data["namespace_name"]}"
    )
    |> Kazan.run!(server: kazan_server)

    response =
      Kazan.Apis.Core.V1.create_namespaced_service!(
        %Kazan.Apis.Core.V1.Service{
          api_version: nil,
          kind: nil,
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            labels: %{
              "app" => "nginx-ingress",
              "chart" => "nginx-ingress-1.24.2",
              "component" => "default-backend",
              "heritage" => "Tiller",
              "release" => "nginx-ingress-#{input_data["namespace_name"]}"
            },
            name: "nginx-ingress-#{input_data["namespace_name"]}-default-backend",
            namespace: "nginx-ingress-#{input_data["namespace_name"]}"
          },
          spec: %Kazan.Apis.Core.V1.ServiceSpec{
            ports: [
              %Kazan.Apis.Core.V1.ServicePort{
                name: "http",
                node_port: nil,
                port: 80,
                protocol: "TCP",
                target_port: "http"
              }
            ],
            selector: %{
              "app" => "nginx-ingress",
              "component" => "default-backend",
              "release" => "nginx-ingress-#{input_data["namespace_name"]}"
            },
            type: "ClusterIP"
          },
          status: nil
        },
        "nginx-ingress-#{input_data["namespace_name"]}"
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_service(kazan_server, input_data) do
    response =
      Kazan.Apis.Core.V1.create_namespaced_service!(
        %Kazan.Apis.Core.V1.Service{
          api_version: nil,
          kind: nil,
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            labels: %{
              "app.kubernetes.io/instance" => input_data["namespace_name"],
              "app.kubernetes.io/managed-by" => "Trixta",
              "app.kubernetes.io/name" => "trixta-space"
            },
            name: "#{input_data["namespace_name"]}-trixta-space",
            namespace: input_data["namespace_name"]
          },
          spec: %Kazan.Apis.Core.V1.ServiceSpec{
            ports: [
              %Kazan.Apis.Core.V1.ServicePort{
                name: "http",
                port: 80,
                protocol: "TCP",
                target_port: 4000
              }
            ],
            selector: %{
              "app" => "trixta-space",
              "app.kubernetes.io/instance" => input_data["namespace_name"],
              "app.kubernetes.io/name" => "trixta-space"
            },
            type: "LoadBalancer"
          },
          status: nil
        },
        input_data["namespace_name"]
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_service_headless(kazan_server, input_data) do
    response =
      Kazan.Apis.Core.V1.create_namespaced_service!(
        %Kazan.Apis.Core.V1.Service{
          api_version: nil,
          kind: nil,
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            labels: %{
              "app.kubernetes.io/instance" => input_data["namespace_name"],
              "app.kubernetes.io/managed-by" => "Trixta",
              "app.kubernetes.io/name" => "trixta-space"
            },
            name: "trixta-space-headless",
            namespace: input_data["namespace_name"]
          },
          spec: %Kazan.Apis.Core.V1.ServiceSpec{
            cluster_ip: "None",
            ports: [
              %Kazan.Apis.Core.V1.ServicePort{
                port: 4000
              }
            ],
            selector: %{
              "app" => "trixta-space",
              "app.kubernetes.io/instance" => input_data["namespace_name"],
              "app.kubernetes.io/name" => "trixta-space"
            }
          },
          status: nil
        },
        input_data["namespace_name"]
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_ingress_service_account(kazan_server, input_data) do
    Kazan.Apis.Core.V1.create_namespaced_service_account!(
      %Kazan.Apis.Core.V1.ServiceAccount{
        metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
          labels: %{
            "app" => "nginx-ingress",
            "chart" => "nginx-ingress-1.24.2",
            "heritage" => "Tiller",
            "release" => "nginx-ingress-#{input_data["namespace_name"]}"
          },
          name: "nginx-ingress-#{input_data["namespace_name"]}",
          namespace: "nginx-ingress-#{input_data["namespace_name"]}"
        },
        secrets: [
          %Kazan.Apis.Core.V1.ObjectReference{
            name: "nginx-ingress-#{input_data["namespace_name"]}-token"
          }
        ]
      },
      "nginx-ingress-#{input_data["namespace_name"]}"
    )
    |> Kazan.run!(server: kazan_server)

    response =
      Kazan.Apis.Core.V1.create_namespaced_service_account!(
        %Kazan.Apis.Core.V1.ServiceAccount{
          api_version: nil,
          kind: nil,
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            labels: %{
              "app" => "nginx-ingress",
              "chart" => "nginx-ingress-1.24.2",
              "heritage" => "Tiller",
              "release" => "nginx-ingress-#{input_data["namespace_name"]}"
            },
            name: "nginx-ingress-#{input_data["namespace_name"]}-backend",
            namespace: "nginx-ingress-#{input_data["namespace_name"]}"
          },
          secrets: [
            %Kazan.Apis.Core.V1.ObjectReference{
              name: "nginx-ingress-#{input_data["namespace_name"]}-backend-token"
            }
          ]
        },
        "nginx-ingress-#{input_data["namespace_name"]}"
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_ingress_cluster_role(kazan_server, input_data) do
    response =
      Kazan.Apis.Rbacauthorization.V1.create_cluster_role!(%Kazan.Apis.Rbac.V1.ClusterRole{
        metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
          labels: %{
            "app" => "nginx-ingress",
            "chart" => "nginx-ingress-1.24.3",
            "heritage" => "Tiller",
            "release" => "nginx-ingress-#{input_data["namespace_name"]}"
          },
          name: "nginx-ingress-#{input_data["namespace_name"]}"
        },
        rules: [
          %Kazan.Apis.Rbac.V1.PolicyRule{
            api_groups: [""],
            resources: ["configmaps", "endpoints", "nodes", "pods", "secrets"],
            verbs: ["list", "watch"]
          },
          %Kazan.Apis.Rbac.V1.PolicyRule{
            api_groups: [""],
            resources: ["nodes"],
            verbs: ["get"]
          },
          %Kazan.Apis.Rbac.V1.PolicyRule{
            api_groups: [""],
            resources: ["services"],
            verbs: ["get", "list", "update", "watch"]
          },
          %Kazan.Apis.Rbac.V1.PolicyRule{
            api_groups: ["extensions", "networking.k8s.io"],
            resources: ["ingresses"],
            verbs: ["get", "list", "watch"]
          },
          %Kazan.Apis.Rbac.V1.PolicyRule{
            api_groups: [""],
            resources: ["events"],
            verbs: ["create", "patch"]
          },
          %Kazan.Apis.Rbac.V1.PolicyRule{
            api_groups: ["extensions", "networking.k8s.io"],
            non_resource_ur_ls: nil,
            resource_names: nil,
            resources: ["ingresses/status"],
            verbs: ["update"]
          }
        ]
      })
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_ingress_cluster_role_binding(kazan_server, input_data) do
    response =
      Kazan.Apis.Rbacauthorization.V1.create_cluster_role_binding!(
        %Kazan.Apis.Rbac.V1.ClusterRoleBinding{
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            labels: %{
              "app" => "nginx-ingress",
              "chart" => "nginx-ingress-1.24.3",
              "heritage" => "Tiller",
              "release" => "nginx-ingress-#{input_data["namespace_name"]}"
            },
            name: "nginx-ingress-#{input_data["namespace_name"]}"
          },
          role_ref: %Kazan.Apis.Rbac.V1.RoleRef{
            api_group: "rbac.authorization.k8s.io",
            kind: "ClusterRole",
            name: "nginx-ingress-#{input_data["namespace_name"]}"
          },
          subjects: [
            %Kazan.Apis.Rbac.V1.Subject{
              kind: "ServiceAccount",
              name: "nginx-ingress-#{input_data["namespace_name"]}",
              namespace: "nginx-ingress-#{input_data["namespace_name"]}"
            }
          ]
        }
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_ingress_role(kazan_server, input_data) do
    response =
      Kazan.Apis.Rbacauthorization.V1.create_namespaced_role!(
        %Kazan.Apis.Rbac.V1.Role{
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            labels: %{
              "app" => "nginx-ingress",
              "chart" => "nginx-ingress-1.24.3",
              "heritage" => "Tiller",
              "release" => "nginx-ingress-#{input_data["namespace_name"]}"
            },
            name: "nginx-ingress-#{input_data["namespace_name"]}",
            namespace: "nginx-ingress-#{input_data["namespace_name"]}"
          },
          rules: [
            %Kazan.Apis.Rbac.V1.PolicyRule{
              api_groups: [""],
              resources: ["namespaces"],
              verbs: ["get"]
            },
            %Kazan.Apis.Rbac.V1.PolicyRule{
              api_groups: [""],
              resources: ["configmaps", "pods", "secrets", "endpoints"],
              verbs: ["get", "list", "watch"]
            },
            %Kazan.Apis.Rbac.V1.PolicyRule{
              api_groups: [""],
              resources: ["services"],
              verbs: ["get", "list", "update", "watch"]
            },
            %Kazan.Apis.Rbac.V1.PolicyRule{
              api_groups: ["extensions", "networking.k8s.io"],
              resources: ["ingresses"],
              verbs: ["get", "list", "watch"]
            },
            %Kazan.Apis.Rbac.V1.PolicyRule{
              api_groups: ["extensions", "networking.k8s.io"],
              resources: ["ingresses/status"],
              verbs: ["update"]
            },
            %Kazan.Apis.Rbac.V1.PolicyRule{
              api_groups: [""],
              resource_names: [
                "ingress-controller-leader-nginx-ingress-#{input_data["namespace_name"]}"
              ],
              resources: ["configmaps"],
              verbs: ["get", "update"]
            },
            %Kazan.Apis.Rbac.V1.PolicyRule{
              api_groups: [""],
              resources: ["configmaps"],
              verbs: ["create"]
            },
            %Kazan.Apis.Rbac.V1.PolicyRule{
              api_groups: [""],
              resources: ["endpoints"],
              verbs: ["create", "get", "update"]
            },
            %Kazan.Apis.Rbac.V1.PolicyRule{
              api_groups: [""],
              resources: ["events"],
              verbs: ["create", "patch"]
            }
          ]
        },
        "nginx-ingress-#{input_data["namespace_name"]}"
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def create_ingress_role_binding(kazan_server, input_data) do
    response =
      Kazan.Apis.Rbacauthorization.V1.create_namespaced_role_binding!(
        %Kazan.Apis.Rbac.V1.RoleBinding{
          metadata: %Kazan.Models.Apimachinery.Meta.V1.ObjectMeta{
            labels: %{
              "app" => "nginx-ingress",
              "chart" => "nginx-ingress-1.24.3",
              "heritage" => "Tiller",
              "release" => "nginx-ingress-#{input_data["namespace_name"]}"
            },
            name: "nginx-ingress-#{input_data["namespace_name"]}",
            namespace: "nginx-ingress-#{input_data["namespace_name"]}"
          },
          role_ref: %Kazan.Apis.Rbac.V1.RoleRef{
            api_group: "rbac.authorization.k8s.io",
            kind: "Role",
            name: "nginx-ingress-#{input_data["namespace_name"]}"
          },
          subjects: [
            %Kazan.Apis.Rbac.V1.Subject{
              api_group: nil,
              kind: "ServiceAccount",
              name: "nginx-ingress-#{input_data["namespace_name"]}",
              namespace: "nginx-ingress-#{input_data["namespace_name"]}"
            }
          ]
        },
        "nginx-ingress-#{input_data["namespace_name"]}"
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  # Deletion
  # TODO: Equivilant
  # - helm del --purge $TRIXTA_NAMESPACE || true
  # - kubectl delete namespace $TRIXTA_NAMESPACE --ignore-not-found=true
  # - helm del --purge nginx-ingress-${TRIXTA_NAMESPACE} || true
  # - kubectl delete namespace nginx-ingress-${TRIXTA_NAMESPACE} --ignore-not-found=true
  # ++ AWS config
  def delete_namespace(kazan_server, input_data) do
    response =
      Kazan.Apis.Core.V1.delete_namespace!(
        %Kazan.Models.Apimachinery.Meta.V1.DeleteOptions{},
        input_data["namespace_name"],
        []
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def delete_cluster_role_binding(kazan_server, input_data) do
    response =
      Kazan.Apis.Rbacauthorization.V1.delete_cluster_role_binding!(
        %Kazan.Models.Apimachinery.Meta.V1.DeleteOptions{},
        "nginx-ingress-#{input_data["namespace_name"]}",
        []
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def delete_cluster_role(kazan_server, input_data) do
    response =
      Kazan.Apis.Rbacauthorization.V1.delete_cluster_role!(
        %Kazan.Models.Apimachinery.Meta.V1.DeleteOptions{},
        "nginx-ingress-#{input_data["namespace_name"]}",
        []
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end

  def delete_ingress_namespace(kazan_server, input_data) do
    response =
      Kazan.Apis.Core.V1.delete_namespace!(
        %Kazan.Models.Apimachinery.Meta.V1.DeleteOptions{},
        "nginx-ingress-#{input_data["namespace_name"]}",
        []
      )
      |> Kazan.run!(server: kazan_server)

    {"success", response}
  end
end
