defmodule Mix.Tasks.Trixta.DeleteAwsSpaceRouting do
  @moduledoc """
  Uses ExAWS to delete the non-k8s AWS resources for a space, including the ALB rule and target group.
  Arguments are as follows:
  - Cluster VPC id (including the 'vpc-' prefix.)
  - Space public hostname (e.g. foobar.test.trixta.io) . Must be the same as was used when creating the space.
  - aws region, e.g. eu-west-2
  - aws access key ID (plaintext, not encoded)
  - aws secret key (plaintext, not encoded)
  """
  use Mix.Task

  @shortdoc "Mix Task to set up public ALB routing for a space already existing on the cluster"

  def run([vpc_id, host_name, aws_region, aws_access_key_id, aws_secret_key] = args) do
    {:ok, _started} = Application.ensure_all_started(:httpoison)

    credentials = %{
      "aws_access_key" => aws_secret_key,
      "aws_access_key_id" => aws_access_key_id,
      "aws_region" => aws_region
    }

    TrixtaLibrary.Core.AwsExpert.delete_space_routing(vpc_id, host_name, credentials)
  end
end
