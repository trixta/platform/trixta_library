defmodule Mix.Tasks.Trixta.SetupAwsSpaceRouting do
  @moduledoc """
  Uses ExAWS to set up ALB-level routing to a pre-existing space on the cluster.
  Arguments are as follows:
  - Cluster VPC id (including the 'vpc-' prefix.)
  - Space public hostname (e.g. foobar.test.trixta.io)
  - Port on which the space's ingress is listening, e.g. 30084
  - aws region, e.g. eu-west-2
  - aws access key ID (plaintext, not encoded)
  - aws secret key (plaintext, not encoded)
  """
  use Mix.Task

  @shortdoc "Mix Task to set up public ALB routing for a space already existing on the cluster"

  def run([vpc_id, host_name, port, aws_region, aws_access_key_id, aws_secret_key] = args) do
    {:ok, _started} = Application.ensure_all_started(:httpoison)

    credentials = %{
      "aws_access_key" => aws_secret_key,
      "aws_access_key_id" => aws_access_key_id,
      "aws_region" => aws_region
    }

    TrixtaLibrary.Core.AwsExpert.set_up_new_space_routing(vpc_id, host_name, port, credentials)
  end
end
