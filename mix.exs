defmodule TrixtaLibrary.MixProject do
  use Mix.Project

  def project do
    [
      app: :trixta_library,
      version: "0.1.1",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      aliases: aliases(),
      name: "TrixtaLibrary",
      docs: [main: "TrixtaLibrary", extras: ["README.md"]],
      dialyzer: [plt_add_deps: :transitive],
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
      # if you want to use espec,
      # test_coverage: [tool: ExCoveralls, test_task: "espec"]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {TrixtaLibrary.Application, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:chess_logic, "~> 0.3.0"},
      {:httpotion, "~> 3.1.0"},
      {:bamboo, "~> 2.2.0"},
      {:bamboo_ses, "~> 0.2.0"},
      {:mustache, "~> 0.3.0"},
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.1", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.12", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.21", only: [:dev, :test], runtime: false},
      {:inch_ex, "~> 2.0", only: [:dev, :test], runtime: false},
      {:poison, "~> 4.0", override: true},
      {:jason, "~> 1.2"},
      {:trixta_utils, path: "../trixta_utils"},
      {:trixta_spaces, path: "../trixta_spaces"},
      {:trixta_metrics, path: "../trixta_metrics"},
      {:ex_aws, "~> 2.2.3"},
      {:ex_aws_s3, "~> 2.0"},
      {:ex_aws_ec2, "~> 2.0"},
      {:ex_aws_elastic_load_balancing, "~> 2.1"},
      {:ex_aws_textract, "~> 0.0.1"},
      {:hackney, "~> 1.17"},
      {:sweet_xml, "~> 0.6"},
      {:postgrex, "~> 0.15.10"},
      {:ecto_sql, "~> 3.6.2"},
      {:quantum, "~> 3.0"},
      {:timex, "~> 3.7"},
      {:kazan, "~> 0.11"},
      {:slack, "~> 0.23.5"},
      {:fastglobal, "~> 1.0"},
      {:castore, "~> 0.1.11"},
      {:slipstream, "~> 1.0.1"},
      {:closed_intervals, "~> 0.6.0"},
      {:hashids, "~> 2.0"}
    ]
  end

  def aliases do
    [
      review: ["coveralls", "dialyzer", "inch", "hex.audit", "hex.outdated", "credo --strict"]
    ]
  end

  defp description() do
    "The Trixta Library Application."
  end

  defp package() do
    [
      licenses: ["AGPL-3.0-or-later"],
      links: %{"GitLab" => "https://gitlab.com/trixta/platform/trixta_library"}
    ]
  end
end
