use Mix.Config

# This will be overridden by the trixta_space config. It is only used here when running tests on this project in isolation.
# When running tests, configure the Logger console backend as it would be on trixta_space,
# so that we can test our Ethereum logs, for example (They need to be in a specific format for FluentBit).
config :logger,
  backends: [:console],
  compile_time_purge_level: :debug,
  handle_sasl_reports: false

# This section should be the same as in trixta_space/config/config.exs,
# because that's what it would be in production.
config :logger, :console,
  utc_log: true,
  format: {TrixtaUtils.Logging.ConsoleFormatter, :format_log},
  metadata: [:module, :function, :override_timestamp],
  sync_threshold: 1000
