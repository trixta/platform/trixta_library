ExUnit.start()

defmodule TrixtaLibrary.TrafficAlerts.TrafficExpert.Test do
  use ExUnit.Case

  @moduletag :capture_log

  setup_all do
    %{
      traffic_data: %{
        "class" => "vehicle",
        "action" => "move",
        "make" => "BMW",
        "objectId" => "2323432",
        "gps" => %{
          "long" => 327_363,
          "lat" => 327_363
        },
        "image" => "DKDKDJDJJSKSJSKDKDJSKDJSKLDJDJ",
        "probability" => 96,
        "numberplate" => %{
          "image" => "DKDKDJDJJSKSJSKDKDJSKDJSKLDJDJ",
          "text" => "CA787263",
          "probability" => 96
        }
      }
    }
  end

  test "traffic alert matches with 2 parameters", %{:traffic_data => traffic_data} do
    result =
      TrixtaLibrary.TrafficAlerts.TrafficExpert.match_alert(
        traffic_data,
        %{
          "numberplate" => "CA787263",
          "make" => "BMW"
        }
      )

    assert result == {"criteria_met", %{"result" => true}}
  end

  test "traffic alert matches with 1 parameter", %{:traffic_data => traffic_data} do
    result =
      TrixtaLibrary.TrafficAlerts.TrafficExpert.match_alert(
        traffic_data,
        %{
          "make" => "BMW"
        }
      )

    assert result == {"criteria_met", %{"result" => true}}
  end

  test "traffic alert matches with 'make' parameter", %{:traffic_data => traffic_data} do
    result =
      TrixtaLibrary.TrafficAlerts.TrafficExpert.match_alert(
        traffic_data,
        %{
          "make" => "BMW"
        }
      )

    assert result == {"criteria_met", %{"result" => true}}
  end

  test "traffic alert matches with 'numberplate' parameter", %{:traffic_data => traffic_data} do
    result =
      TrixtaLibrary.TrafficAlerts.TrafficExpert.match_alert(
        traffic_data,
        %{
          "numberplate" => "CA787263"
        }
      )

    assert result == {"criteria_met", %{"result" => true}}
  end

  test "traffic alert matches with no parameters", %{:traffic_data => traffic_data} do
    result =
      TrixtaLibrary.TrafficAlerts.TrafficExpert.match_alert(
        traffic_data,
        %{}
      )

    assert result == {"criteria_met", %{"result" => true}}
  end

  test "traffic alert matches with 1 parameter in lowercase", %{:traffic_data => traffic_data} do
    result =
      TrixtaLibrary.TrafficAlerts.TrafficExpert.match_alert(
        traffic_data,
        %{
          "make" => "bmw"
        }
      )

    assert result == {"criteria_met", %{"result" => true}}
  end

  test "traffic alert does not match with incorrect matching values", %{
    :traffic_data => traffic_data
  } do
    result =
      TrixtaLibrary.TrafficAlerts.TrafficExpert.match_alert(
        traffic_data,
        %{
          "make" => "KIA SOUL"
        }
      )

    assert result == {"criteria_not_met", %{"result" => false}}
  end
end
