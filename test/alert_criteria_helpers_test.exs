defmodule TrixtaLibraryTest.Blockchain.AlertCriteriaHelpersTest do
  @moduledoc """
    Tests the code that processes alert criteria (i.e. compares ElasticSearch results against an alert to determine an outcome)
  """
  use ExUnit.Case
  doctest TrixtaLibrary.BlockChain.AlertCriteriaHelpers
  alias TrixtaLibrary.BlockChain.AlertCriteriaHelpers, as: Helpers

  test "Can detect triggered alert on Event Count query" do
    alert = %{
      "agent_id" => "test@example.com",
      "alert" => %{
        "address" => "0x06012c8cf97bead5deae237070f9587f8e7a266d",
        "aggregation" => "value_count",
        "condition" => "greater_than",
        # We are using Cryptokitties as the test case. :-)
        "contract_event" => "Pregnant",
        "dataset" => "contract_logs",
        "id" => "zzh9hJSLS-ui3ERZERXO",
        "name" => "Pregnancy Count",
        "period" => "",
        "query" =>
          "{\"query\":{\"bool\":{\"filter\":{\"bool\":{\"must\":[{\"term\":{\"address\":\"0x06012c8cf97bead5deae237070f9587f8e7a266d\"}},{\"term\":{\"cat\":\"eth_contract_log\"}}]}}}},\"aggs\":{\"bucket\":{\"filter\":{\"match\":{\"eventName\":\"Pregnant\"}},\"aggs\":{\"0\":{\"value_count\":{\"field\":\"ts\"}}}}}}",
        "select_fields" => "contract_logs",
        "type" => false,
        "value" => "35331"
      }
    }

    elasticsearch_result = %{
      "_shards" => %{
        "failed" => 0,
        "skipped" => 0,
        "successful" => 205,
        "total" => 205
      },
      "aggregations" => %{
        "bucket" => %{
          "0" => %{"value" => 35332},
          "doc_count" => 35332
        }
      },
      "hits" => %{
        "hits" => [
          # Hits go here. Not used by processing code.
        ],
        "max_score" => 0.0,
        "total" => 181_227
      },
      "timed_out" => false,
      "took" => 19
    }

    assert {"criteria_met", _, 35332} = Helpers.is_condition_met(elasticsearch_result, alert)
  end

  test "Can detect triggered alert on date histogram query" do
    alert = %{
      "agent_id" => "test@example.com",
      "alert" => %{
        "address" => "0x06012c8cf97bead5deae237070f9587f8e7a266d",
        "aggregation" => "value_count",
        "condition" => "greater_than",
        "contract_event" => "Pregnant",
        "dataset" => "contract_logs",
        "id" => "zzh9hJSLS-ui3ERZERXO",
        "name" => "Pregnancy Count",
        "period" => "",
        "select_fields" => "contract_logs",
        "type" => false,
        "value" => "10000"
      }
    }

    elasticsearch_result = %{
      "_shards" => %{
        "failed" => 0,
        "skipped" => 145,
        "successful" => 220,
        "total" => 220
      },
      "aggregations" => %{
        "bucket" => %{
          "buckets" => [
            %{
              "0" => %{"value" => 8238},
              "doc_count" => 9046,
              "key" => 1_562_630_400_000,
              "key_as_string" => "2019-07-09T00:00:00.000Z"
            },
            %{
              "0" => %{"value" => 17027},
              "doc_count" => 18036,
              "key" => 1_562_716_800_000,
              "key_as_string" => "2019-07-10T00:00:00.000Z"
            },
            %{
              "0" => %{"value" => 19867},
              "doc_count" => 20686,
              "key" => 1_562_803_200_000,
              "key_as_string" => "2019-07-11T00:00:00.000Z"
            }
          ]
        }
      },
      "hits" => %{"hits" => [], "max_score" => 0, "total" => 523_657},
      "timed_out" => false,
      "took" => 120
    }

    # The alert threshold value is 10000, so 17027 in the ES result should be the first one matching.
    assert {"criteria_met", "2019-07-10T00:00:00.000Z", 17027} =
             Helpers.is_condition_met(elasticsearch_result, alert)

    # If we raise the threshold to 19000, the value 19867 should still match:
    assert {"criteria_met", "2019-07-11T00:00:00.000Z", 19867} =
             Helpers.is_condition_met(
               elasticsearch_result,
               alert |> put_in(["alert", "value"], "19000")
             )

    # If we raise the threshold to above 19867, no value in the result matches the alert anymore:
    assert "criteria_not_met" =
             Helpers.is_condition_met(
               elasticsearch_result,
               alert |> put_in(["alert", "value"], "19868")
             )

    # If we change the condition to 'less than', it should work the other way around:
    alert_less_than = alert |> put_in(["alert", "condition"], "less_than")

    assert "criteria_not_met" =
             Helpers.is_condition_met(
               elasticsearch_result,
               alert_less_than |> put_in(["alert", "value"], "8237")
             )

    assert {"criteria_met", "2019-07-09T00:00:00.000Z", 8238} =
             Helpers.is_condition_met(
               elasticsearch_result,
               alert_less_than |> put_in(["alert", "value"], "17000")
             )
  end
end
